package structs

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"text/template"

	// sql "database/sql"
	"net/http"
	"time"

	// "github.com/gravitational/teleport/lib/backend"
	// "github.com/gravitational/teleport/lib/services"
	// "github.com/gravitational/teleport/lib/utils"
	"github.com/docker/docker/api/types"
	"github.com/hunterlong/statping/utils"
	"github.com/juju/juju/network"
	api "github.com/osrg/gobgp/api"
	"github.com/osrg/gobgp/pkg/packet/bgp"
	compute "google.golang.org/api/compute/v1"
	container "google.golang.org/api/container/v1"
	"gopkg.in/yaml.v2"
	// "owl.cbsi.com/c3r-builder/teleport/lib/services"
	// "owl.cbsi.com/c3r-builder/teleport/lib/utils"
)

// Still need more
// I need more comments
// type GlobalVariables struct {
// 	Environment []RouterEnvironment `yaml:"Environment" json:"environment"`
// }

type RouterEnvironment struct {
	EncryptionKey string               `yaml:"EncryptionKey" json:"encryptionkey"`
	Bgp           RouterEnvironmentBgp `yaml:"Bgp" json:"bgp"`
}

type RouterEnvironmentBgp struct {
	AcceptedCommunityString []string                    `yaml:"AcceptedCommunityString" json:"acceptedcommunitystring"`
	RejectedCommunityString []string                    `yaml:"RejectedCommunityString" json:"rejectedcommunitystring"`
	AcceptedPrefix          []string                    `yaml:"AcceptedPrefix" json:"acceptedprefix"`
	RejectedPrefix          []string                    `yaml:"RejectedPrefix" json:"rejectedprefix"`
	DynamicNeighbors        []BgpCustomDynamicNeighbors `yaml:"DynamicNeighbors" json:"dynamicneighbors"`
	Flowspec                []RouterFlowSpecRoutes      `yaml:"BgpFlowspec" json:"bgpflowspec"`
}

type HaController struct {
	Ctx         *context.Context    `yaml:"Ctx" json:"ctx"`
	Enabled     bool                `yaml:"Enabled" json:"enabled"`
	Provider    string              `yaml:"Provider" json:"provider"`
	GcpConfig   *CloudProviderGcp   `yaml:"GcpConfig" json:"gcpconfig"`
	AwsConfig   *CloudProviderAws   `yaml:"AwsConfig" json:"awsconfig"`
	AzureConfig *CloudProviderAzure `yaml:"AzureConfig" json:"azureconfig"`
	Peers       []HAPeers           `yaml:"Peers" json:"peers"`
}

type RouterBootstrap struct {
	AdminUp               bool                   `yaml:"AdminUp" json:"adminup"`
	DeviceName            string                 `yaml:"DeviceName" json:"devicename"`
	C3                    C3                     `yaml:"C3" json:"c3"`
	Icon                  string                 `yaml:"Icon" json:"icon"`
	FQDN                  string                 `yaml:"FQDN" json:"fqdn"`
	DeviceCommunity       string                 `yaml:"DeviceCommunity" json:"devicecommunity"`
	DiscoveryRecords      RouterDiscoveryRecords `yaml:"DiscoveryRecords" json:"discoveryrecords"`
	CoreRouters           []string               `yaml:"CoreRouters" json:"corerouters"`
	Vxlan                 RouterVxlan            `yaml:"Vxlan" json:"vxlan"`
	Authentication        RouterCloudAuth        `yaml:"Authentication" json:"authentication"`
	Encryption            RouterCloudKms         `yaml:"Encryption" json:"encryption"`
	Storage               RouterCloudStorage     `yaml:"Storage" json:"storage"`
	Registry              RouterDockerRegistry   `yaml:"Registry" json:"registry"`
	SerialNumber          string                 `yaml:"SerialNumber" json:"serialnumber"`
	EnableIPSla           bool                   `yaml:"EnableIPSla" json:"enableipsla"`
	ConfigFilePath        string                 `yaml:"ConfigFilePath" json:"configfilepath"`
	HostConfigFilePath    string                 `yaml:"HostConfigFilePath" json:"hostconfigfilepath"`
	DindConfigFilePath    string                 `yaml:"DindConfigFilePath" json:"dindconfigfilepath"`
	ServiceEndpoints      RouterServiceEndpoints `yaml:"ServiceEndpoints" json:"serviceendpoints"`
	Services              RouterServices         `yaml:"Services" json:"services"`
	DevTools              bool                   `yaml:"DevTools" json:"devtools"`
	DevCreds              DevCredentials         `yaml:"DevCreds"`
	Plugins               RouterPlugins          `yaml:"Plugins" json:"plugins"`
	UpgradeBucket         string                 `yaml:"UpgradeBucket" json:"upgradebucket"`
	Version               string                 `yaml:"Version" json:"version"`
	CanaryTest            bool                   `yaml:"CanaryTest" json:"canarytest"`
	RunVersion            string                 `yaml:"RunVersion" json:"runversion"`
	CfgctlVersion         string                 `yaml:"CfgctlVersion" json:"cfgctlversion"`
	CoreRouter            bool                   `yaml:"CoreRouter" json:"corerouter"`
	KubernetesDS          bool                   `yaml:"KubernetesDS" json:"kubernetesds"`
	KubernetesPeer        bool                   `yaml:"KubernetesPeer" json:"kubernetespeer"`
	GcpPrivateBridge      bool                   `yaml:"GcpPrivateBridge" json:"gcpprivatebridge"`
	HighAvailability      bool                   `yaml:"HighAvailability" json:"highavailability"`
	HAPeers               []HAPeers              `yaml:"HAPeers" json:"hapeers"`
	HaAdminUp             bool                   `yaml:"HaAdminUp" json:"haadminup"`
	HASetLabel            string                 `yaml:"HASetLabel" json:"hasetlable"`
	Debug                 RouterDebug            `yaml:"Debug" json:"debug"`
	CloudProvider         string                 `yaml:"CloudProvider" json:"cloudprovider"`
	Interfaces            []InterfaceConfig      `yaml:"Interfaces" json:"interfaces"`
	PrimaryLanInterface   InterfaceConfig        `yaml:"PrimaryLanInterface" json:"primarylaninterface"`
	AttachedSubnets       []AttachedSubnets      `yaml:"AttachedSubnets" json:"attachedsubnets"`
	PrimaryLanSubnet      AttachedSubnets        `yaml:"PrimaryLanSubnet" json:"primarylansubnet"`
	Logging               RouterLogging          `yaml:"Logging" json:"logging"`
	LastUpdated           time.Time              `yaml:"LastUpdated" json:"lastupdated"`
	UnmanagedState        bool                   `yaml:"UnmanagedState" json:"unmanagedstate"`
	BuildStatus           string                 `yaml:"BuildStatus" json:"buildstatus"`
	SSHPublicKeyMaterial  string                 `yaml:"SSHPublicKeyMaterial" json:"sshpublickeymaterial"`
	SSHPrivateKeyMaterial string                 `yaml:"SSHPrivateKeyMaterial" json:"sshprivatekeymaterial"`
	SSHKeyPath            string                 `yaml:"SSHKeyPath" json:"sshkeypath"`
	PublicIP              string                 `yaml:"PublicIP" json:"publicip"`
	PublicIPLastCheck     time.Time              `yaml:"PublicIPLastCheck" json:"publiciplastcheck"`
	Loopbacks             []InterfaceConfig      `yaml:"Loopbacks" json:"loopbacks"`
	Platform              string                 `yaml:"Platform" json:"platform"`
	PlatformPodCidr       string                 `yaml:"PlatformPodCidr" json:"platformpodcidr"`
	PlatformServiceCidr   string                 `yaml:"PlatformServiceCidr" json:"platformservicecidr"`
	PlatformConnector     bool                   `yaml:"PlatformConnector" json:"platformconnector"`
	PlatformConnectorIP   string                 `yaml:"PlatformConnectorIP" json:"platformconnectorip"`
	PlatformUUID          string                 `yaml:"PlatformUUID" json:"platformuuid"`
	PlatformConnectorCidr string                 `yaml:"PlatformConnectorCidr" json:"platformconnectorcidr"`
	PlatformEdgeRouters   []string               `yaml:"PlatformEdgeRouters" json:"platformedgerouters"`
	PlatformEdgeIP        []string               `yaml:"PlatformEdgeIP" json:"platformedgeip"`
	PlatformPodIP         string                 `yaml:"PlatformPodIP" json:"platformpodip"`
	PlatformCommunity     string                 `yaml:"PlatformCommunity" json:"platformpeercommunity"`
	PlatformAsn           uint32                 `yaml:"PlatformAsn" json:"platformpeerlocalasn"`
	PlatformEdgeAsn       uint32                 `yaml:"PlatformEdgeAsn" json:"platformedgelocalasn"`
	TemplateSource        string                 `yaml:"TemplateSource" json:"templatesource"`
	Access                []AccessRules          `yaml:"AccessRules,omitempty" json:"accessrules,omitempty"`
}

type AccessRules struct {
	Description      string
	Name             string
	SourceAddress    string
	DestinationRange string
	SourcePort       string
	DestinationPort  string
	Protocol         string
	Action           string
}

type DevCredentials struct {
	Username    string `yaml:"Username"`
	KeyMaterial string `yaml:"KeyMaterial"`
	IPAddress   string `yaml:"IPAddress"`
	ScpPath     string `yaml:"ScpPath"`
}

type RouterLogging struct {
	Enabled               bool   `yaml:"Enabled" json:"enabled"`
	Type                  string `yaml:"Type" json:type"`
	Stdout                bool   `yaml:"Stdout" json:"stdout"`
	Stackdriver           bool   `yaml:"Stackdriver" json:"stackdriver"`
	Elk                   bool   `yaml:"Elk" json:"elk"`
	Cloudtrail            bool   `yaml:"Cloudtrail" json:"cloudtrail"`
	Level                 string `yaml:"Level" json:"level"`
	LogfilePath           string `yaml:"LogfilePath" json:"logfilepath"`
	ElasticsearchEndpoint string `yaml:"ElasticsearchEndpoint" json:"elasticsearchendpoint"`
	LogstashEndpoint      string `yaml:"LogstashEndpoint" json:"logstashendpoint"`
	JsonFormat            bool   `yaml:"JsonFormat" json:"jsonformat"`
}

type RouterConfigIPSla struct {
	Token    string          `yaml:"Token" json:"token"`
	Services []IPSlaServices `yaml:"Services" json:"services"`
}

type Notification struct {
	// Id          int64              `gorm:"primary_key;column:id" json:"id"`
	// Method      string             `gorm:"column:method" json:"method"`
	// Host        string             `gorm:"not null;column:host" json:"host"`
	// Port        int                `gorm:"not null;column:port" json:"port"`
	// Username    string             `gorm:"not null;column:username" json:"username"`
	// Password    string             `gorm:"not null;column:password" json:"password"`
	// Var1        string             `gorm:"not null;column:var1" json:"var1"`
	// Var2        string             `gorm:"not null;column:var2" json:"var2"`
	// ApiKey      string             `gorm:"not null;column:api_key" json:"api_key"`
	// ApiSecret   string             `gorm:"not null;column:api_secret" json:"api_secret"`
	// Enabled     bool               `gorm:"column:enabled;type:boolean;default:false" json:"enabled"`
	// Limits      int                `gorm:"not null;column:limits" json:"limits"`
	// Removable   bool               `gorm:"column:removable" json:"removeable"`
	// CreatedAt   time.Time          `gorm:"column:created_at" json:"created_at"`
	// UpdatedAt   time.Time          `gorm:"column:updated_at" json:"updated_at"`
	// Form        []NotificationForm `gorm:"-" json:"form"`
	// logs        []*NotificationLog `gorm:"-" json:"logs"`
	// Title       string             `gorm:"-" json:"title"`
	// Description string             `gorm:"-" json:"description"`
	// Author      string             `gorm:"-" json:"author"`
	// AuthorUrl   string             `gorm:"-" json:"author_url"`
	// Icon        string             `gorm:"-" json:"icon"`
	// Delay       time.Duration      `gorm:"-" json:"delay,string"`
	// Queue       []*QueueData       `gorm:"-" json:"-"`
	// Running     chan bool          `gorm:"-" json:"-"`
	// Online      bool               `gorm:"-" json:"online"`
	// testable    bool               `gorm:"-" json:"testable"`
	Id          int64              `gorm:"primary_key;column:id" json:"id"`
	Method      string             `gorm:"column:method" json:"method"`
	Host        string             `gorm:"not null;column:host" json:"host"`
	Port        int                `gorm:"not null;column:port" json:"port"`
	Username    string             `gorm:"not null;column:username" json:"username"`
	Password    string             `gorm:"not null;column:password" json:"password"`
	Var1        string             `gorm:"not null;column:var1" json:"var1"`
	Var2        string             `gorm:"not null;column:var2" json:"var2"`
	ApiKey      string             `gorm:"not null;column:api_key" json:"api_key"`
	ApiSecret   string             `gorm:"not null;column:api_secret" json:"api_secret"`
	Enabled     bool               `gorm:"column:enabled;type:boolean;default:false" json:"enabled"`
	Limits      int                `gorm:"not null;column:limits" json:"limits"`
	Removable   bool               `gorm:"column:removable" json:"removeable"`
	CreatedAt   time.Time          `gorm:"column:created_at" json:"created_at"`
	UpdatedAt   time.Time          `gorm:"column:updated_at" json:"updated_at"`
	Form        []NotificationForm `gorm:"-" json:"form"`
	logs        []*NotificationLog `gorm:"-" json:"logs"`
	Title       string             `gorm:"-" json:"title"`
	Description string             `gorm:"-" json:"description"`
	Author      string             `gorm:"-" json:"author"`
	AuthorUrl   string             `gorm:"-" json:"author_url"`
	Icon        string             `gorm:"-" json:"icon"`
	Delay       time.Duration      `gorm:"-" json:"delay,string"`
	Queue       []*QueueData       `gorm:"-" json:"-"`
	Running     chan bool          `gorm:"-" json:"-"`
	Online      bool               `gorm:"-" json:"online"`
	testable    bool               `gorm:"-" json:"testable"`
}

// QueueData is the struct for the messaging queue with service
type QueueData struct {
	Id   string
	Data interface{}
}

// NotificationForm contains the HTML fields for each variable/input you want the notifier to accept.
type NotificationForm struct {
	// Type        string `json:"type"`        // the html input type (text, password, email)
	// Title       string `json:"title"`       // include a title for ease of use
	// Placeholder string `json:"placeholder"` // add a placeholder for the input
	// DbField     string `json:"field"`       // true variable key for input
	// SmallText   string `json:"small_text"`  // insert small text under a html input
	// Required    bool   `json:"required"`    // require this input on the html form
	// IsHidden    bool   `json:"hidden"`      // hide this form element from end user
	// IsList      bool   `json:"list"`        // make this form element a comma separated list
	// IsSwitch    bool   `json:"switch"`      // make the notifier a boolean true/false switch
	Type        string `json:"type"`        // the html input type (text, password, email)
	Title       string `json:"title"`       // include a title for ease of use
	Placeholder string `json:"placeholder"` // add a placeholder for the input
	DbField     string `json:"field"`       // true variable key for input
	SmallText   string `json:"small_text"`  // insert small text under a html input
	Required    bool   `json:"required"`    // require this input on the html form
	IsHidden    bool   `json:"hidden"`      // hide this form element from end user
	IsList      bool   `json:"list"`        // make this form element a comma separated list
	IsSwitch    bool   `json:"switch"`      // make the notifier a boolean true/false switch
}

// NotificationLog contains the normalized message from previously sent notifications
type NotificationLog struct {
	// Message   string          `json:"message"`
	// Time      utils.Timestamp `json:"time"`
	// Timestamp time.Time       `json:"timestamp"`
	Message   string          `json:"message"`
	Time      utils.Timestamp `json:"time"`
	Timestamp time.Time       `json:"timestamp"`
}

type IPSlaServices struct {
	// Id                 int64          `gorm:"primary_key;column:id" json:"id"`
	// Name               string         `gorm:"column:name" json:"name"`
	// Domain             string         `gorm:"column:domain" json:"domain"`
	// Expected           string         `gorm:"column:expected" json:"expected"`
	// ExpectedStatus     int            `gorm:"default:200;column:expected_status" json:"expected_status"`
	// Interval           int            `gorm:"default:30;column:check_interval" json:"check_interval"`
	// Type               string         `gorm:"column:check_type" json:"type"`
	// Method             string         `gorm:"column:method" json:"method"`
	// PostData           string         `gorm:"column:post_data" json:"post_data"`
	// Port               int            `gorm:"not null;column:port" json:"port"`
	// Timeout            int            `gorm:"default:30;column:timeout" json:"timeout"`
	// Order              int            `gorm:"default:0;column:order_id" json:"order_id"`
	// AllowNotifications bool           `gorm:"default:true;column:allow_notifications" json:"allow_notifications"`
	// Public             bool           `gorm:"default:true;column:public" json:"public"`
	// GroupId            int            `gorm:"default:0;column:group_id" json:"group_id"`
	// Headers            string         `gorm:"column:headers" json:"headers"`
	// Permalink          string         `gorm:"column:permalink" json:"permalink"`
	// CreatedAt          time.Time      `gorm:"column:created_at" json:"created_at"`
	// UpdatedAt          time.Time      `gorm:"column:updated_at" json:"updated_at"`
	// Online             bool           `gorm:"-" json:"online"`
	// Latency            float64        `gorm:"-" json:"latency"`
	// PingTime           float64        `gorm:"-" json:"ping_time"`
	// Online24Hours      float32        `gorm:"-" json:"online_24_hours"`
	// AvgResponse        string         `gorm:"-" json:"avg_response"`
	// Running            bool           `gorm:"-" json:"running"`
	// Checkpoint         time.Time      `gorm:"-" json:"checkpoint"`
	// SleepDuration      time.Duration  `gorm:"-" json:"sleep_duration"`
	// LastResponse       string         `gorm:"-" json:"last_response"`
	// LastStatusCode     int            `gorm:"-" json:"status_code"`
	// LastOnline         time.Time      `gorm:"-" json:"last_success"`
	// Failures           []IPSlaFailure `gorm:"-" json:"failures"`
	Id                 int64     `yaml:"Id" gorm:"primary_key;column:id" json:"id"`
	Name               string    `yaml:"Name" gorm:"column:name" json:"name"`
	Domain             string    `yaml:"Domain" gorm:"column:domain" json:"domain"`
	Expected           string    `yaml:"Expected" gorm:"column:expected" json:"expected"`
	ExpectedStatus     int       `yaml:"ExpectedStatus" gorm:"default:200;column:expected_status" json:"expected_status"`
	Interval           int       `yaml:"Interval" gorm:"default:30;column:check_interval" json:"check_interval"`
	Type               string    `yaml:"Type" gorm:"column:check_type" json:"type"`
	Method             string    `yaml:"Method" gorm:"column:method" json:"method"`
	PostData           string    `yaml:"PostData" gorm:"column:post_data" json:"post_data"`
	Port               int       `yaml:"Port" gorm:"not null;column:port" json:"port"`
	Timeout            int       `yaml:"Timeout" gorm:"default:30;column:timeout" json:"timeout"`
	Order              int       `yaml:"Order" gorm:"default:0;column:order_id" json:"order_id"`
	AllowNotifications bool      `yaml:"AllowNotifications" gorm:"default:true;column:allow_notifications" json:"allow_notifications"`
	Public             bool      `yaml:"Public" gorm:"default:true;column:public" json:"public"`
	GroupId            int       `yaml:"GroupId" gorm:"default:0;column:group_id" json:"group_id"`
	Headers            string    `yaml:"Headers" gorm:"column:headers" json:"headers"`
	Permalink          string    `yaml:"Permalink" gorm:"column:permalink" json:"permalink"`
	CreatedAt          time.Time `yaml:"CreatedAt" gorm:"column:created_at" json:"created_at"`
	UpdatedAt          time.Time `yaml:"UpdatedAt" gorm:"column:updated_at" json:"updated_at"`
	Online             bool      `yaml:"Online" gorm:"-" json:"online"`
	Latency            float64   `yaml:"Latency" gorm:"-" json:"latency"`
	PingTime           float64   `yaml:"PingTime" gorm:"-" json:"ping_time"`
	Online24Hours      float32   `yaml:"Online24Hours" gorm:"-" json:"online_24_hours"`
	AvgResponse        string    `yaml:"AvgResponse" gorm:"-" json:"avg_response"`
	// Running            bool           `yaml:"Running" gorm:"-" json:"-"`
	Checkpoint     time.Time      `yaml:"Checkpoint" gorm:"-" json:"-"`
	SleepDuration  time.Duration  `yaml:"SleepDuration" gorm:"-" json:"-"`
	LastResponse   string         `yaml:"LastResponse" gorm:"-" json:"-"`
	LastStatusCode int            `yaml:"LastStatusCode" gorm:"-" json:"status_code"`
	LastOnline     time.Time      `yaml:"LastOnline" gorm:"-" json:"last_success"`
	Failures       []IPSlaFailure `yaml:"Failures" gorm:"-" json:"failures"`
}

type IPSlaFailure struct {
	ID        int       `yaml:"ID" json:"id"`
	Issue     string    `yaml:"Issue" json:"issue"`
	ErrorCode int       `yaml:"ErrorCode" json:"error_code"`
	Ping      float64   `yaml:"Ping" json:"ping"`
	CreatedAt time.Time `yaml:"CreatedAt" json:"created_at"`
}

type IPSlaWebhook struct {
	ServiceID     string `yaml:"ServiceID" json:"service-id"`
	ServiceName   string `yaml:"ServiceName" json:"service-name"`
	ServiceStatus string `yaml:"ServiceStatus" json:"service-status"`
	Failure       string `yaml:"Failure" json:"failure"`
}

type HAPeers struct {
	Address     string    `yaml:"Address" json:"address"`
	TargetIP    string    `yaml:"TargetIP" json:"targetip"`
	Name        string    `yaml:"Name" json:"name"`
	Healthy     bool      `yaml:"Healthy" json:"healthy"`
	HaAdminUp   bool      `yaml:"HaAdminUp" json:"haadminup"`
	InstanceID  string    `yaml:"InstanceID" json:"instanceid"`
	LastHealthy time.Time `yaml:"LastHealthy" json:"lasthealthy"`
}

type AttachedSubnets struct {
	NumberIPAddresses     int      `yaml:"NumberIPAddresses" json:"numberipaddresses"`
	NumberAddressableHost int      `yaml:"NumberAddressableHost" json:"numberaddressablehost"`
	IPAddressRange        []string `yaml:"IPAddressRange" json:"ipaddressrange"`
	BroadcastAddress      string   `yaml:"BroadcastAddress" json:"broadcastaddress"`
	DeviceIPAddress       string   `yaml:"DeviceIPAddress" json:"deviceipaddress"`
	SubnetMask            string   `yaml:"SubnetMask" json:"subnetmask"`
	NetworkPortion        string   `yaml:"NetworkPortion" json:"networkportion"`
	HostPortion           string   `yaml:"HostPortion" json:"hostportion"`
	CIDR                  string   `yaml:"CIDR" json:"cidr"`
	BGPAdvertise          bool     `yaml:"BGPAdvertise" json:"bgpadvertise"`
	CommunityStrings      []string `yaml:"CommunityStrings" json:"communitystrings"`
	VRF                   string   `yaml:"VRF" json:"vrf"`
}
type InterfaceConfig struct {
	Name    string `yaml:"Name" json:"name"`
	Address string `yaml:"Address" json:"address"`
	Primary bool   `yaml:"Primary" json:"primary"`
	CIDR    string `yaml:"CIDR" json:"cidr"`
}

type RouterDebug struct {
	GcpProfiler bool `yaml:"GcpProfiler" json:"gcpprofiler"`
	PprofStdout bool `yaml:"PprofStdout" json:"pprofstdout"`
}

type RouterPlugins struct {
	Cloud      RouterCloudPlugins      `yaml:"Cloud" json:"cloud"`
	Mobile     RouterMobilePlugins     `yaml:"Mobile" json:"mobile"`
	Networking RouterNetworkingPlugins `yaml:"Networking" json:"networking"`
	Automation RouterAutomationPlugins `yaml:"Automation" json:"automation"`
}

type RouterAutomationPlugins struct {
	Terraform bool `yaml:"Terraform" json:"terraform"`
}

type RouterMobilePlugins struct {
	Enabled bool   `yaml:"Enabled" json:"enabled"`
	Service string `yaml:"Service" json:"service"`
	Carrier string `yaml:"Carrier" json:"carrier"`
	Device  string `yaml:"Device" json:"device"`
	Band    string `yaml:"Band" json:"band"`
}

type RouterNetworkingPlugins struct {
	Enabled    bool                    `yaml:"Enabled" json:"enabled"`
	Interfaces []network.InterfaceInfo `yaml:"Interface" json:"interface"`
}

type NetplanInterfaces struct {
}

type RouterCloudPlugins struct {
	Providers RouterCloudProvider `yaml:"Providers" json:"providers"`
	Gcp       *CloudProviderGcp   `yaml:"Gcp" json:"gcp"`
	Aws       *CloudProviderAws   `yaml:"Aws" json:"aws"`
	Azure     *CloudProviderAzure `yaml:"Azure" json:"azure"`
}

type CloudProviderGcp struct {
	Provider              string           `yaml:"Provider" json:"provider"`
	AuthFile              string           `yaml:"AuthFile" json:"authfile"`
	Project               string           `yaml:"Project" json:"project"`
	EncryptedAuthFilePath string           `yaml:"EncryptedAuthFilePath" json:"encryptedauthfilepath"`
	Routes                []ProviderRoutes `yaml:"Routes" json:"routes"`
	Zone                  string           `yaml:"Zone" json:"zone"`
	Region                string           `yaml:"Region" json:"region"`
	Instance              string           `yaml:"Instance" json:"instance"`
	Svc                   *compute.Service `yaml:"Svc" json:"svc"`
	Ctx                   *context.Context `yaml:"Ctx" json:"ctx"`
	Http                  *http.Client     `yaml:"Http" json:"http"`
	Network               string           `yaml:"Network" json:"network"`
	Selfink               string           `yaml:"Selflink" json:"selflink"`
	// IP is the address we will target routes at. Only mandatory and non guessable argument.
	IP string `yaml:"IP" json:"ip"`

	// Hoster (AWS or GCP) can be guessed automaticaly when we run on an instance
	Hoster string `yaml:"Hoster" json:"hoster"`

	// When DryRun is true, we don't really apply changes
	DryRun bool `yaml:"DryRun" json:"dryrun"`

	// When Quiet is true, we only display errors
	Quiet bool `yaml:"Quiet" json:"quiet"`

	// Ignore tables associated with the main route table
	NoMain bool `yaml:"NoMain" json:"nomain"`

	// Interface ID
	Iface string `yaml:"Iface" json:"ifance"`

	// Subnet ID
	Subnet string `yaml:"Subnet" json:"subnet"`

	// Target private IP
	TargetIP string `yaml:"TargetIP" json:"targetip"`

	// Restricted set of AWS route tables
	RouteTables []string `yaml:"RouteTables" json:"routetables"`

	// AwsAccesKeyID (AWS only) is the acccess key to use (if we don't use an instance profile's role)
	AwsAccessKeyID string `yaml:"AwsAccessKeyID" json:"awsaccesskeyid"`

	// AwsSecretKey (AWS only) is the secret key to use (if we don't use an instance profile's role)
	AwsSecretKey string `yaml:"AwsSecretKey" json:"awssecretkey"`
}

type ProviderRoutes struct {
	Prefix             string `yaml:"Prefix" json:"prefix"`
	NexthopType        string `yaml:"NexthopType" json:"nexthoptype"`
	NexthopIP          string `yaml:"NexthopIP" json:"nexthopip"`
	NexthopInstance    string `yaml:"NexthopInstance" json:"nexthopinstance"`
	Network            string `yaml:"Network" json:"network"`
	DestinationIPRange string `yaml:"DestinationIPRange" json:"destinationiprange"`
	Priority           string `yaml:"Priority" json:"priority"`
	InstanceTags       string `yaml:"InstanceTags" json:"instancetags"`
	Name               string `yaml:"Name" json:"name"`
}

type CloudProviderAws struct {
	Provider string           `yaml:"Provider" json:"provider"`
	AuthFile string           `yaml:"AuthFile" json:"authfile"`
	Routes   []ProviderRoutes `yaml:"Routes" json:"routes"`
	Zone     string           `yaml:"Zone" json:"zone"`
	Region   string           `yaml:"Region" json:"region"`
	Instance string           `yaml:"Instance" json:"instance"`
	Svc      *compute.Service `yaml:"Svc" json:"svc"`
	Ctx      *context.Context `yaml:"Ctx" json:"ctx"`
	Http     *http.Client     `yaml:"Http" json:"http"`
	Network  string           `yaml:"Network" json:"network"`
	Selfink  string           `yaml:"Selflink" json:"selflink"`
	Primary  bool             `yaml:"Primary" json:"primary"`
	// IP is the address we will target routes at. Only mandatory and non guessable argument.
	IP string `yaml:"IP" json:"ip"`

	// Hoster (AWS or GCP) can be guessed automaticaly when we run on an instance
	Hoster string `yaml:"Hoster" json:"hoster"`

	// When DryRun is true, we don't really apply changes
	DryRun bool `yaml:"DryRun" json:"dryrun"`

	// When Quiet is true, we only display errors
	Quiet bool `yaml:"Quiet" json:"quiet"`

	// Ignore tables associated with the main route table
	NoMain bool `yaml:"NoMain" json:"nomain"`

	// Interface ID
	Iface string `yaml:"Iface" json:"ifance"`

	// Subnet ID
	Subnet string `yaml:"Subnet" json:"subnet"`

	// Target private IP
	TargetIP string `yaml:"TargetIP" json:"targetip"`

	// Restricted set of AWS route tables
	RouteTables []string `yaml:"RouteTables" json:"routetables"`

	// AwsAccesKeyID (AWS only) is the acccess key to use (if we don't use an instance profile's role)
	AwsAccesKeyID string `yaml:"AwsAccessKeyID" json:"awsaccesskeyid"`

	// AwsSecretKey (AWS only) is the secret key to use (if we don't use an instance profile's role)
	AwsSecretKey string `yaml:"AwsSecretKey" json:"awssecretkey"`
}

type CloudProviderAzure struct {
	Provider string
}

type RouterCloudProvider struct {
	Gcp   bool `yaml:"Gcp" json:"gcp"`
	Aws   bool `yaml:"Aws" json:"aws"`
	Azure bool `yaml:"Azure" json:"azure"`
}

// CfiConfig is the configuration structucture
type CfiConfig struct {
	// IP is the address we will target routes at. Only mandatory and non guessable argument.
	IP string

	// Hoster (AWS or GCP) can be guessed automaticaly when we run on an instance
	Hoster string

	// Instance name or ID
	Instance string

	// When DryRun is true, we don't really apply changes
	DryRun bool

	// When Quiet is true, we only display errors
	Quiet bool

	// Project (GCP only) identifies the Google Project (guessed on instance)
	Project string

	// Zone is the AWS or GCP zone of the target instance
	Zone string

	// Region is the AWS region
	Region string

	// Ignore tables associated with the main route table
	NoMain bool

	// Interface ID
	Iface string

	// Subnet ID
	Subnet string

	// Target private IP
	TargetIP string

	// Restricted set of AWS route tables
	RouteTables []string

	// AwsAccesKeyID (AWS only) is the acccess key to use (if we don't use an instance profile's role)
	AwsAccesKeyID string

	// AwsSecretKey (AWS only) is the secret key to use (if we don't use an instance profile's role)
	AwsSecretKey string
}

type RouterServices struct {
	IPam                  bool                 `yaml:"IPam" json:"ipam"`
	Api                   bool                 `yaml:"Api" json:"api"`
	Vxlan                 bool                 `yaml:"Vxlan" json:"vxlan"`
	ScopeDashboard        bool                 `yaml:"ScopeDashboard" json:"scopedashboard"`
	Netdata               bool                 `yaml:"Netdata" json:"netdata"`
	Cadvisor              bool                 `yaml:"Cadvisor" json:"cadvisor"`
	Bgp                   bool                 `yaml:"Bgp" json:"bgp"`
	Prometheus            bool                 `yaml:"Prometheus" json:"prometheus"`
	Slack                 bool                 `yaml:"Slack" json:"slack"`
	Scope                 bool                 `yaml:"Scope" json:"scope"`
	Snmp                  bool                 `yaml:"Snmp" json:"snmp"`
	Nat                   bool                 `yaml:"Nat" json:"nat"`
	DynamicSnat           bool                 `yaml:"DynamicSnat" json:"dynamicsnat"`
	Loadbalancer          bool                 `yaml:"Loadbalancer" json:"loadbalancer"`
	Traefik               bool                 `yaml:"Traefik" json:"traefik"`
	Teleport              bool                 `yaml:"Teleport" json:"teleport"`
	Mqtt                  bool                 `yaml:"Mqtt" json:"mqtt"`
	HostApd               bool                 `yaml:"HostApd" json:"hostapd"`
	Ips                   bool                 `yaml:"Ips" json:"ips"`
	IPSla                 bool                 `yaml:"IPSla" json:"ipsla"`
	C3                    bool                 `yaml:"C3" json:"c3"`
	K8s                   bool                 `yaml:"K8s" "json:"k8s"`
	Configs               RouterServiceConfigs `yaml:"Configs" json:"configs"`
	Status                RouterServiceStatus  `yaml:"Status" json:"status"`
	QuickStatus           []ServiceStatus      `yaml:"QuickStatus" json:"quickstatus"`
	SSHUsername           string               `yaml:"SSHUsername" json:"sshusername"`
	SSHPublicKeyMaterial  string               `yaml:"SSHPublicKeyMaterial" json:"sshpublickeymaterial"`
	SSHPrivateKeyMaterial string               `yaml:"SSHPrivateKeyMaterial" json:"sshprivatekeymaterial"`
	SSHKeyPath            string               `yaml:"SSHKeyPath" json:"sshkeypath"`
}

type RouterServiceStatus struct {
	Vxlan        WeaveStatusReport  `yaml:"Vxlan" json:"vxlan"`
	Weave        ServiceStatus      `yaml:"Weave" json:"weave"`
	Bgp          BgpStatus          `yaml:"Bgp" json:"bgp"`
	Cadvisor     CadvisorStatus     `yaml:"Cadvisor" json:"cadvisor"`
	WeaveScope   WeaveScopeStatus   `yaml:"WeaveScope" json:"weavescope"`
	Netdata      NetdataStatus      `yaml:"Netdata" json:"netdata"`
	Jobs         JobsStatus         `yaml:"Jobs" json:"jobs"`
	Api          ApiStatus          `yaml:"Api" json:"api"`
	MqttState    MqttStateStatus    `yaml:"MqttState" json:"mqttstate"`
	HostApd      HostApdStatus      `yaml:"HostApd" json:"hostapd"`
	Loadbalancer LoadbalancerStatus `yaml:"Loadbalancer" json:"loadbalancer"`
	Traefik      TraefikStatus      `yaml:"Traefik" json:"traefik"`
	Ips          IpsStateStatus     `yaml:"Ips" json:"ips"`
	IPSla        IPSlaStateStatus   `yaml:"IPSla" json:"ipsla"`
	K8sConnector K8sConnectorStatus `yaml:"K8HsConnector" json:"k8sconnector"`
	HaAdmin      HaAdminStatus      `yaml:"HaAdmin" json:"haadmin"`
	HaInfo       HaInfo             `yaml:"HaInfo" json:"hainfo"`
	HaStatus     HaStatus           `yaml:"HaStatus" json:"hastatus"`
	HostApi      HostApiStatus      `yaml:"HostApi" json:"hostapi"`
	Snat         SnatStatus         `yaml:"Snat" json:"snat"`
	Exitmon      ExitmonStatus      `yaml:"Exitmon" json:"exitmon"`
	Cfgctl       ServiceStatus      `yaml:"Cfgctl" json:"cfgctl"`
	Kubewatch    ServiceStatus      `yaml:"Kubewatch" json:"kubewatch"`
	CfgctlApi    ServiceStatus      `yaml:"CfgctlApi" json:"cfgctlapi"`
	Echo         ServiceStatus      `yaml:"Echo" json:"echo"`
	Routectl     ServiceStatus      `yaml:"Routectl" json:"routectl"`
	Hactl        ServiceStatus      `yaml:"Hactl" json:"hactl"`
}

type DynamicSnat struct {
	Provider            string             `yaml:"Provider" json:"provider"`
	Gke                 bool               `yaml:"Gke" json:"gke"`
	Eks                 bool               `yaml:"Eks" json:"eks"`
	GkeAliasIP          bool               `yaml:"GkeAliasIP" json:"gkealiasip"`
	SnatPods            bool               `yaml:"SnatPods" json:"snatpods"`
	ClusterName         string             `yaml:"ClusterName" json:"clustername"`
	Region              string             `yaml:"Region" json:"region"`
	Zone                string             `yaml:"Zone" json:"zone"`
	PodCidr             string             `yaml:"PodCidr" json:"podcidr"`
	ProjectID           string             `yaml:"ProjectID" json:"projectid"`
	SnatServices        bool               `yaml:"SnatServices" json:"snatservices"`
	ServiceCidr         string             `yaml:"ServiceCidr" json:"servicecidr"`
	SnatNodes           bool               `yaml:"SnatNodes" json:"snatnodes"`
	FilterIPDestination bool               `yaml:"FilterIPDestination" json:"filteripdestination"`
	PollingInterval     string             `yaml:"PollingInterval" json:"pollinginterval"`
	KubeconfigPath      string             `yaml:"KubeconfigPath" json:"kubeconfigpath"`
	GkeInfo             *container.Cluster `yaml:"GkeInfo" json:"gkeinfo"`
}

type ServiceStatus struct {
	Name    string          `yaml:"Name" json:"name"`
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `yaml:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}

type SnatStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `yaml:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
	Nodes   []string        `yaml:"Nodes" json:"nodes"`
}

type ExitmonStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `yaml:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}

type HostApiStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `yaml:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}

type HaStatus struct {
	Details    types.Container  `yaml:"Details" json:"details"`
	Peers      []HaStatusPeers  `yaml:"HaStatusPeers" json:"hastatuspeers"`
	Routes     []HaStatusRoutes `yaml:"HaStatusRoutes" json:"hastatusroutes"`
	Ready      bool             `mapstructure:"Ready" json:"ready"`
	HASetLabel string           `yaml:"HASetLabel" json:"hasetlable"`
	Status     string           `yaml:"Status" json:"status"`
	State      string           `yaml:"State" json:"state"`
}

type HaStatusPeers struct {
	Name      string `yaml:"Name" json:"name"`
	IP        string `yaml:"IP" json:"ip"`
	Healthy   bool   `yaml:"Healthy" json:"healthy"`
	HaAdminUp bool   `yaml:"HaAdminUp" json:"haadminup"`
}

type HaStatusRoutes struct {
	Prefix  string `yaml:"Prefix" json:"prefix"`
	Healthy bool   `yaml:"Healthy" json:"healthy"`
}

type HaInfo struct {
	Address    string           `yaml:"Address" json:"address"`
	InstanceID string           `yaml:"InstanceID" json:"instanceid"`
	Name       string           `yaml:"Name" json:"name"`
	HaSetLabel string           `yaml:"HaSetLabel" json:"hasetlabel"`
	TargetIP   string           `yaml:"TargetIP" json:"targetip"`
	Routes     []ProviderRoutes `yaml:"Routes" json:"routes"`
}

type HaAdminStatus struct {
	Ready  bool   `mapstructure:"Ready" json:"ready"`
	Status string `yaml:"Status" json:"status"`
	State  string `yaml:"State" json:"state"`
}

type K8sConnectorStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}

type LoadbalancerStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}

type TraefikStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}

type HostApdStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}

type BgpStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}
type CadvisorStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}
type WeaveScopeStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}
type NetdataStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}
type JobsStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}
type ApiStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}
type MqttStateStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}
type IpsStateStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}
type IPSlaStateStatus struct {
	Details types.Container `yaml:"Details" json:"details"`
	Ready   bool            `mapstructure:"Ready" json:"ready"`
	Status  string          `yaml:"Status" json:"status"`
	State   string          `yaml:"State" json:"state"`
}

type WeaveStatusReport struct {
	Details      types.Container    `yaml:"Details" json:"details"`
	Ready        bool               `mapstructure:"Ready" json:"ready"`
	Version      string             `mapstructure:"Version" json:"version"`
	VersionCheck WeaveServiceStatus `mapstructure:"VersionCheck" json:"versioncheck"`
	Router       WeaveRouter        `mapstructure:"WeaveRouter" json:"weaverouter"`
	IPAM         WeaveIpamEntries   `mapstructure:"WeaveIpamEntries" json:"weaveipamentries"`
	DNS          WeaveDns           `mapstructure:"WeaveDns" json:"weavedns"`
}

type WeaveServiceStatus struct {
	Enabled     bool   `mapstructure:"Enabled" json:"Enabled"`
	Success     bool   `mapstructure:"Success" json:"Success"`
	NewVersion  string `mapstructure:"NewVersion" json:"NewVersion"`
	NextCheckAt string `mapstructure:"NextCheckAt" json:"NextCheckAt"`
}

type WeaveRouter struct {
	Protocol           string       `mapstructure:"Protocol" json:"Protocol"`
	ProtocolMinVersion int          `mapstructure:"ProtocolMinVersion" json:"ProtocolMinVersion"`
	ProtocolMaxVersion int          `mapstructure:"ProtocolMaxVersion" json:"ProtocolMaxVersion"`
	Encryption         bool         `mapstructure:"Encryption" json:"Encryption"`
	PeerDiscovery      bool         `mapstructure:"PeerDiscovery" json:"PeerDiscovery"`
	Name               string       `mapstructure:"Name" json:"Name"`
	NickName           string       `mapstructure:"NickName" json:"NickName"`
	Por                int          `mapstructure:"Por" json:"Por"`
	Peers              []WeavePeers `mapstructure:"Peers" json:"Peers"`
}

type WeavePeers struct {
	Name        string             `mapstructure:"Name" json:"Name"`
	NickName    string             `mapstructure:"NickName" json:"NickName"`
	UID         uint64             `mapstructure:"UID" json:"UID"`
	ShortID     int                `mapstructure:"ShortID" json:"ShortID"`
	Version     int                `mapstructure:"Version" json:"Version"`
	Connections []WeaveConnections `mapstructure:"Connections" json:"Connections"`
}

type WeaveConnections struct {
	Name        string `mapstructure:"Name" json:"Name"`
	NickName    string `mapstructure:"NickName" json:"NickName"`
	Address     string `mapstructure:"Address" json:"Address"`
	Outbound    bool   `mapstructure:"Outbound" json:"Outbound"`
	Established bool   `mapstructure:"Established" json:"Established"`
}

type WeaveIpamEntries struct {
	Token       string `mapstructure:"Token" json:"Token"`
	Size        int    `mapstructure:"Size" json:"Size"`
	Peer        string `mapstructure:"Peer" json:"Peer"`
	Nickname    string `mapstructure:"Nickname" json:"Nickname"`
	IsKnownPeer bool   `mapstructure:"IsKnownPeer" json:"IsKnownPeer"`
	Version     string `mapstructure:"Version" json:"Version"`
}

type WeaveDns struct {
	Domain   string            `mapstructure:"Domain" json:"domain"`
	Upstream []string          `mapstructure:"Upstream" json:"upstream"`
	Address  string            `mapstructure:"Address" json:"address"`
	TTL      uint32            `mapstructure:"TTL" json:"ttl"`
	Entries  []WeaveDnsEntries `mapstructure:"Entries" json:"entries"`
}

type WeaveDnsEntries struct {
	Hostname  string `mapstructure:"Hostname" json:"hostname"`
	Origin    string `mapstructure:"Origin" json:"origin"`
	Address   string `mapstructure:"Address" json:"address"`
	Version   int    `mapstructure:"Version" json:"version"`
	Tombstone int64  `mapstructure:"Tomebstone" json:"tombstone"`
}

type RouterServiceConfigs struct {
	Api          RouterServiceConfigsApi            `yaml:"Api" json:"api"`
	Bgp          RouterServiceConfigsBgp            `yaml:"Bgp" json:"bgp"`
	Nat          RouterServiceConfigsNat            `yaml:"Nat" json:"nat"`
	Routes       RouterRoutes                       `yaml:"Routes" json:"routes"`
	Slack        RouterServiceConfigsSlack          `yaml:"Slack" json:"slack"`
	Router       RouterServiceConfigsRouter         `yaml:"Router" json:"router"`
	Scheduler    RouterServiceConfigsScheduler      `yaml:"Scheduler" json:"scheduler"`
	Scope        RouterServiceConfigsScope          `yaml:"Scope" json:"scope"`
	Snmp         RouterServiceConfigsSnmp           `yaml:"Snmp" json:"snmp"`
	Teleport     RouterServiceConfigsTeleport       `yaml:"Teleport" json:"teleport"`
	Loadbalancer []RouterServiceConfigsLoadbalancer `yaml:"Loadbalancer" json:"loadbalancer"`
	Traefik      RouterServiceConfigsTraefik        `yaml:"Traefik" json:"traefik"`
	Mqtt         RouterServiceConfigsMqtt           `yaml:"Mqtt" json:"mqtt"`
	HostApd      RouterServiceConfigsHostApd        `yaml:"HostApd" json:"hostapd"`
	Ips          RouterServiceConfigsIps            `yaml:"Ips" json:"ips"`
	IPSla        RouterConfigIPSla                  `yaml:"IPSla" json:"ipsla"`
	IPam         RouterServiceConfigsIPam           `yaml:"IPam" json:"ipam"`
	Prometheus   RouterServiceConfigsPrometheus     `yaml:"Prometheus" json:"prometheus"`
	K8s          RouterServiceConfigsK8s            `yaml:"K8s" json:"k8s"`
	DynamicSnat  []DynamicSnat                      `yaml:"DynamicSnat" json:"dynamicsnat"`
}

type RouterServiceConfigsK8s struct {
	Kubeconfig         string `yaml:"Kubeconfig" json:"kubeconfig"`
	CfgctlVersion      string `yaml:"CfgctlVersion" json:"cfgctlversion"`
	Namespace          string `yaml:"Namespace" json:"namespace"`
	Name               string `yaml:"Name" json:"name"`
	ConfigmapName      string `yaml:"ConfigmapName" json:"configmapname"`
	HelmDeploymentName string `yaml:"HelmDeploymentName" json:"helmdeploymentname"`
	WeavePskSecret     string `yaml:"WeavePskSecret" json:"weavepsksecret"`
	ApiSecret          string `yaml:"ApiSecret" json:"apisecret"`
	MqttSecret         string `yaml:"MqttSecret" json:"mqttsecret"`
	ScopeSecret        string `yaml:"ScopeSecret" json:"scopesecret"`
	ServiceUUID        string `yaml:"ServiceUUID" json:"serviceuuid"`
	PublicIP           string `yaml:"PublicIP" json:"publicip"`
	ServiceLink        string `yaml:"ServiceLink" json:"servicelink"`
	CredentialProvider string `yaml:"CredentialProvider" json:"credentialprovider"`
}

type RouterServiceConfigsIPam struct {
	Session IPamSession `yaml:"Session" json:"session"`
}

type IPamServiceAPITokenResp struct {
	Code    int                `yaml:"Code" json:"code"`
	Success bool               `yaml:"Success" json:"success"`
	Data    IPamServiceAPIData `yaml:"Data" json:"data"`
	Time    float64            `yaml:"Time" json:"time"`
}

type IPamServiceAPIAddressResp struct {
	Code int `yaml:"Code" json:"code"`
	// Success BoolIntString `yaml:"Success" json:"success"`
	Data    []IPamAddress `yaml:"Data" json:"data"`
	Time    float64       `yaml:"Time" json:"time"`
	Message string        `yaml:"Message" json:"message"`
}

type IPamServiceAPISubnetResp struct {
	Code int `yaml:"Code" json:"code"`
	// Success bool         `yaml:"Success" json:"success"`
	Data []IPamSubnet `yaml:"Data" json:"data"`
	Time float64      `yaml:"Time" json:"time"`
}

type IPamServiceAPINextIPResp struct {
	Code int `yaml:"Code" json:"code"`
	// Success bool    `yaml:"Success" json:"success"`
	Data string  `yaml:"Data" json:"data"`
	Time float64 `yaml:"Time" json:"time"`
}

type IPamServiceAPIDevicesResp struct {
	Code int `yaml:"Code" json:"code"`
	// Success bool          `yaml:"Success" json:"success"`
	Data []IPamDevices `yaml:"Data" json:"data"`
	Time float64       `yaml:"Time" json:"time"`
}

type IPamServiceAPIDeviceResp struct {
	Code int `yaml:"Code" json:"code"`
	// Success bool        `yaml:"Success" json:"success"`
	Data IPamDevices `yaml:"Data" json:"data"`
	Time float64     `yaml:"Time" json:"time"`
}

type IPamServiceAPICreateNextIPResp struct {
	Code int `yaml:"Code" json:"code"`
	// Success bool    `yaml:"Success" json:"success"`
	Data    string  `yaml:"Data" json:"data"`
	ID      string  `yaml:"ID" json:"id"`
	Time    float64 `yaml:"Time" json:"time"`
	Message string  `yaml:"Messsage" json:"message"`
}

type IPamServiceAPIData struct {
	Token   string `yaml:"Token" json:"token"`
	Expires string `yaml:"Expires" json:"expires"`
}

type IPamAPICall struct {
	Method  string      `yaml:"Method" json:"method"`
	Url     string      `yaml:"Url" json:"url"`
	Cidr    string      `yaml:"Cidr" json:"cidr"`
	Id      int         `yaml:"Id" json:"id"`
	Payload interface{} `yaml:"Payload" json:"payload"`
	IPaddr  string      `yaml:"IPaddr" json:"ipaddr"`
}

type BoolIntString bool

type IPamConfig struct {
	// The application ID required for API requests. This needs to be created in
	// the PHPIPAM console.
	AppID string `yaml:"AppID" json:"appid"`

	// The API endpoint.
	Endpoint string `yaml:"Endpoint" json:"endpoint"`

	// The password for the PHPIPAM account.
	Password string `yaml:"Password" json:"password"`

	// The user name for the PHPIPAM account.
	Username   string `yaml:"Username" json:"username"`
	AuthHeader string `yaml:"AuthHeader" json:"authheader"`
}

type IPamClient struct {
	// The session for this client.
	Session *IPamSession `yaml:"Session" json:"session"`
}

type IPamController struct {
	IPamClient `yaml:"Client" json:"client"`
}

// Address represents an IP address resource within
type IPamAddress struct {
	// The ID of the IP address entry within PHPIPAM.
	ID int `json:"id,string"`

	// The ID of the subnet that the address belongs to.
	SubnetID string `json:"subnetId"`

	// The IP address, without a CIDR subnet mask.
	IPAddress string `json:"ip"`

	// true if this IP address is a gateway address.
	IsGateway BoolIntString `json:"is_gateway"`

	// A detailed description of the IP address entry.
	Description string `json:"description"`

	// A hostname for the IP address.
	Hostname string `json:"hostname"`

	// The MAC address for the IP.
	MACAddress string `json:"mac"`

	// The address owner (customer, hostname, application, etc).
	Owner string `json:"owner"`

	// The tag ID for the IP address.
	Tag int `json:"tag,string"`

	// true if PTR records should not be created for this IP address.
	PTRIgnore BoolIntString `json:"PTRIgnore,string"`

	// The ID of a PowerDNS PTR record.
	PTRRecordID int `json:"PTR,string"`

	// An ID of a device that this address belongs to.
	DeviceID int `json:"deviceId,string"`

	// A switchport number/label that this IP address belongs to.
	Port string `json:"port"`

	// A note for this IP address, detailing state information not sutiable for
	// entering in the description.
	Note string `json:"note"`

	// A timestamp for when the address was last seen with ping.
	LastSeen string `json:"lastSeen"`

	// true if you want to exclude this address from ping scans.
	ExcludePing BoolIntString `json:"excludePing,string"`

	// The date of the last edit to this resource.
	EditDate string `json:"editDate"`

	// A map[string]interface{} of custom fields to set on the resource. Note
	// that this functionality requires PHPIPAM 1.3 or higher with the "Nest
	// custom fields" flag set on the specific API integration. If this is not
	// enabled, this map will be nil on GETs and POSTs and PATCHes with this
	// field set will fail. Use the explicit custom field functions instead.
	CustomFields map[string]interface{} `json:"custom_fields"`
}

// Address represents an IP address resource within
type IPamDevices struct {
	// The ID of the IP address entry within PHPIPAM.
	ID int `json:"id,string"`

	// The ID of the subnet that the address belongs to.
	Hostname string `json:"hostname"`

	// The IP address, without a CIDR subnet mask.
	IPAddress string `json:"ip"`

	// true if this IP address is a gateway address.
	Type string `json:"type"`

	// A detailed description of the IP address entry.
	Description string `json:"description"`

	Sections           string `json:"sections"`
	Rack               string `json:"rack"`
	RackStart          string `json:"rack_start"`
	RackSize           string `json:"rack_size"`
	Location           string `json:"location"`
	SnmpCommunity      string `json:"snmp_community"`
	SnmpVersion        string `json:"snmp_version"`
	SnmpPort           string `json:"snmp_port"`
	SnmpTimeout        string `json:"snmp_timeout"`
	SnmpQueries        string `json:"snmp_queries"`
	SnmpV3SecLevel     string `json:"snmp_v3_sec_level"`
	SnmpV3AuthProtocol string `json:"snmp_v3_auth_protocol"`
	SnmpV3AuthPass     string `json:"snmp_v3_auth_pass"`
	SnmpV3PrivProtocol string `json:"snmp_v3_priv_protocol"`
	SnmpV3CtxName      string `json:"snmp_v3_ctx_name"`
	SnmpV3CtxEngineId  string `json:"snmp_v3_ctx_engine_id"`

	// The date of the last edit to this resource.
	EditDate string `json:"editDate"`

	// A map[string]interface{} of custom fields to set on the resource. Note
	// that this functionality requires PHPIPAM 1.3 or higher with the "Nest
	// custom fields" flag set on the specific API integration. If this is not
	// enabled, this map will be nil on GETs and POSTs and PATCHes with this
	// field set will fail. Use the explicit custom field functions instead.
	CustomFields map[string]interface{} `json:"custom_fields"`
}

type JSONIntString int

// Subnet represents a PHPIPAM subnet.
type IPamSubnet struct {
	// The subnet ID.
	ID int `json:"id,string"`

	// The subnet address, in dotted quad format (i.e. A.B.C.D).
	SubnetAddress string `json:"subnet"`

	// The subnet's mask in number of bits (i.e. 24).
	Mask int `json:"mask,string"`

	// A detailed description of the subnet.
	Description string `json:"description"`

	// The section ID to add the subnet to (required when adding).
	SectionID int `json:"sectionId,string"`

	// The ID of a linked IPv6 subnet.
	LinkedSubnet int `json:"linked_subnet,string"`
	Device       int `json:"device,string"`

	// The ID of the VLAN that this subnet belongs to.
	VLANID int `json:"vlanId,string"`

	// The ID of the VRF this subnet belongs to.
	VRFID int `json:"vrfId,string"`

	// The parent subnet ID if this is a nested subnet.
	MasterSubnetID int `json:"masterSubnetId,string"`

	// The ID of the nameserver to attache the subnet to.
	NameserverID int `json:"nameserverId,string"`

	// true if the name should be displayed in listing instead of the subnet
	// address.
	ShowName BoolIntString `json:"showName,string"`

	// A JSON object, stringified, that represents the permissions for this
	// section.
	Permissions string `json:"permissions"`

	// Controls if PTR records should be created for the subnet.
	DNSRecursive BoolIntString `json:"DNSrecursive,string"`

	// Controls if DNS hostname records are displayed.
	DNSRecords BoolIntString `json:"DNSrecords,string"`

	// Controls if IP requests are allowed for the subnet.
	AllowRequests BoolIntString `json:"allowRequests,string"`

	// The ID of the scan agent to use for the subnet.
	ScanAgent int `json:"scanAgent,string"`

	// Controls if the subnet should be included in status checks.
	PingSubnet BoolIntString `json:"pingSubnet,string"`

	// Controls if new hosts should be discovered for new host scans.
	DiscoverSubnet BoolIntString `json:"discoverSubnet,string"`

	// Controls if we are adding a subnet or folder.
	IsFolder BoolIntString `json:"isFolder,string"`

	// Marks the subnet as used.
	IsFull BoolIntString `json:"isFull,string"`

	// The threshold of the subnet.
	Threshold int `json:"threshold,string"`

	// The location index of the subnet.
	Location int `json:"location,string"`

	// The date of the last edit to this resource.
	EditDate string `json:"editDate"`

	// A map[string]interface{} of custom fields to set on the resource. Note
	// that this functionality requires PHPIPAM 1.3 or higher with the "Nest
	// custom fields" flag set on the specific API integration. If this is not
	// enabled, this map will be nil on GETs and POSTs and PATCHes with this
	// field set will fail. Use the explicit custom field functions instead.
	CustomFields map[string]interface{} `json:"custom_fields"`
}

const IPamtimeLayout = "2006-01-02 15:04:05"

// Session represents a PHPIPAM session.
type IPamSession struct {
	// The session's configuration.
	Config IPamConfig `yaml:"Config" json:"config"`

	// The session token.
	Token     string `yaml:"Token" json:"token"`
	AuthToken string `yaml:"AuthToken" json:"authtoken"`
}

type RouterServiceConfigsPrometheus struct {
	HostTags PrometheusHostTags `yaml:"HostTags" json:"hosttags"`
}

type PrometheusHostTags struct {
	Tags map[string]string `yaml:"Tags" json:"tags"`
}

type RouterServiceConfigsIps struct {
	MonitorInterface string `yaml:"MonitorInterface" json:"monitorinterface"`
}

type RouterServiceConfigsHostApd struct {
	SSID              string                   `yaml:"SSID" json:"ssid"`
	EnableDHCP        bool                     `yaml:"EnableDHCP" json:"enabledhcp"`
	WirelessInterface string                   `yaml:"WirelessInterface" json:"wirelessinterface"`
	OutgoingInterface string                   `yaml:"OutgoingInterface" json:"outgoinginterface"`
	DHCPConfig        RouterServiceConfigsDhcp `yaml:"DHCPConfig" json:"dhcpconfig"`
	WPA2PSK           string                   `yaml:"WPA2PSK" json:"wpa2psk"`
	ApAddress         string                   `yaml:"ApAddress" json:"apaddress"`
	Subnet            string                   `yaml:"Subnet" json:"subnet"`
	Channel           string                   `yaml:"Channel" json:"channel"`
	CredsPath         string                   `yaml:"CredsPath" json:"credspath"`
}

type RouterServiceConfigsDhcp struct {
}

type RouterServiceConfigsMqtt struct {
	Username           string `yaml:"Username" json:"username"`
	Password           string `yaml:"Password" json:"password"`
	Token              string `yaml:"Token" json:"token"`
	EncryptedCredsPath string `yaml:"EncryptedCredsPath" json:"encryptedcredspath"`
}

type RouterServiceConfigsLoadbalancer struct {
	Name                     string                                `yaml:"Name" json:"name"`
	BindAddress              string                                `yaml:"BindAddress" json:"bindaddress"`
	HeaderPassthrough        bool                                  `yaml:"HeaderPassthrough" json:"headerpassthrough"`
	BindPort                 string                                `yaml:"BindPort" json:"bindport"`
	Protocol                 string                                `yaml:"Protocol" json:"protocol"`
	BalanceMethod            string                                `yaml:"BalanceMethod" json:"balancemethod"`
	MaxConnections           string                                `yaml:"MaxConnections" json:"maxconnections"`
	ClientIdleTimeout        string                                `yaml:"ClientIdleTimeout" json:"clientidletimeout"`
	BackendConnectionTimeout string                                `yaml:"BackendConnectionTimeout" json:"backendconnectiontimeout"`
	DiscoveryKind            string                                `yaml:"DiscoveryKind" json:"discoverykind"`
	StaticList               []RouterLoadbalancerBackendStaticList `yaml:"StaticList" json:"staticlist"`
	BackendK8s               RouterLoadbalancerBackendK8s          `yaml:"BackendK8s" json:"backendk8s"`
	BackendCidr              string                                `yaml:"BackendCidr" json:"backendcidr"`
	BackendPort              string                                `yaml:"BackendPort" json:"backendport"`
	BackendWeight            int                                   `yaml:"BackendWeight" json:"backendweight"`
	HealthCheck              string                                `yaml:"HealthChecks" json:"healthchecks"`
	Fails                    int                                   `yaml:"Fails" json:"fails"`
	Passes                   int                                   `yaml:"Passes" json:"passes"`
	Interval                 string                                `yaml:"Interval" json:"interval"`
	Timeout                  string                                `yaml:"Timeout" json:"timeout"`
	PingTimeoutDuration      string                                `yaml:"PingTimeoutDuration" json:"pingtimeoutduration"`
	Memory                   string                                `yaml:"Memory" json:"memory"`
	MemorySwap               string                                `yaml:"MemorySwap" json:"memoryswap"`
	MemoryReservation        string                                `yaml:"MemoryReservation" json:"memoryreservation"`
	CPUPercent               int64                                 `yaml:"CPUPercent" json:"cpupercent"`
	CPUs                     int64                                 `yaml:"CPUs" json:"cpus"`
}

type RouterServiceConfigsTraefik struct {
	K8sBackend     []TraefikK8sBackend   `yaml:"K8sBackend" json:"k8sbackend"`
	StaticBackend  TraefikStaticBackend  `yaml:"StaticBackend" json:"staticbackend"`
	DynamicBackend TraefikDynamicBackend `yaml:"DynamicBackend" json:"dynamicbackend, omitempty"`
	Config         TraefikConfig         `yaml:"Config" json:"config"`
	Rules          TraefikRules          `yaml:"Rules" json:"rules"`
}

type TraefikFileHeader struct {
	File string `yaml:"File" json:"file" toml:"file"`
}

type TraefikRules struct {
	File RulesFile `yaml:"File" json:"file" toml:"file"`
}

type RulesFile struct {
	Backends  map[string]RulesBackendsMap  `yaml:"Backends" json:"backends" toml:"backends"`
	Frontends map[string]RulesFrontendsMap `yaml:"Frontends" json:"frontends" toml:"frontends"`
	Tls       []RulesTls                   `yaml:"Tls" json:"tls" toml:"tls"`
}

// type RulesBackends struct {
// 	Name map[string]RulesBackendsMap `yaml:"Name" json:"name" toml:"name"`
// }

// type RulesFrontends struct {
// 	Name map[string]RulesFrontendsMap `yaml:"Name" json:"name" toml:"name"`
// }

type RulesBackendsMap struct {
	Servers map[string]BackendServers `yaml:"Servers" json:"servers" toml:"servers"`
	// CircuitBreaker     BackendCircuitBreaker     `yaml:"CircuitBreaker" json:"circuitbreaker" toml:"circuitBreaker"`
	// ResponseForwarding BackendResponseForwarding `yaml:"ResponseForwarding" json:"responseforwarding" toml:"responseForwarding"`
	// LoadBalancer       BackendLoadBalancer       `yaml:"LoadBalancer" json:"loadbalancer" toml:"loadBalancer"`
	// MaxConn            BackendMaxConn            `yaml:"MaxConn" json:"maxconn" toml:"maxConn"`
	// HealthCheck        BackendHealthCheck        `yaml:"Healthcheck" json:"healthcheck" toml:"healthCheck"`
}

type BackendHealthCheck struct {
	Path     string `yaml:"Path" json:"path" toml:"path"`
	Port     int    `yaml:"Port" json:"port" toml:"port"`
	Interval string `yaml:"Interval" json:"interval" toml:"interval"`
	Scheme   string `yaml:"Scheme" json:"scheme" toml:"scheme"`
	Hostname string `yaml:"Hostname" json:"hostname" toml:"hostname"`
}
type BackendLoadBalancer struct {
	Method     string       `yaml:"Method" json:"method" toml:"method"`
	Stickiness LBStickiness `yaml:"Stickiness" json:"stickiness" toml:"stickiness"`
}

type LBStickiness struct {
	CookieName string `yaml:"CookieName" json:"cookiename" toml:"cookieName"`
}
type BackendServers struct {
	URL    string `yaml:"URL" json:"url" toml:"url"`
	Weight int    `yaml:"Weight" json:"weight" toml:"weight"`
}

type BackendCircuitBreaker struct {
	Expression string `yaml:"Expression" json:"expression" toml:"expression"`
}

type BackendResponseForwarding struct {
	FlushInterval string `yaml:"FlushInterval" json:"flushinterval" toml:"flushInterval"`
}

type BackendMaxConn struct {
	Amount        int    `yaml:"Amount" json:"amount" toml:"amount"`
	Extractorfunc string `yaml:"Extractorfunc" json:"extractorfunc" toml:"extractorfunc"`
}
type RulesFrontendsMap struct {
	Entrypoints    []string               `yaml:"Entrypoints" json:"entrypoints" toml:"entrypoints"`
	Backend        string                 `yaml:"Backend" json:"backend" toml:"backend"`
	PassHostHeader bool                   `yaml:"PassHostHeader" json:"passhostheader" toml:"passHostHeader"`
	Priority       int                    `yaml:"Priority" json:"priority" toml:"priority"`
	BasicAuth      []string               `yaml:"BasicAuth" json:"basicauth" toml:"basicAuth"`
	Routes         map[string]RulesRoutes `yaml:"Routes" json:"routes" toml"routes"`
	// PassTLSClientCert PassTLSClientCert `yaml:"passtlsclientcert" json:"passtlsclientcert" toml:"passtlsclientcert"`
	// Whitelist         FrontendWhitelist `yaml:"" json:"" toml:""`
	SSLRedirect         bool     `yaml:"SSLRedirect" json:"sslredirect" toml:"SSLRedirect"`
	AllowedHosts        []string `yaml:"AllowedHosts" json:"allowedhosts" toml:"allowedHosts"`
	STSSeconds          int      `yaml:"STSSeconds" json:"stsseconds" toml:"STSSeconds"`
	SSIncludeSubdomains bool     `yaml:"STSIncludeSubdomains" json:"stsincludesubdomains" toml:"STSIncludeSubdomains"`
	STSPreload          bool     `yaml:"STSPreload" json:"stspreload" toml:"STSPreload"`
	ForceSTSHeader      bool     `yaml:"ForceSTSHeader" json:"forcehtsheader" toml:"forceSTSHeader"`
	FrameDeny           bool     `yaml:"FrameDeny" json:"framedeny" toml:"frameDeny"`
	ContentTypeNosniff  bool     `yaml:"ContentTypeNosniff" json:"contenttypenosniff" toml:"contentTypeNosniff"`
	BrowserXSSFilter    bool     `yaml:"BrowserXSSFilter" json:"browserxssfilter" toml:"browserXSSFilter"`
	IsDevelopment       bool     `yaml:"IsDevelopment" json:"isdevelopement" toml:"isDevelopement"`
}

type RulesRoutes struct {
	Rule string `yaml:"Rule" json:"rule" toml:"rule"`
}
type FrontendWhitelist struct {
	SourceRange      []string `yaml:"SourceRange" json:"sourcerange" toml:"sourceRange"`
	UseXforwardedFor bool     `yaml:"UseXforwardedFor" json:"usexforwardedfor" toml:"useXforwardedFor"`
}
type PassTLSClientCert struct {
	Pem bool `yaml:"" json:"" toml:""`
}
type RulesTls struct {
	EntryPoints []string         `yaml:"EntryPoints" json:"entrypoints" toml:"entryPoints"`
	Certificate RulesCertificate `yaml:"Certificate" json:"certificate" toml:"certificate"`
}

type RulesCertificate struct {
	CertFile string `yaml:"CertFile" json:"certfile" toml:"certFile"`
	KeyFile  string `yaml:"KeyFile" json:"keyfile" toml:"keyFile"`
}

// Global holds the global configuration.
type TraefikGlobal struct {
	CheckNewVersion    bool  `description:"Periodically check if a new version has been released." json:"checkNewVersion" toml:"checkNewVersion" yaml:"CheckNewVersion" label:"allowEmpty" export:"true"`
	SendAnonymousUsage *bool `description:"Periodically send anonymous usage statistics. If the option is not specified, it will be enabled by default." json:"sendAnonymousUsage" toml:"sendAnonymousUsage" yaml:"SendAnonymousUsage" label:"allowEmpty" export:"true"`
}

type TraefikConfig struct {
	Global           *TraefikGlobal             `description:"Global configuration options" json:"global" toml:"global" yaml:"Global" export:"true"`
	ServersTransport *TraefikServersTransport   `description:"Servers default transport." json:"serversTransport" toml:"serversTransport" yaml:"ServersTransport" export:"true"`
	EntryPoints      TraefikEntryPoints         `description:"Entry points definition." json:"entryPoints" toml:"entryPoints" yaml:"EntryPoints" export:"true"`
	Providers        *TraefikProviders          `description:"Providers configuration." json:"providers" toml:"providers" yaml:"Providers" export:"true"`
	API              *TraefikAPI                `description:"Enable api/dashboard." json:"api" toml:"api" yaml:"Api" label:"allowEmpty" export:"true"`
	Metrics          *TraefikMetrics            `description:"Enable a metrics exporter." json:"metrics" toml:"metrics" yaml:"Metrics" export:"true"`
	Ping             *TraefikPing               `description:"Enable ping." json:"ping" toml:"ping" yaml:"Ping" label:"allowEmpty" export:"true"`
	Log              *TraefikLog                `description:"Traefik log settings." json:"log" toml:"log" yaml:"Log" label:"allowEmpty" export:"true"`
	AccessLog        *TraefikAccessLog          `description:"Access log settings." json:"accessLog" toml:"accessLog" yaml:"AccessLog" label:"allowEmpty" export:"true"`
	Tracing          *TraefikTracing            `description:"OpenTracing configuration." json:"tracing" toml:"tracing" yaml:"Tracing" label:"allowEmpty" export:"true"`
	HostResolver     *TraefikHostResolverConfig `description:"Enable CNAME Flattening." json:"hostResolver" toml:"hostResolver" yaml:"HostResolver" label:"allowEmpty" export:"true"`
	ACME             *TraefikAcme               `description:"Enable ACME (Let's Encrypt): automatic SSL." json:"acme" toml:"acme" yaml:"Acme" export:"true"`
}

type TraefikAcme struct {
	Email         string                `description:"Email address used for registration." json:"email" toml:"email" yaml:"Email"`
	ACMELogging   bool                  `description:"Enable debug logging of ACME actions." json:"acmeLogging" toml:"acmeLogging" yaml:"AcmeLogging"`
	CAServer      string                `description:"CA server to use." json:"caServer" toml:"caServer" yaml:"CaServer"`
	Storage       string                `description:"Storage to use." json:"storage" toml:"storage" yaml:"Storage"`
	EntryPoint    string                `description:"EntryPoint to use." json:"entryPoint" toml:"entryPoint" yaml:"EntryPoint"`
	KeyType       string                `description:"KeyType used for generating certificate private key. Allow value 'EC256', 'EC384', 'RSA2048', 'RSA4096', 'RSA8192'." json:"keyType" toml:"keyType" yaml:"KeyType"`
	OnHostRule    bool                  `description:"Enable certificate generation on router Host rules." json:"onHostRule" toml:"onHostRule" yaml:"OnHostRule"`
	DNSChallenge  *TraefikDNSChallenge  `description:"Activate DNS-01 Challenge." json:"dnsChallenge" toml:"dnsChallenge" yaml:"DnsChallenge" label:"allowEmpty"`
	HTTPChallenge *TraefikHTTPChallenge `description:"Activate HTTP-01 Challenge." json:"httpChallenge" toml:"httpChallenge" yaml:"HttpChallenge" label:"allowEmpty"`
	TLSChallenge  *TraefikTLSChallenge  `description:"Activate TLS-ALPN-01 Challenge." json:"tlsChallenge" toml:"tlsChallenge" yaml:"TlsChallenge" label:"allowEmpty"`
	Domains       []TraefikDomain       `description:"The list of domains for which certificates are generated on startup. Wildcard domains only accepted with DNSChallenge." json:"domains" toml:"domains" yaml:"Domains"`
}

// Domain holds a domain name with SANs.
type TraefikDomain struct {
	Main string   `description:"Default subject name." json:"main" toml:"main" yaml:"Main"`
	SANs []string `description:"Subject alternative names." json:"sans" toml:"sans" yaml:"Sans"`
}

// Certificate is a struct which contains all data needed from an ACME certificate
type TraefikAcmeCertificate struct {
	Domain      TraefikDomain `json:"domain" toml:"domain" yaml:"Domain"`
	Certificate []byte        `json:"certificate" toml:"certificate" yaml:"Certificate"`
	Key         []byte        `json:"key" toml:"key" yaml:"Key"`
}

// DNSChallenge contains DNS challenge Configuration
type TraefikDNSChallenge struct {
	Provider                string          `description:"Use a DNS-01 based challenge provider rather than HTTPS." json:"provider" toml:"provider" yaml:"Provider"`
	DelayBeforeCheck        TraefikDuration `description:"Assume DNS propagates after a delay in seconds rather than finding and querying nameservers." json:"delayBeforeCheck" toml:"delayBeforeCheck" yaml:"DelayBeforeCheck"`
	Resolvers               []string        `description:"Use following DNS servers to resolve the FQDN authority." json:"resolvers" toml:"resolvers" yaml:"Resolvers"`
	DisablePropagationCheck bool            `description:"Disable the DNS propagation checks before notifying ACME that the DNS challenge is ready. [not recommended]" json:"disablePropagationCheck" toml:"disablePropagationCheck" yaml:"DisablePropagationCheck"`
}

// HTTPChallenge contains HTTP challenge Configuration
type TraefikHTTPChallenge struct {
	TraefikEntryPoint string `description:"HTTP challenge EntryPoint" json:"entryPoint" toml:"entryPoint" yaml:"EntryPoint"`
}

// TLSChallenge contains TLS challenge Configuration
type TraefikTLSChallenge struct{}

// HostResolverConfig contain configuration for CNAME Flattening.
type TraefikHostResolverConfig struct {
	CnameFlattening bool   `description:"A flag to enable/disable CNAME flattening" json:"cnameFlattening" toml:"cnameFlattening" yaml:"CnameFlattening" export:"true"`
	ResolvConfig    string `description:"resolv.conf used for DNS resolving" json:"resolvConfig" toml:"resolvConfig" yaml:"ResolvConfig" export:"true"`
	ResolvDepth     int    `description:"The maximal depth of DNS recursive resolving" json:"resolvDepth" toml:"resolvDepth" yaml:"ResolvDepth" export:"true"`
}

type TraefikPing struct {
	Address string `json:"ping" toml:"ping" label:allowEmpty" export:"true"`
}
type TraefikAccessLog struct {
	FilePath      string                   `description:"Access log file path. Stdout is used when omitted or empty." json:"filePath" toml:"filePath" yaml:"FilePath" export:"true"`
	Format        string                   `description:"Access log format: json | common" json:"format" toml:"format" yaml:"Format" export:"true"`
	Filters       *TraefikAccessLogFilters `description:"Access log filters, used to keep only specific access logs." json:"filters" toml:"filters" yaml:"Filters" export:"true"`
	Fields        *TraefikAccessLogFields  `description:"AccessLogFields." json:"fields" toml:"fields" yaml:"Fields" export:"true"`
	BufferingSize int64                    `description:"Number of access log lines to process in a buffered way." json:"bufferingSize" toml:"bufferingSize" yaml:"BufferingSize" export:"true"`
}

// AccessLogFilters holds filters configuration
type TraefikAccessLogFilters struct {
	StatusCodes   []string        `description:"Keep access logs with status codes in the specified range." json:"statusCodes" toml:"statusCodes" yaml:"StatusCodes" export:"true"`
	RetryAttempts bool            `description:"Keep access logs when at least one retry happened." json:"retryAttempts" toml:"retryAttempts" yaml:"RetryAttempts" export:"true"`
	MinDuration   TraefikDuration `description:"Keep access logs when request took longer than the specified duration." json:"minDuration" toml:"minDuration" yaml:"MinDuration" export:"true"`
}

// FieldHeaders holds configuration for access log headers
type TraefikFieldHeaders struct {
	DefaultMode string            `description:"Default mode for fields: keep | drop | redact" json:"defaultMode" toml:"defaultMode" yaml:"DefaultMode" export:"true"`
	Names       map[string]string `description:"Override mode for headers" json:"names" toml:"names" yaml:"Names" export:"true"`
}

// AccessLogFields holds configuration for access log fields
type TraefikAccessLogFields struct {
	DefaultMode string               `description:"Default mode for fields: keep | drop" json:"defaultMode" toml:"defaultMode" yaml:"DefaultMode"  export:"true"`
	Names       map[string]string    `json:"names" description:"Override mode for fields" json:"names" toml:"names" yaml:"Names" export:"true"`
	Headers     *TraefikFieldHeaders `description:"Headers to keep, drop or redact" json:"headers" toml:"headers" yaml:"Headers" export:"true"`
}

type TraefikMetrics struct {
	Prometheus *TraefikPrometheus `description:"Prometheus metrics exporter type." json:"prometheus" toml:"prometheus" yaml:"Prometheus" export:"true" label:"allowEmpty"`
	DataDog    *TraefikDataDog    `description:"DataDog metrics exporter type." json:"dataDog" toml:"dataDog" yaml:"DataDog" export:"true" label:"allowEmpty"`
	StatsD     *TraefikStatsd     `description:"StatsD metrics exporter type." json:"statsD" toml:"statsD" yaml:"StatsD" export:"true" label:"allowEmpty"`
	InfluxDB   *TraefikInfluxDB   `description:"InfluxDB metrics exporter type." json:"influxDB" toml:"influxDB" yaml:"InfluxDB" label:"allowEmpty"`
}

// Prometheus can contain specific configuration used by the Prometheus Metrics exporter
type TraefikPrometheus struct {
	Buckets     []float64 `description:"Buckets for latency metrics." json:"buckets" toml:"buckets" yaml:"Buckets" export:"true"`
	EntryPoint  string    `description:"EntryPoint." json:"entryPoint" toml:"entryPoint" yaml:"EntryPoint" export:"true"`
	Middlewares []string  `description:"Middlewares." json:"middlewares" toml:"middlewares" yaml:"Middlewares" export:"true"`
}

// DataDog contains address and metrics pushing interval configuration
type TraefikDataDog struct {
	Address      string          `description:"DataDog's address." json:"address" toml:"address" yaml:"Address"`
	PushInterval TraefikDuration `description:"DataDog push interval." json:"pushInterval" toml:"pushInterval" yaml:"PushInterval" export:"true"`
}

// Statsd contains address and metrics pushing interval configuration
type TraefikStatsd struct {
	Address      string          `description:"StatsD address." json:"address" toml:"address" yaml:"Address"`
	PushInterval TraefikDuration `description:"StatsD push interval." json:"pushInterval" toml:"pushInterval" yaml:"PushInterval" export:"true"`
}

// InfluxDB contains address, login and metrics pushing interval configuration
type TraefikInfluxDB struct {
	Address         string          `description:"InfluxDB address." json:"address" toml:"address" yaml:"Address"`
	Protocol        string          `description:"InfluxDB address protocol (udp or http)." json:"protocol" toml:"protocol" yaml:"Protocol"`
	PushInterval    TraefikDuration `description:"InfluxDB push interval." json:"pushInterval" toml:"pushInterval" yaml:"PushInterval" export:"true"`
	Database        string          `description:"InfluxDB database used when protocol is http." json:"database" toml:"database" yaml:"Database" export:"true"`
	RetentionPolicy string          `description:"InfluxDB retention policy used when protocol is http." json:"retentionPolicy" toml:"retentionPolicy" yaml:"RetentionPolicy" export:"true"`
	Username        string          `description:"InfluxDB username (only with http)." json:"username" toml:"username" yaml:"Username" export:"true"`
	Password        string          `description:"InfluxDB password (only with http)." json:"password" toml:"password" yaml:"Password" export:"true"`
}

type TraefikDocker struct {
	Constraints             string            `description:"Constraints is an expression that Traefik matches against the container's labels to determine whether to create any route for that container." json:"constraints" toml:"constraints" yaml:"Constraints" export:"true"`
	Watch                   bool              `description:"Watch provider." json:"watch" toml:"watch" yaml:"Watch" export:"true"`
	Endpoint                string            `description:"Docker server endpoint. Can be a tcp or a unix socket endpoint." json:"endpoint" toml:"endpoint" yaml:"Endpoint"`
	DefaultRule             string            `description:"Default rule." json:"defaultRule" toml:"defaultRule" yaml:"DefaultRule"`
	TLS                     *TraefikClientTLS `description:"Enable Docker TLS support." json:"tls" toml:"tls" yaml:"Tls" export:"true"`
	ExposedByDefault        bool              `description:"Expose containers by default." json:"exposedByDefault" toml:"exposedByDefault" yaml:"ExposedByDefault" export:"true"`
	UseBindPortIP           bool              `description:"Use the ip address from the bound port, rather than from the inner network." json:"useBindPortIP" toml:"useBindPortIP" yaml:"UseBindPortIP" export:"true"`
	SwarmMode               bool              `description:"Use Docker on Swarm Mode." json:"swarmMode" toml:"swarmMode" yaml:"SwarmMode" export:"true"`
	Network                 string            `description:"Default Docker network used." json:"network" toml:"network" yaml:"Network" export:"true"`
	SwarmModeRefreshSeconds TraefikDuration   `description:"Polling interval for swarm mode." json:"swarmModeRefreshSeconds" toml:"swarmModeRefreshSeconds" yaml:"SwarmModeRefreshSeconds" export:"true"`
	defaultRuleTpl          *template.Template
}

type TraefikKubernetes struct {
	Endpoint               string                  `description:"Kubernetes server endpoint (required for external cluster client)." json:"endpoint" toml:"endpoint" yaml:"Endpoint"`
	Token                  string                  `description:"Kubernetes bearer token (not needed for in-cluster client)." json:"token" toml:"token" yaml:"Token"`
	CertAuthFilePath       string                  `description:"Kubernetes certificate authority file path (not needed for in-cluster client)." json:"certAuthFilePath" toml:"certAuthFilePath" yaml:"CertAuthFilePath"`
	DisablePassHostHeaders bool                    `description:"Kubernetes disable PassHost Headers." json:"disablePassHostHeaders" toml:"disablePassHostHeaders" yaml:"DisablePassHostHeaders" export:"true"`
	Namespaces             []string                `description:"Kubernetes namespaces." json:"namespaces" toml:"namespaces" yaml:"Namespaces" export:"true"`
	LabelSelector          string                  `description:"Kubernetes Ingress label selector to use." json:"labelSelector" toml:"labelSelector" yaml:"LabelSelector" export:"true"`
	IngressClass           string                  `description:"Value of kubernetes.io/ingress.class annotation to watch for." json:"ingressClass" toml:"ingressClass" yaml:"IngressClass" export:"true"`
	IngressEndpoint        *TraefikEndpointIngress `description:"Kubernetes Ingress Endpoint." json:"ingressEndpoint" toml:"ingressEndpoint" yaml:"IngressEndpoint"`
}

// EndpointIngress holds the endpoint information for the Kubernetes provider
type TraefikEndpointIngress struct {
	IP               string `description:"IP used for Kubernetes Ingress endpoints." json:"ip" toml:"ip" yaml:"Ip"`
	Hostname         string `description:"Hostname used for Kubernetes Ingress endpoints." json:"hostname" toml:"hostname" yaml:"Hostname"`
	PublishedService string `description:"Published Kubernetes Service to copy status from." json:"publishedService" toml:"publishedService" yaml:"PublishedService"`
}

type TraefikCrd struct {
	Endpoint               string   `description:"Kubernetes server endpoint (required for external cluster client)." json:"endpoint" toml:"endpoint" yaml:"Endpoint"`
	Token                  string   `description:"Kubernetes bearer token (not needed for in-cluster client)." json:"token" toml:"token" yaml:"Token"`
	CertAuthFilePath       string   `description:"Kubernetes certificate authority file path (not needed for in-cluster client)." json:"certAuthFilePath" toml:"certAuthFilePath" yaml:"CertAuthFilePath"`
	DisablePassHostHeaders bool     `description:"Kubernetes disable PassHost Headers." json:"disablePassHostHeaders" toml:"disablePassHostHeaders" yaml:"DisablePassHostHeaders" export:"true"`
	Namespaces             []string `description:"Kubernetes namespaces." json:"namespaces" toml:"namespaces" yaml:"Namespaces" export:"true"`
	LabelSelector          string   `description:"Kubernetes label selector to use." json:"labelSelector" toml:"labelSelector" yaml:"LabelSelector" export:"true"`
	IngressClass           string   `description:"Value of kubernetes.io/ingress.class annotation to watch for." json:"ingressClass" toml:"ingressClass" yaml:"IngressClass" export:"true"`
}

type TraefikFile struct {
	Directory                 string `description:"Load configuration from one or more .toml files in a directory." json:"directory" toml:"directory" yaml:"Directory" export:"true"`
	Watch                     bool   `description:"Watch provider." json:"watch" toml:"watch" yaml:"Watch" export:"true"`
	Filename                  string `description:"Override default configuration template. For advanced users :)" json:"filename" toml:"filename" yaml:"Filename" export:"true"`
	DebugLogGeneratedTemplate bool   `description:"Enable debug logging of generated configuration template." json:"debugLogGeneratedTemplate" toml:"debugLogGeneratedTemplate" yaml:"DebugLogGeneratedTemplate" export:"true"`
	TraefikFile               string `description:"-" json:"traefikFile" toml:"traefikFile" yaml:"TraefikFile"`
}

// ServersTransport options to configure communication between Traefik and the servers
type TraefikServersTransport struct {
	InsecureSkipVerify  bool                       `description:"Disable SSL certificate verification." json:"insecureSkipVerify" toml:"insecureSkipVerify" yaml:"InsecureSkipVerify" export:"true"`
	RootCAs             []TraefikFileOrContent     `description:"Add cert file for self-signed certificate." json:"rootCAs" toml:"rootCAs" yaml:"RootCAs"`
	MaxIdleConnsPerHost int                        `description:"If non-zero, controls the maximum idle (keep-alive) to keep per-host. If zero, DefaultMaxIdleConnsPerHost is used" json:"maxIdleConnsPerHost" toml:"maxIdleConnsPerHost" yaml:"MaxIdleConnsPerHost" export:"true"`
	ForwardingTimeouts  *TraefikForwardingTimeouts `description:"Timeouts for requests forwarded to the backend servers." json:"forwardingTimeouts" toml:"forwardingTimeouts" yaml:"ForwardingTimeouts" export:"true"`
}

type TraefikFileOrContent string

// API holds the API configuration
type TraefikAPI struct {
	EntryPoint  string             `description:"The entry point that the API handler will be bound to." json:"entryPoint" toml:"entryPoint" yaml:"EntryPoint" export:"true"`
	Dashboard   bool               `description:"Activate dashboard." json:"dashboard" toml:"dashboard" yaml:"Dashboard" export:"true"`
	Debug       bool               `description:"Enable additional endpoints for debugging and profiling." json:"debug" toml:"debug" yaml:"Debug" export:"true"`
	Statistics  *TraefikStatistics `description:"Enable more detailed statistics." json:"statistics" toml:"statistics" yaml:"Statistics" export:"true" label:"allowEmpty"`
	Middlewares []string           `description:"Middleware list." json:"middlewares" toml:"middlewares" yaml:"Middlewares" export:"true"`
}

// Statistics provides options for monitoring request and response stats
type TraefikStatistics struct {
	RecentErrors int `description:"Number of recent errors logged." json:"recentErrors" toml:"recentErrors" yaml:"RecentErrors" export:"true"`
}

type TraefikClientTLS struct {
	CA                 string `description:"TLS CA" json:"ca" toml:"ca" yaml:"Ca"`
	CAOptional         bool   `description:"TLS CA.Optional" json:"caOptional" toml:"caOptional" yaml:"CaOptional"`
	Cert               string `description:"TLS cert" json:"cert" toml:"cert" yaml:"Cert"`
	Key                string `description:"TLS key" json:"key" toml:"key" yaml:"Key"`
	InsecureSkipVerify bool   `description:"TLS insecure skip verify" json:"insecureSkipVerify" toml:"insecureSkipVerify" yaml:"InsecureSkipVerify"`
}

type TraefikRespondingTimeouts struct {
	ReadTimeout  TraefikDuration `description:"ReadTimeout is the maximum duration for reading the entire request, including the body. If zero, no timeout is set." json:"readTimeout" toml:"readTimeout" yaml:"ReadTimeout" export:"true"`
	WriteTimeout TraefikDuration `description:"WriteTimeout is the maximum duration before timing out writes of the response. If zero, no timeout is set." json:"writeTimeout" toml:"writeTimeout" yaml:"WriteTimeout" export:"true"`
	IdleTimeout  TraefikDuration `description:"IdleTimeout is the maximum amount duration an idle (keep-alive) connection will remain idle before closing itself. If zero, no timeout is set." json:"idleTimeout" toml:"idleTimeout" yaml:"IdleTimeout" export:"true"`
}

// ForwardingTimeouts contains timeout configurations for forwarding requests to the backend servers.
type TraefikForwardingTimeouts struct {
	DialTimeout           TraefikDuration `description:"The amount of time to wait until a connection to a backend server can be established. If zero, no timeout exists." json:"dialTimeout" toml:"dialTimeout" yaml:"DialTimeout" export:"true"`
	ResponseHeaderTimeout TraefikDuration `description:"The amount of time to wait for a server's response headers after fully writing the request (including its body, if any). If zero, no timeout exists." json:"responseHeaderTimeout" toml:"responseHeaderTimeout" yaml:"ResponseHeaderTimeout" export:"true"`
	IdleConnTimeout       TraefikDuration `description:"The maximum period for which an idle HTTP keep-alive connection will remain open before closing itself" json:"idleConnTimeout" toml:"idleConnTimeout" yaml:"IdleConnTimeout" export:"true"`
}

type TraefikDuration time.Duration

// LifeCycle contains configurations relevant to the lifecycle (such as the shutdown phase) of Traefik.
type TraefikLifeCycle struct {
	RequestAcceptGraceTimeout TraefikDuration `description:"Duration to keep accepting requests before Traefik initiates the graceful shutdown procedure." json:"requestAcceptGraceTimeout" toml:"requestAcceptGraceTimeout" yaml:"RequestAcceptGraceTimeout" export:"true"`
	GraceTimeOut              TraefikDuration `description:"Duration to give active requests a chance to finish before Traefik stops." json:"graceTimeOut" toml:"graceTimeOut" yaml:"GraceTimeOut" export:"true"`
}

// Tracing holds the tracing configuration.
type TraefikTracing struct {
	ServiceName   string           `description:"Set the name for this service." json:"serviceName" toml:"serviceName" yaml:"ServiceName" export:"true"`
	SpanNameLimit int              `description:"Set the maximum character limit for Span names (default 0 = no limit)." json:"spanNameLimit" toml:"spanNameLimit" yaml:"SpanNameLimit" export:"true"`
	Jaeger        *TraefikJaeger   `description:"Settings for Jaeger." json:"jaeger" toml:"jaeger" yaml:"Jaeger" export:"true" label:"allowEmpty"`
	Zipkin        *TraefikZipkin   `description:"Settings for Zipkin." json:"zipkin" toml:"zipkin" yaml:"Zipkin" export:"true" label:"allowEmpty"`
	DataDog       *TraefikDatadog  `description:"Settings for DataDog." json:"dataDog" toml:"dataDog" yaml:"DataDog" export:"true" label:"allowEmpty"`
	Instana       *TraefikInstana  `description:"Settings for Instana." json:"instana" toml:"instana" yaml:"Instana" export:"true" label:"allowEmpty"`
	Haystack      *TraefikHaystack `description:"Settings for Haystack." json:"haystack" toml:"haystack" yaml:"Haystack" export:"true" label:"allowEmpty"`
}

type TraefikZipkin struct {
	HTTPEndpoint string  `description:"HTTP Endpoint to report traces to." json:"httpEndpoint" toml:"httpEndpoint" yaml:"HttpEndpoint"`
	SameSpan     bool    `description:"Use Zipkin SameSpan RPC style traces." json:"sameSpan" toml:"sameSpan" yaml:"SameSpan" export:"true"`
	ID128Bit     bool    `description:"Use Zipkin 128 bit root span IDs." json:"id128Bit" toml:"id128Bit" yaml:"Id128Bit" export:"true"`
	Debug        bool    `description:"Enable Zipkin debug." json:"debug" toml:"debug" yaml:"Debug" export:"true"`
	SampleRate   float64 `description:"The rate between 0.0 and 1.0 of requests to trace." json:"sampleRate" toml:"sampleRate" yaml:"SampleRate" export:"true"`
}

type TraefikJaeger struct {
	SamplingServerURL      string  `description:"Set the sampling server url." json:"samplingServerURL" toml:"samplingServerURL" yaml:"SamplingServerURL"`
	SamplingType           string  `description:"Set the sampling type." json:"samplingType" toml:"samplingType" yaml:"SamplingType" export:"true"`
	SamplingParam          float64 `description:"Set the sampling parameter." json:"samplingParam" toml:"samplingParam" yaml:"SamplingParam" export:"true"`
	LocalAgentHostPort     string  `description:"Set jaeger-agent's host:port that the reporter will used." json:"localAgentHostPort" toml:"localAgentHostPort" yaml:"LocalAgentHostPort"`
	Gen128Bit              bool    `description:"Generate 128 bit span IDs." json:"gen128Bit" toml:"gen128Bit" yaml:"Gen128Bit" export:"true"`
	Propagation            string  `description:"Which propgation format to use (jaeger/b3)." json:"propagation" toml:"propagation" yaml:"Propagation" export:"true"`
	TraceContextHeaderName string  `description:"Set the header to use for the trace-id." json:"traceContextHeaderName" toml:"traceContextHeaderName" yaml:"TraceContextHeaderName" export:"true"`
}

type TraefikInstana struct {
	LocalAgentHost string `description:"Set instana-agent's host that the reporter will used." json:"localAgentHost" toml:"localAgentHost" yaml:"LocalAgentHost"`
	LocalAgentPort int    `description:"Set instana-agent's port that the reporter will used." json:"localAgentPort" toml:"localAgentPort" yaml:"LocalAgentPort"`
	LogLevel       string `description:"Set instana-agent's log level. ('error','warn','info','debug')" json:"logLevel" toml:"logLevel" yaml:"LogLevel" export:"true"`
}

type TraefikHaystack struct {
	LocalAgentHost          string `description:"Set haystack-agent's host that the reporter will used." json:"localAgentHost" toml:"localAgentHost" yaml:"LocalAgentHost"`
	LocalAgentPort          int    `description:"Set haystack-agent's port that the reporter will used." json:"localAgentPort" toml:"localAgentPort" yaml:"LocalAgentPort"`
	GlobalTag               string `description:"Key:Value tag to be set on all the spans." json:"globalTag" toml:"globalTag" yaml:"GlobalTag" export:"true"`
	TraceIDHeaderName       string `description:"Specifies the header name that will be used to store the trace ID." json:"traceIDHeaderName" toml:"traceIDHeaderName" yaml:"TraceIDHeaderName" export:"true"`
	ParentIDHeaderName      string `description:"Specifies the header name that will be used to store the parent ID." json:"parentIDHeaderName" toml:"parentIDHeaderName" yaml:"ParentIDHeaderName" export:"true"`
	SpanIDHeaderName        string `description:"Specifies the header name that will be used to store the span ID." json:"spanIDHeaderName" toml:"spanIDHeaderName" yaml:"SpanIDHeaderName" export:"true"`
	BaggagePrefixHeaderName string `description:"Specifies the header name prefix that will be used to store baggage items in a map." json:"baggagePrefixHeaderName" toml:"baggagePrefixHeaderName" yaml:"BaggagePrefixHeaderName" export:"true"`
}

type TraefikDatadog struct {
	LocalAgentHostPort         string `description:"Set datadog-agent's host:port that the reporter will used." json:"localAgentHostPort" toml:"localAgentHostPort" yaml:"LocalAgentHostPort"`
	GlobalTag                  string `description:"Key:Value tag to be set on all the spans." json:"globalTag" toml:"globalTag" yaml:"GlobalTag" export:"true"`
	Debug                      bool   `description:"Enable DataDog debug." json:"debug" toml:"debug" yaml:"Debug" export:"true"`
	PrioritySampling           bool   `description:"Enable priority sampling. When using distributed tracing, this option must be enabled in order to get all the parts of a distributed trace sampled." json:"prioritySampling" toml:"prioritySampling" yaml:"PrioritySampling"`
	TraceIDHeaderName          string `description:"Specifies the header name that will be used to store the trace ID." json:"traceIDHeaderName" toml:"traceIDHeaderName" yaml:"TraceIDHeaderName" export:"true"`
	ParentIDHeaderName         string `description:"Specifies the header name that will be used to store the parent ID." json:"parentIDHeaderName" toml:"parentIDHeaderName" yaml:"ParentIDHeaderName" export:"true"`
	SamplingPriorityHeaderName string `description:"Specifies the header name that will be used to store the sampling priority." json:"samplingPriorityHeaderName" toml:"samplingPriorityHeaderName" yaml:"SamplingPriorityHeaderName" export:"true"`
	BagagePrefixHeaderName     string `description:"Specifies the header name prefix that will be used to store baggage items in a map." json:"bagagePrefixHeaderName" toml:"bagagePrefixHeaderName" yaml:"BagagePrefixHeaderName" export:"true"`
}

// Providers contains providers configuration
type TraefikProviders struct {
	ProvidersThrottleDuration TraefikDuration    `description:"Backends throttle duration: minimum duration between 2 events from providers before applying a new configuration. It avoids unnecessary reloads if multiples events are sent in a short amount of time." json:"providersThrottleDuration" toml:"providersThrottleDuration" yaml:"ProvidersThrottleDuration" export:"true"`
	Docker                    *TraefikDocker     `description:"Enable Docker backend with default settings." json:"docker" toml:"docker" yaml:"Docker" export:"true" label:"allowEmpty"`
	File                      *TraefikFile       `description:"Enable File backend with default settings." json:"file" toml:"file" yaml:"File" export:"true" label:"allowEmpty"`
	Kubernetes                *TraefikKubernetes `description:"Enable Kubernetes backend with default settings." json:"kubernetes" toml:"kubernetes" yaml:"Kubernetes" export:"true" label:"allowEmpty"`
	KubernetesCRD             *TraefikCrd        `description:"Enable Kubernetes backend with default settings." json:"kubernetesCRD" toml:"kubernetesCRD" yaml:"KubernetesCRD" export:"true" label:"allowEmpty"`
	Rest                      *TraefikRest       `description:"Enable Rest backend with default settings." json:"rest" toml:"rest" yaml:"Rest" export:"true" label:"allowEmpty"`
	Rancher                   *TraefikRancher    `description:"Enable Rancher backend with default settings." json:"rancher" toml:"rancher" yaml:"Rancher" export:"true" label:"allowEmpty"`
}

// Provider is a provider.Provider implementation that provides a Rest API.
type TraefikRest struct {
	configurationChan chan<- TraefikMessage
	EntryPoint        string `description:"EntryPoint." json:"entryPoint" toml:"entryPoint" yaml:"EntryPoint" export:"true"`
}

type TraefikMessage struct {
	ProviderName  string
	Configuration *TraefikConfiguration
}

// Configurations is for currentConfigurations Map.
type TraefikConfigurations map[string]*TraefikConfiguration

// Configuration is the root of the dynamic configuration
type TraefikConfiguration struct {
	HTTP *TraefikHTTPConfiguration `json:"http" toml:"http" yaml:"Http"`
	TCP  *TraefikTCPConfiguration  `json:"tcp" toml:"tcp" yaml:"Tcp"`
	TLS  *TraefikTLSConfiguration  `json:"tls" toml:"tls" yaml:"Tls"`
}

// TLSConfiguration contains all the configuration parameters of a TLS connection.
type TraefikTLSConfiguration struct {
	Certificates []*TraefikCertAndStores   `json:"-"  toml:"certificates" yaml:"Certificates" label:"-"`
	Options      map[string]TraefikOptions `json:"options" toml:"options" yaml:"Options"`
	Stores       map[string]TraefikStore   `json:"stores" toml:"stores" yaml:"Stores"`
}

// Options configures TLS for an entry point
type TraefikOptions struct {
	MinVersion   string          `json:"minVersion" toml:"minVersion" yaml:"MinVersion" export:"true"`
	CipherSuites []string        `json:"cipherSuites" toml:"cipherSuites" yaml:"CipherSuites"`
	ClientCA     TraefikClientCA `json:"clientCA" toml:"clientCA" yaml:"ClientCA"`
	SniStrict    bool            `json:"sniStrict" toml:"sniStrict" yaml:"SniStrict" export:"true"`
}

type TraefikClientCA struct {
	Files    []TraefikFileOrContent `json:"files" toml:"files" yaml:"Files"`
	Optional bool                   `json:"optional" toml:"optional" yaml:"Optional"`
}

// Store holds the options for a given Store
type TraefikStore struct {
	DefaultCertificate *TraefikCertificate `json:"defaultCertificate" toml:"defaultCertificate" yaml:"DefaultCertificate"`
}

// CertAndStores allows mapping a TLS certificate to a list of entry points.
type TraefikCertAndStores struct {
	TraefikCertificate `yaml:",inline"`
	CertStores         []string `json:"stores" toml:"stores" yaml:"Stores"`
}

type TraefikCertificate struct {
	CertFile TraefikFileOrContent `json:"certFile" toml:"certFile" yaml:"CertFile"`
	KeyFile  TraefikFileOrContent `json:"keyFile" toml:"keyFile" yaml:"KeyFile"`
}

// Certificates defines traefik certificates type
// Certs and Keys could be either a file path, or the file content itself
type Certificates []TraefikCertificate

// Store holds the options for a given Store
type CertStore struct {
	DefaultCertificate *TraefikCertificate `json:"defaultCertificate" toml:"defaultCertificate" yaml:"DefaultCertificate"`
}

// HTTPConfiguration contains all the HTTP configuration parameters.
type TraefikHTTPConfiguration struct {
	Routers  map[string]*TraefikRouter  `json:"routers" toml:"routers" yaml:"Routers"`
	Services map[string]*TraefikService `json:"services" toml:"services" yaml:"Services"`
}

// TCPConfiguration contains all the TCP configuration parameters.
type TraefikTCPConfiguration struct {
	Routers  map[string]*TraefikTCPRouter  `json:"routers" toml:"routers" yaml:"Routers"`
	Services map[string]*TraefikTCPService `json:"services" toml:"services" yaml:"Services"`
}

// Service holds a service configuration (can only be of one type at the same time).
type TraefikService struct {
	LoadBalancer *TraefikLoadBalancerService `json:"loadBalancer" toml:"loadBalancer" yaml:"LoadBalancer"`
}

// TCPService holds a tcp service configuration (can only be of one type at the same time).
type TraefikTCPService struct {
	LoadBalancer *TraefikTCPLoadBalancerService `json:"loadBalancer" toml:"loadBalancer" yaml:"LoadBalancer"`
}

// Router holds the router configuration.
type TraefikRouter struct {
	EntryPoints []string                `json:"entryPoints" toml:"entryPoints" yaml:"EntryPoints"`
	Middlewares []string                `json:"middlewares" toml:"middlewares" yaml:"Middlewares"`
	Service     string                  `json:"service" toml:"service" yaml:"Service"`
	Rule        string                  `json:"rule" toml:"rule" yaml:"Rule"`
	Priority    int                     `json:"priority" toml:"priority,omitzero" yaml:"Priority"`
	TLS         *TraefikRouterTLSConfig `json:"tls" toml:"tls" yaml:"Tls" label:"allowEmpty"`
}

// RouterTLSConfig holds the TLS configuration for a router
type TraefikRouterTLSConfig struct {
	Options string `json:"options" toml:"options" yaml:"Options"`
}

// TCPRouter holds the router configuration.
type TraefikTCPRouter struct {
	EntryPoints []string                   `json:"entryPoints" toml:"entryPoints" yaml:"EntryPoints"`
	Service     string                     `json:"service" toml:"service" yaml:"Service"`
	Rule        string                     `json:"rule" toml:"rule" yaml:"Rule"`
	TLS         *TraefikRouterTCPTLSConfig `json:"tls" toml:"tls" yaml:"Tls" label:"allowEmpty"`
}

// RouterTCPTLSConfig holds the TLS configuration for a router
type TraefikRouterTCPTLSConfig struct {
	Passthrough bool   `json:"passthrough" toml:"passthrough" yaml:"Passthrough"`
	Options     string `json:"options" toml:"options" yaml:"Options"`
}

// LoadBalancerService holds the LoadBalancerService configuration.
type TraefikLoadBalancerService struct {
	Stickiness         *TraefikStickiness         `json:"stickiness" toml:"stickiness" yaml:"Stickiness" label:"allowEmpty"`
	Servers            []TraefikServer            `json:"servers" toml:"servers" yaml:"Servers" label-slice-as-struct:"server"`
	HealthCheck        *TraefikHealthCheck        `json:"healthCheck" toml:"healthCheck" yaml:"HealthCheck"`
	PassHostHeader     bool                       `json:"passHostHeader" toml:"passHostHeader" yaml:"PassHostHeader"`
	ResponseForwarding *TraefikResponseForwarding `json:"responseForwarding" toml:"responseForwarding" yaml:"ResponseForwarding"`
}

type TraefikResponseForwarding struct {
	FlushInterval string `json:"flushInterval" toml:"flushInterval" yaml:"FlushInterval"`
}

// HealthCheck is the HealthCheck definition.
type TraefikHealthCheck struct {
	Path            string            `json:"path"`
	Host            string            `json:"host"`
	Scheme          string            `json:"scheme"`
	IntervalSeconds int64             `json:"intervalSeconds"`
	TimeoutSeconds  int64             `json:"timeoutSeconds"`
	Headers         map[string]string `json:"headers"`
}

// Server holds the server configuration.
type TraefikServer struct {
	URL    string `json:"url" toml:"url" yaml:"Url" label:"-"`
	Scheme string `toml:"-" json:"-" yaml:"-"`
	Port   string `toml:"-" json:"-" yaml:"-"`
}

// TCPServer holds a TCP Server configuration
type TraefikTCPServer struct {
	Address string `json:"address" toml:"address" yaml:"Address" label:"-"`
	Port    string `toml:"-" json:"-" yaml:"-"`
}

type TraefikStickiness struct {
	CookieName     string `json:"cookieName" toml:"cookieName" yaml:"CookieName"`
	SecureCookie   bool   `json:"secureCookie" toml:"secureCookie" yaml:"SecureCookie"`
	HTTPOnlyCookie bool   `json:"httpOnlyCookie" toml:"httpOnlyCookie" yaml:"HttpOnlyCookie"`
}

// TCPLoadBalancerService holds the LoadBalancerService configuration.
type TraefikTCPLoadBalancerService struct {
	Servers []TraefikTCPServer `json:"servers" toml:"servers" yaml:"Servers" label-slice-as-struct:"server" label-slice-as-struct:"server"`
}

// Provider holds configurations of the provider.
type TraefikRancher struct {
	Constraints               string `description:"Constraints is an expression that Traefik matches against the container's labels to determine whether to create any route for that container." json:"constraints" toml:"constraints" yaml:"Constraints" export:"true"`
	Watch                     bool   `description:"Watch provider." json:"watch" toml:"watch" yaml:"Watch" export:"true"`
	DefaultRule               string `description:"Default rule." json:"defaultRule" toml:"defaultRule" yaml:"DefaultRule"`
	ExposedByDefault          bool   `description:"Expose containers by default." json:"exposedByDefault" toml:"exposedByDefault" yaml:"ExposedByDefault" export:"true"`
	EnableServiceHealthFilter bool   `description:"Filter services with unhealthy states and inactive states." json:"enableServiceHealthFilter" toml:"enableServiceHealthFilter" yaml:"EnableServiceHealthFilter" export:"true"`
	RefreshSeconds            int    `description:"Defines the polling interval in seconds." json:"refreshSeconds" toml:"refreshSeconds" yaml:"RefreshSeconds" export:"true"`
	IntervalPoll              bool   `description:"Poll the Rancher metadata service every 'rancher.refreshseconds' (less accurate)." json:"intervalPoll" toml:"intervalPoll" yaml:"IntervalPoll"`
	Prefix                    string `description:"Prefix used for accessing the Rancher metadata service." json:"prefix" toml:"prefix" yaml:"Prefix"`
	defaultRuleTpl            *template.Template
}

type TraefikLog struct {
	Format string `toml:"format"`
}

type TraefikStaticBackend struct {
}

type TraefikDynamicBackend struct {
}

type TraefikEntryPoints map[string]*TraefikEntryPoint

// EntryPoint holds the entry point configuration.
type TraefikEntryPoint struct {
	Address          string                       `description:"Entry point address." json:"address" toml:"address" yaml:"Address"`
	Transport        *TraefikEntryPointsTransport `description:"Configures communication between clients and Traefik." json:"transport" toml:"transport" yaml:"Transport"`
	ProxyProtocol    *TraefikProxyProtocol        `description:"Proxy-Protocol configuration." json:"proxyProtocol" toml:"proxyProtocol" yaml:"ProxyProtocol" label:"allowEmpty"`
	ForwardedHeaders *TraefikForwardedHeaders     `description:"Trust client forwarding headers." json:"forwardedHeaders" toml:"forwardedHeaders" yaml:"ForwardedHeaders"`
}

// ForwardedHeaders Trust client forwarding headers.
type TraefikForwardedHeaders struct {
	Insecure   bool     `description:"Trust all forwarded headers." json:"insecure" toml:"insecure" yaml:"Insecure" export:"true"`
	TrustedIPs []string `description:"Trust only forwarded headers from selected IPs." json:"trustedIPs" toml:"trustedIPs" yaml:"TrustedIPs"`
}

// ProxyProtocol contains Proxy-Protocol configuration.
type TraefikProxyProtocol struct {
	Insecure   bool     `description:"Trust all." json:"insecure" toml:"insecure" yaml:"Insecure" export:"true"`
	TrustedIPs []string `description:"Trust only selected IPs." json:"trustedIPs" toml:"trustedIPs" yaml:"TrustedIPs"`
}

// EntryPointsTransport configures communication between clients and Traefik.
type TraefikEntryPointsTransport struct {
	LifeCycle          *TraefikLifeCycle          `description:"Timeouts influencing the server life cycle." json:"lifeCycle" toml:"lifeCycle" yaml:"LifeCycle" export:"true" export:"true"`
	RespondingTimeouts *TraefikRespondingTimeouts `description:"Timeouts for incoming requests to the Traefik instance." json:"respondingTimeouts" toml:"respondingTimeouts" yaml:"RespondingTimeouts" export:"true" export:"true"`
}

type TraefikK8sBackend struct {
	ServiceName             string             `yaml:"ServiceName" json:"servicename"`
	Namespace               string             `yaml:"Namespace" json:"namespace"`
	PortName                string             `yaml:"PortName" json:"portname"`
	EncryptedKubeconfigPath string             `yaml:"EncryptedKubeconfigPath" json:"encryptedkubeconfigpath"`
	EncryptedCert           string             `yaml:"EncryptedCert" json:"encryptedcert"`
	MasterIP                string             `yaml:"MasterIP" json:"masterip"`
	CACert                  string             `yaml:"CACert" json:"cacert"`
	Name                    string             `yaml:"Name" json:"name"`
	Provider                string             `yaml:"Provider" json:"provider"`
	Gke                     bool               `yaml:"Gke" json:"gke"`
	Eks                     bool               `yaml:"Eks" json:"eks"`
	ClusterName             string             `yaml:"ClusterName" json:"clustername"`
	Region                  string             `yaml:"Region" json:"region"`
	Zone                    string             `yaml:"Zone" json:"zone"`
	ProjectID               string             `yaml:"ProjectID" json:"projectid"`
	FilterIPDestination     bool               `yaml:"FilterIPDestination" json:"filteripdestination"`
	PollingInterval         string             `yaml:"PollingInterval" json:"pollinginterval"`
	KubeconfigPath          string             `yaml:"KubeconfigPath" json:"kubeconfigpath"`
	GkeInfo                 *container.Cluster `yaml:"GkeInfo" json:"gkeinfo"`
	FrontendRoute           string             `yaml:"FrontendRoute" json:"FrontendRoute"`
}

type RouterLoadbalancerBackendK8s struct {
	ServiceName             string             `yaml:"ServiceName" json:"servicename"`
	Namespace               string             `yaml:"Namespace" json:"namespace"`
	PortName                string             `yaml:"PortName" json:"portname"`
	EncryptedKubeconfigPath string             `yaml:"EncryptedKubeconfigPath" json:"encryptedkubeconfigpath"`
	EncryptedCert           string             `yaml:"EncryptedCert" json:"encryptedcert"`
	MasterIP                string             `yaml:"MasterIP" json:"masterip"`
	CACert                  string             `yaml:"CACert" json:"cacert"`
	Provider                string             `yaml:"Provider" json:"provider"`
	Gke                     bool               `yaml:"Gke" json:"gke"`
	Eks                     bool               `yaml:"Eks" json:"eks"`
	GkeAliasIP              bool               `yaml:"GkeAliasIP" json:"gkealiasip"`
	SnatPods                bool               `yaml:"SnatPods" json:"snatpods"`
	ClusterName             string             `yaml:"ClusterName" json:"clustername"`
	Region                  string             `yaml:"Region" json:"region"`
	Zone                    string             `yaml:"Zone" json:"zone"`
	PodCidr                 string             `yaml:"PodCidr" json:"podcidr"`
	ProjectID               string             `yaml:"ProjectID" json:"projectid"`
	SnatServices            bool               `yaml:"SnatServices" json:"snatservices"`
	ServiceCidr             string             `yaml:"ServiceCidr" json:"servicecidr"`
	SnatNodes               bool               `yaml:"SnatNodes" json:"snatnodes"`
	FilterIPDestination     bool               `yaml:"FilterIPDestination" json:"filteripdestination"`
	PollingInterval         string             `yaml:"PollingInterval" json:"pollinginterval"`
	KubeconfigPath          string             `yaml:"KubeconfigPath" json:"kubeconfigpath"`
	GkeInfo                 *container.Cluster `yaml:"GkeInfo" json:"gkeinfo"`
}

type RouterLoadbalancerBackendStaticList struct {
	Address     string `yaml:"Address" json:"address"`
	Port        string `yaml:"Port" json:"port"`
	Description string `yaml:"Description" json:"description"`
	Weight      string `yaml:"Weight" json:"weight"`
}
type RouterServiceConfigsTeleport struct {
	//Config                 RouterServiceConfigTeleportConfig `yaml:"Config" json:"config"`
	WebCertFilePath string `yaml:"WebCertFilePath" json:"webcertfilepath"`
	WebKeyFilePath  string `yaml:"WebKeyFilePath" json:"webkeyfilepath"`
	Token           string `yaml:"Token" json:"token"`
	Proxy           bool   `yaml:"Proxy" json:"proxy"`
	Authentication  bool   `yaml:"Authentication" json:"authentication"`
	Node            bool   `yaml:"Node" json:"node"`
	SecondFactor    string `yaml:"SecondFactor" json:"secondfactor"`
	ProxyEndpoint   string `yaml:"ProxyEndpoint" json:"proxyendpoint"`
	AuthServer      string `yaml:"AuthServer" json:"authserver"`
	WebEndpoint     string `yaml:"WebEndpoint" json:"webendpoint"`
	Private         bool   `yaml:"Private" json:"private"`
}

type RouterServiceConfigsApi struct {
	EncryptedCredsPath string `yaml:"EncryptedCredsPath" json:"encryptedcredspath"`
	Username           string `yaml:"Username" json:"username"`
	Password           string `yaml:"Password" json:"password"`
	HeaderCreds        string `yaml:"HeaderCreds" json:"headercreds"`
}

type RouterServiceConfigsSnmp struct {
	RoCommunityString string   `yaml:"RoCommunityString" json:"rocommunitystring"`
	RwCommunityString string   `yaml:"RwCommunityString" json:"rwcommunitystring"`
	AgentAddress      string   `yaml:"AgentAddress" json:"agentaddress"`
	SysLocation       string   `yaml:"SysLocation" json:"syslocation"`
	SysContact        string   `yaml:"SysContact" json:"syscontact"`
	MonitoredServices []string `yaml:"MonitoredServices" json:"monitoredservices"`
}

type RouterServiceConfigsScope struct {
	Username           string `yaml:"Username" json:"username"`
	Password           string `yaml:"Password" json:"password"`
	EncryptedCredsPath string `yaml:"EncryptedCredsPath" json:"encryptedcredspath"`
	Proxy              bool   `yaml:"Proxy" json:"proxy"`
}

type RouterRoutes struct {
	Static   []RouterStaticRoutes `yaml:"Static" json:"static"`
	Bgp      []RouterBgpRoutes    `yaml:"Bgp" json:"bgp"`
	FlowSpec RouterFlowSpecRoutes `yaml:"FlowSpec" json:"flowspec"`
}

type RouterStaticRoutes struct {
	Prefix      string `yaml:"Prefix" json:"prefix"`
	NextHop     string `yaml:"NextHop" json:"nexthop"`
	Metric      string `yaml:"Metric" json:"metric"`
	Interface   string `yaml:"Interface" json:"interface"`
	Description string `yaml:"Description" json:"description"`
}

type RouterBgpDecoder struct {
	Prefix string               `json:"prefix"`
	Path   []RouterBgpGlobalRib `json:"path"`
}

type RouterBgpGlobalRib []struct {
	Nlri  BgpStatusNlri `json:"nlri"`
	Attrs BgpAttrs      `json:"attrs"`
}

type BgpAttrs []struct {
	NextHop NexthopStatus
	AsPaths AsPathStatus
}

type BgpStatusNlri struct {
	Prefix string `json:"prefix"`
}

type NexthopStatus struct {
	Type    int    `json:"type"`
	NextHop string `json:"nexthop"`
}
type AsPathStatus struct {
	Type   int    `json:"type"`
	AsPath string `json:"as_paths"`
}
type BGPApiInput struct {
	TargetIP string `json:""`
	Command  string `json:""`
}

type RouterBgpRoutes struct {
	Prefix           string   `yaml:"Prefix" json:"prefix"`
	NextHop          string   `yaml:"NextHop" json:"nexthop"`
	LocalPref        string   `yaml:"LocalPref" json:"localpref"`
	Med              string   `yaml:"Med" json:"med"`
	CommunityStrings []string `yaml:"CommunityStrings" json:"communitystrings"`
	Description      string   `yaml:"Description" json:"description"`
}

type RouterBgpIpv4Route struct {
	Route          string                  `yaml:"Route" json:"route"`
	Advertisements []RouterBgpRouteDetails `yaml:"Advertisement" json:"advertisement"`
}

type RouterBgpRouteDetails struct {
	Prefix           string    `yaml:"Prefix" json:"prefix"`
	NextHop          string    `yaml:"NextHop" json:"nexthop"`
	LocalPref        string    `yaml:"LocalPref" json:"localpref"`
	Med              string    `yaml:"Med" json:"med"`
	CommunityStrings []string  `yaml:"CommunityStrings" json:"communitystrings"`
	AsPaths          []uint32  `yaml:"AsPaths" json:"aspaths"`
	Descriptor       uint64    `yaml:"Descriptor" json:"descriptor"`
	Description      string    `yaml:"Description" json:"description"`
	Best             bool      `yaml:"Best" json:"best"`
	Stale            bool      `yaml:"Stale" json:"stale"`
	Age              time.Time `yaml:"Age" json:"age"`
	NeighborIP       string    `yaml:"NeighborIP" json:"neighborip"`
	SourceID         string    `yaml:"SourceID" json:"sourceid"`
}
type RouterFlowSpecRoutes struct {
	MatchInfo RouterFlowspecMatchInfo   `yaml:"MatchInfo" json:"matchinfo"`
	Protocols []RouterFlowspecProtocols `yaml:"Protocols" json:"protocols"`
	Protocol  RouterFlowspecProtocol    `yaml:"Protocol" json:"protocol"`
	// Fragments []RouterFlowspecFragments `yaml:"Fragments" json:"fragments"`
	Fragment RouterFlowspecFragment   `yaml:"Fragment" json:"fragment"`
	TcpFlags []RouterFlowspecTcpFlags `yaml:"TcpFlags" json:"tcpflags"`
	TcpFlag  RouterFlowspecTcpFlag    `yaml:"TcpFlag" json:"tcpflag"`
	// Item      RouterFlowspecItem        `yaml:"Item" json:"item"`
	Then  RouterFlowspecThen     `yaml:"Then" json:"then"`
	Rt    RouterFlowspecRt       `yaml:"Rt" json:"rt"`
	Match []RouterFlowSpecPolicy `yaml:"Match" json:"match"`
}

type RouterFlowspecRt struct {
	Rt string `yaml:"Rt" json:"rt"`
}
type RouterFlowspecAction struct {
	Sample         bool `yaml:"Sample" json:"sample"`
	Terminal       bool `yaml:"Terminal" json:"terminal"`
	SampleTerminal bool `yaml:"SampleTerminal" json:"sampleterminal"`
}
type RouterFlowspecThen struct {
	Accept         bool                 `yaml:"Accept" json:"accept"`
	Discard        bool                 `yaml:"Discard" json:"discard"`
	RateLimit      bool                 `yaml:"RateLimit" json:"ratelimit"`
	RateLimitValue string               `yaml:"RateLimitValue" json:"ratelimitvalue"`
	Redirect       bool                 `yaml:"Redirect" json:"redirect"`
	RedirectValue  string               `yaml:"RedirectValue" json:"redirectvalue"`
	Mark           bool                 `yaml:"Mark" json:"mark"`
	MarkValue      string               `yaml:"MarkValue" json:"markvalue"`
	Action         RouterFlowspecAction `yaml:"Action" json:"action"`
}
type RouterFlowspecFragment struct {
	DontFragment  bool `yaml:"DontFragment" json:"dontfragment"`
	IsFragment    bool `yaml:"IsFragment" json:"isfragment"`
	FirstFragment bool `yaml:"FirstFragment" json:"firstfragment"`
	LastFragment  bool `yaml:"LastFragment" json:"lastfragment"`
	NotAFragment  bool `yaml:"NotAFragment" json:"notafragment"`
}
type RouterFlowspecProtocol struct {
	Egp     bool `yaml:"Egp" json:"egp"`
	Gre     bool `yaml:"Gre" json:"gre"`
	Icmp    bool `yaml:"Icmp" json:"icmp"`
	Igmp    bool `yaml:"Igmp" json:"igmp"`
	Igp     bool `yaml:"Igp" json:"igp"`
	Ipip    bool `yaml:"Ipip" json:"ipip"`
	Ospf    bool `yaml:"Ospf" json:"ospf"`
	Pim     bool `yaml:"Pim" json:"pim"`
	Rsvp    bool `yaml:"Rsvp" json:"rsvp"`
	Sctp    bool `yaml:"Sctp" json:"sctp"`
	Tcp     bool `yaml:"Tcp" json:"tcp"`
	Udp     bool `yaml:"Udp" json:"udp"`
	Unknown bool `yaml:"Unknown" json:"unknown"`
}

type RouterFlowspecProtocols struct {
	Protocol RouterFlowspecProtocol `yaml:"Protocol" json:"protocol"`
}

type RouterFlowspecMatchInfo struct {
	Description     string                   `yaml:"Description" json:"description"`
	Destination     string                   `yaml:"Destination" json:"destination"`
	Source          string                   `yaml:"Source" json:"source"`
	TcpFlags        []RouterFlowspecTcpFlags `yaml:"TcpFlags" json:"tcpflags"`
	Port            string                   `yaml:"Port" json:"port"`
	DestinationPort string                   `yaml:"DestinationPort" json:"destinationport"`
	SourcePort      string                   `yaml:"SourcePort" json:"sourceport"`
	IcmpType        string                   `yaml:"IcmpType" json:"icmptype"`
	IcmpCode        string                   `yaml:"IcmpCode" json:"icmpcode"`
	PacketLength    string                   `yaml:"PacketLength" json:"packetlength"`
	Dscp            string                   `yaml:"Dscp" json:"dscp"`
	Label           string                   `yaml:"Label" json:"label"`
}

type RouterFlowspecTcpFlags struct {
	TcpFlag RouterFlowspecTcpFlag `yaml:"TcpFlag" json:"tcpflag"`
}

type RouterFlowspecTcpFlag struct {
	Flag string `yaml:"Flag" json:"flag"`
}

type RouterFlowSpecPolicy struct {
	Source      string `yaml:"Source" json:"source"`
	Destination string `yaml:"Destination" json:"destination"`
	Action      string `yaml:"Action" json:"action"`
}

type RouterServiceConfigsScheduler struct {
	Memory            string `yaml:"Memory" json:"memory"`
	MemorySwap        string `yaml:"MemorySwap" json:"memoryswap"`
	MemoryReservation string `yaml:"MemoryReservation" json:"memoryreservation"`
	CPUPercent        int64  `yaml:"CPUPercent" json:"cpupercent"`
	CPUs              int64  `yaml:"CPUs" json:"cpus"`
}

type RouterServiceConfigsRouter struct {
	MemCgroup         string `yaml:"MemCgroup" json:"memcgroup"`
	CpuCgroup         string `yaml:"CpuCgroup" json:"cpucgroup"`
	Memory            string `yaml:"Memory" json:"memory"`
	MemorySwap        string `yaml:"MemorySwap" json:"memoryswap"`
	MemoryReservation string `yaml:"MemoryReservation" json:"memoryreservation"`
	CPUPercent        int64  `yaml:"CPUPercent" json:"cpupercent"`
	CPUs              int64  `yaml:"CPUs" json:"cpus"`
}

type RouterServiceConfigsSlack struct {
	WebHook string `yaml:"WebHook" json:"webhook"`
	Channel string `yaml:"Channel" json:"channel"`
}

type RouterServiceConfigsNat struct {
	Interface string               `yaml:"Interface" json:"interface"`
	NatRules  []RouterNatRuleType  `yaml:"NatRules" json:"natrules"`
	SnatRules []RouterSnatRuleType `yaml:"SnatRules" json:"snatrules"`
}

type RouterSnatRuleType struct {
	Name                 string `yaml:"Name" json:"name"`
	SnatAddress          string `yaml:"SnatAddress" json:"snataddress"`
	SnatAddressInterface string `yaml:"SnatAddressInterface" json:"snataddressinterface"`
	SourcePrefix         string `yaml:"SourcePrefix" json:"sourceprefix"`
	DestinationPrefix    string `yaml:"DestinationPrefix" json:"destinationprefix"`
	InInterface          string `yaml:"InInterface" json:"ininterface"`
	OutInterface         string `yaml:"OutInterface" json:"outinterface"`
}

type RouterNatRuleType struct {
	InterfaceRule bool           `yaml:"InterfaceRule" json:"interfacerule"`
	PrefixRule    bool           `yaml:"PrefixRule" json:"prefixrule"`
	Rule          RouterNatRules `yaml:"Rule" json:"rule"`
}
type RouterNatRules struct {
	IncomingInterface string `yaml:"IncomingInterface" json:"incominginterface"`
	OutgoingInterface string `yaml:"OutgoingInterface" json:"outgoinginterface"`
	SourcePrefix      string `yaml:"SourcePrefix" json:"sourceprefix"`
	DestinationPrefix string `yaml:"DestinationPrefix" json:"destinationprefix"`
}

type NatOutput struct {
	Num         string `yaml:"Num" json:"num"`
	Pkts        string `yaml:"Pkts" json:"pkts"`
	Bytes       string `yaml:"Bytes" json:"bytes"`
	Target      string `yaml:"Target" json:"target"`
	Prot        string `yaml:"Prot" json:"prot"`
	Opt         string `yaml:"Opt" json:"opt"`
	In          string `yaml:"In" json:"in"`
	Out         string `yaml:"Out" json:"out"`
	Source      string `yaml:"Source" json:"source"`
	Destination string `yaml:"Destination" json:"destination"`
	Comment     string `yaml:"Comment" json:"comment"`
}
type RouterServiceConfigsBgp struct {
	RouterID                string                      `yaml:"RouterID" json:"routerid"`
	ZebraVersion            string                      `yaml:"ZebraVersion" json:"zebraversion"`
	Neighbors               []BgpNeighbors              `yaml:"Neighbors" json:"neighbors"`
	SourceInterface         string                      `yaml:"SourceInterface" json:"sourceinterface"`
	Port                    int32                       `yaml:"Port" json:"port"`
	MultiPath               bool                        `yaml:"MultiPath" json:"multipath"`
	Memory                  string                      `yaml:"Memory" json:"memory"`
	MemorySwap              string                      `yaml:"MemorySwap" json:"memoryswap"`
	MemoryReservation       string                      `yaml:"MemoryReservation" json:"memoryreservation"`
	CPUPercent              int64                       `yaml:"CPUPercent" json:"cpupercent"`
	CPUs                    int64                       `yaml:"CPUs" json:"cpus"`
	Asn                     uint32                      `yaml:"Asn" json:"asn"`
	Redistribute            []InstallProtocolType       `yaml:"Redistribute" json:"redistribute"`
	AcceptedCommunityString []string                    `yaml:"AcceptedCommunityString" json:"acceptedcommunitystring"`
	RejectedCommunityString []string                    `yaml:"RejectedCommunityString" json:"rejectedcommunitystring"`
	AcceptedPrefix          []string                    `yaml:"AcceptedPrefix" json:"acceptedprefix"`
	IngressAcceptedPrefix   []string                    `yaml:"IngressAcceptedPrefix" json:"ingressacceptedprefix"`
	EgressAcceptedPrefix    []string                    `yaml:"EgressAcceptedPrefix" json:"egressacceptedprefix"`
	RejectedPrefix          []string                    `yaml:"RejectedPrefix" json:"rejectedprefix"`
	IngressRejectedPrefix   []string                    `yaml:"IngressRejectedPrefix" json:"ingressrejectedprefix"`
	EgressRejectedPrefix    []string                    `yaml:"EgressRejectedPrefix" json:"egressrejectedprefix"`
	IngressRejectedRange    []string                    `yaml:"IngressRejectedRange" json:"ingressrejectedrange"`
	EgressRejectedRange     []string                    `yaml:"EgressRejectedRange" json:"egressrejectedrange"`
	DynamicNeighbors        []BgpCustomDynamicNeighbors `yaml:"DynamicNeighbors" json:"dynamicneighbors"`
	Kubernetes              KubernetesPeering           `yaml:"Kubernetes" json:"kubernetes"`
}

type BgpNeighbors struct {
	Neighbor         Neighbor                    `yaml:"Neighbor" json:"neighbor"`
	PolicyStatements []BgpCustomPolicyStatements `yaml:"PolicyStatements" json:"policystatements"`
	NeighborSet      BgpNeighborSet              `yaml:"NeighborSet" json:"neighborset"`
}

type BgpCustomPolicyStatements struct {
	Name       string     `yaml:"Name" json:"name"`
	PolicyName string     `yaml:"PolicyName" json:"policyname"`
	Ingress    bool       `yaml:"Ingress" json:"ingress"`
	Egress     bool       `yaml:"Egress" json:"egress"`
	Conditions Conditions `yaml:"Conditions" json:"conditions"`
	Actions    Actions    `yaml:"Actions" json:"actions"`
}

type BgpNeighborSet struct {
	Name   string   `yaml:"Name" json:"name"`
	Prefix []string `yaml:"Prefix" json:"prefix"`
}

type KubernetesPeering struct {
	Description         string                       `yaml:"Description" json:"description"`
	Secure              bool                         `yaml:"Secure" json:"secure"`
	LocalAsn            uint32                       `yaml:"LocalAsn" json:"localasn"`
	PeerAsn             uint32                       `yaml:"PeerAsn" json:"peerasn"`
	Prefix              string                       `yaml:"Prefix" json:"prefix"`
	PeerGroup           string                       `yaml:"PeerGroup" json:"peergroup"`
	K8SCommunity        string                       `yaml:"K8SCommunity" json:"k8scommunity"`
	PodNetwork          string                       `yaml:"PodNetwork" json:"podnetwork"`
	HostNetwork         string                       `yaml:"HostNetwork" json:"hostnetwork"`
	NatPods             bool                         `yaml:"NatPods" json:"natpods"`
	NatInterface        string                       `yaml:"NatInterface" json:"natinterface"`
	ServiceNetwork      string                       `yaml:"ServiceNetwork" json:"servicenetwork"`
	MultiPath           bool                         `yaml:"MultiPath" json:"multipath"`
	VrfInterface        string                       `yaml:"VrfInterface" json:"vrfinterface"`
	VrfInterfaceIP      string                       `yaml:"VrfInterfaceIP" json:"vrfinterfaceip"`
	VrfInterfaceGW      string                       `yaml:"VrfInterfaceGW" json:"vrfinterfacegw"`
	VrfInterfaceSM      string                       `yaml:"VrfInterfaceSM" json:"vrfinterfacesm"`
	CorpInterfaceSubnet string                       `yaml:"CorpInterfaceSubnet" json:"corpinterfacesubnet"`
	C3RWeavePeers       []string                     `yaml:"C3RWeavePeers" json:"c3rweavepeers"`
	DiscoverWeavePeers  KubernetesDiscoverWeavePeers `yaml:"DiscoverWeavePeers" json:"discoverweavepeers"`
	C3RBgpPeers         []string                     `yaml:"C3RBgpPeers" json:"c3rbgppeers"`
	WeaveStaticIP       string                       `yaml:"WeaveStaticIP" json:"weavestaticip"`
	Platform            string                       `yaml:"Platform" json:"platform"`
}

type KubernetesDiscoverWeavePeers struct {
	ServiceName   string `yaml:"ServiceName" json:"servicename"`
	Namespace     string `yaml:"Namespace" json:"namespace"`
	EncryptedCert string `yaml:"EncryptedCert" json:"encryptedcert"`
	MasterIP      string `yaml:"MasterIP" json:"masterip"`
	CACert        string `yaml:"CACert" json:"cacert"`
}

type RouterServiceEndpoints struct {
	Scope              string `yaml:"Scope" json:"scope"`
	ScopeUI            string `yaml:"ScopeUI" json:"scopeui"`
	Prometheus         string `yaml:"Prometheus" json:"prometheus"`
	Mqtt               string `yaml:"Mqtt" json:"mqtt"`
	PubsubPushEndpoint string `yaml:"PubsubPushEndpoint" json:"pubsubpushendpoint"`
	DNS                string `yaml:"DNS" json:"dns"`
	NetdataMaster      string `yaml:"NetdataMaster" json:"netdatamaster"`
}

type RouterDiscoveryRecords struct {
	Vxlan string `yaml:"Vxlan" json:"vxlan"`
	Scope string `yaml:"Scope" json:"scope"`
}

type RouterVxlan struct {
	PrivateSubnet      string   `yaml:"PrivateSubnet" json:"privatesubnet"`
	StaticIP           string   `yaml:"StaticIP" json:"staticip"`
	DisableFullMesh    bool     `yaml:"DisableFullMesh" json:"disablefullmesh"`
	StaticTargets      []string `yaml:"StaticTargets" json:"statictargets"`
	Mac                string   `yaml:"Mac" json:"mac"`
	EncryptionPassword string   `yaml:"EncryptionPassword" json:"encryptionpassword"`
	Environment        string   `yaml:"Environment" json:"environment"`
	EncryptedCredsPath string   `yaml:"EncryptedCredsPath" json:"encryptedcredspath"`
	SetMtu             string   `yaml:"SetMtu" json:"setmtu"`
	SetMss             string   `yaml:"SetMss" json:"setmss"`
	AutoMssClamping    bool     `yaml:"AutoMssClamping" json:"automssclamping"`
	BGPAdvertise       bool     `yaml:"BGPAdvertise" json:"bgpadvertise"`
	CommunityStrings   []string `yaml:"CommunityStrings" json:"communitystrings"`
	VRF                string   `yaml:"VRF" json:"vrf"`
	TrustedSubnets     []string `yaml:"TrustedSubnets" json:"trustedsubnets"`
}

type RouterCloudStorage struct {
	Aws                       RouterAwsStorage   `yaml:"Aws" json:"aws"`
	Gcp                       RouterGcpStorage   `yaml:"Gcp" json:"gcp"`
	Azure                     RouterAzureStorage `yaml:"Azure" json:"azure"`
	StateConfig               string             `yaml:"StateConfig" json:"stateconfig"`
	GlobalConfig              string             `yaml:"GlobalConfig" json:"globalconfig"`
	GlobalConfigFallback      string             `yaml:"GlobalConfigFallback" json:"globalconfigfallback"`
	EnvironmentConfig         string             `yaml:"EnvironmentConfig" json:"environmentconfig"`
	EnvironmentConfigFallback string             `yaml:"EnvironmentConfigFallback" json:"environmentconfigfallback"`
	StateTimestamp            time.Time          `yaml:"StateTimestamp" json:"statetimestamp"`
}

type RouterAwsStorage struct {
	Bucket string `yaml:"Bucket" json:"bucket"`
	Region string `yaml:"Region" json:"region"`
}

type RouterGcpStorage struct {
	Bucket string `yaml:"Bucket" json:"bucket"`
}

type RouterAzureStorage struct {
	Bucket string `yaml:"Bucket" json:"bucket"`
}

type RouterCloudAuth struct {
	Aws   RouterAwsAuth   `yaml:"Aws" json:"aws"`
	Gcp   RouterGcpAuth   `yaml:"Gcp" json:"gcp"`
	Azure RouterAzureAuth `yaml:"Azure" json:"azure"`
	C3    C3Auth          `yaml:"C3" json:"c3"`
}

type C3Auth struct {
	Token string `yaml:"Token" json:"token"`
}

type C3 struct {
	Enabled bool   `yaml:"Enabled" json:"enabled"`
	Data    C3Data `yaml:"Data" json:"data"`
}

type C3Data struct {
	ID          int64           `yaml:"ID" json:"id"`
	Prefixes    []C3Prefixes    `yaml:"Prefixes" json:"prefixes"`
	IPAddresses []C3IPAddresses `yaml:"IPAddresses" json:"ip_addresses"`
	Created     string          `yaml:"Created" json:"created"`
	LastUpdated string          `yaml:"LastUpdated" json:"last_updated"`
	UUID        string          `yaml:"UUID" json:"uuid"`
	Name        string          `yaml:"Name" json:"name"`
	Account     *int64          `yaml:"Account" json:"account"`
	Site        *C3Site         `yaml:"Site" json:"site"`
}

type C3Prefixes struct {
	ID          int64  `yaml:"ID" json:"id"`
	VRF         string `yaml:"VRF" json:"vrf"`
	Role        string `yaml:"Role" json:"role"`
	Created     string `yaml:"Created" json:"created"`
	LastUpdated string `yaml:"LastUpdated" json:"last_updated"`
	Prefix      string `yaml:"Prefix" json:"prefix"`
	Community   string `yaml:"Community" json:"community"`
	Status      string `yaml:"Status" json:"status"`
	Description string `yaml:"Description" json:"description"`
	Account     *int64 `yaml:"Account" json:"account"`
	Site        string `yaml:"Site" json:"site"`
	Router      *int64 `yaml:"Router" json:"router"`
	Vlan        string `yaml:"Vlan" json:"vlan"`
}

type C3IPAddresses struct {
	ID          int64  `yaml:"ID" json:"id"`
	Created     string `yaml:"Created" json:"created"`
	LastUpdated string `yaml:"LastUpdated" json:"last_updated"`
	Address     string `yaml:"Address" json:"address"`
	Status      string `yaml:"Status" json:"status"`
	Role        string `yaml:"Role" json:"role"`
	Description string `yaml:"Description" json:"description"`
	Account     *int64 `yaml:"Account" json:"account"`
	Router      *int64 `yaml:"Router" json:"router"`
	Site        string `yaml:"Site" json:"site"`
	VRF         *int64 `yaml:"VRF" json:"vrf"`
	NatInside   string `yaml:"NatInside" json:"nat_inside"`
}
type C3Site struct {
}

type RouterAwsAuth struct {
	AccessKey string `yaml:"AccessKey" json:"accesskey"`
	SecretKey string `yaml:"SecretKey" json:"secretkey"`
	Region    string `yaml:"Region" json:"region"`
}

type RouterGcpAuth struct {
	CredsPath string `yaml:"CredsPath" json:"credspath"`
	ProjectID string `yaml:"ProjectID" json:"projectid"`
}

type RouterAzureAuth struct {
}

// type RouterConfig struct {
// 	DeviceName      string             `yaml:"Devicename" json:"devicename"`
// 	Description     string             `yaml:"Description" json:"description"`
// 	Location        string             `yaml:"Location" json:"location"`
// 	StateManagement string             `yaml:"StateManagement" json:"statemanagement"`
// 	EnabledServices RouterEnabledServices `yaml:"EnabledServices" json:"enabledservices"`
// 	ServiceImages   RouterServiceImages   `yaml:"ServiceImages" json:"serviceimages"`
// 	KmsConfig       RouterKmsConfig       `yaml:"KmsConfig" json:"kmsconfig"`
// 	BucketConfig    RouterBucketConfig    `yaml:"BucketConfig" json:"bucketconfig"`
// 	VersionControl  RouterVersionControl  `yaml:"VersionControl" json:"versioncontrol"`
// }

// type RouterEnabledServices struct {
// 	Scheduler  bool `yaml:"Scheduler" json:"scheduler"`
// 	ConfigSync bool `yaml:"ConfigSync" json:"configsync"`
// 	Vxlan      bool `yaml:"Vxlan" json:"vxlan"`
// 	Bgp        bool `yaml:"Bgp" json:"bgp"`
// 	IPSlaec      bool `yaml:"IPSlaec" json:"ipsec"`
// 	Kms        bool `yaml:"Kms" json:"kms"`
// }

// type RouterServiceImages struct {
// 	Scheduler  string `yaml:"Scheduler" json:"scheduler"`
// 	ConfigSync string `yaml:"ConfigSync" json:"configsync"`
// 	Vxlan      string `yaml:"Vxlan" json:"vxlan"`
// 	Bgp        string `yaml:"Bgp" json:"bgp"`
// 	Kms        string `yaml:"Kms" json:"kms"`
// }

// type RouterKmsConfig struct {
// 	Provider string `yaml:"Provider" json:"provider"`
// 	Region   string `yaml:"Region" json:"region"`
// }

// type RouterBucketConfig struct {
// 	Provider string `yaml:"Provider" json:"provider"`
// 	Region   string `yaml:"Region" json:"region"`
// }

// type RouterVersionControl struct {
// 	Provider string `yaml:"Provider" json:"provider"`
// }

type RouterCloudKms struct {
	Aws   RouterAwsKms   `yaml:"Aws" json:"aws"`
	Gcp   RouterGcpKms   `yaml:"Gcp" json:"gcp"`
	Azure RouterAzureKms `yaml:"Azure" json:"azure"`
}

type RouterAwsKms struct {
	Region string `yaml:"Region" json:"region"`
	KeyID  string `yaml:"KeyID" json:"keyid"`
}

type RouterGcpKms struct {
	KeyringID   string `yaml:"KeyringID" json:"keyringid"`
	CryptokeyID string `yaml:"CryptokeyID" json:"cryptokeyid"`
	Location    string `yaml:"Location" json:"location"`
}

type RouterAzureKms struct {
}

type RouterDockerRegistry struct {
	ServerAddress string                `yaml:"ServerAddress" json:"serveraddress"`
	Public        bool                  `yaml:"Public" json:"public"`
	Username      string                `yaml:"Username" json:"username"`
	Password      string                `yaml:"Password" json:"password"`
	Images        RouterContainerImages `yaml:"Images" json:"images"`
}

type RouterContainerImages struct {
	Router       string `yaml:"Router" json:"router"`
	Api          string `yaml:"Api" json:"api"`
	Bgp          string `yaml:"Bgp" json:"bgp"`
	Netdata      string `yaml:"Netdata" json:"netdata"`
	Iperf        string `yaml:"Iperf" json:"iperf"`
	Cadvisor     string `yaml:"Cadvisor" json:"cadvisor"`
	Scheduler    string `yaml:"Scheduler" json:"scheduler"`
	Loadbalancer string `yaml:"Loadbalancer" json:"loadbalancer"`
	Traefik      string `yaml:"Traefik" json:"traefik"`
	Configsync   string `yaml:"Configsync" json:"configsync"`
	Mqtt         string `yaml:"Mqtt" json:"mqtt"`
	HostApd      string `yaml:"HostApd" json:"hostapd"`
	K6           string `yaml:"K6" json:"k6"`
	Ips          string `yaml:"Ips" json:"ips"`
	IPSla        string `yaml:"IPSla" json:"ipsla"`
	Snat         string `yaml:"Snat" json:"snat"`
	Netflow      string `yaml:"Netflow" json:"netflow"`
	Exitmon      string `yaml:"Exitmon" json:"exitmon"`
	Heartbeat    string `yaml:"Heartbeat" json:"heartbeat"`
	K8sConnector string `yaml:"K8sConnector" json:"k8sconnector"`
	Cfgctl       string `yaml:"Cfgctl" json:"cfgctl"`
	Kubewatch    string `yaml:"Kubewatch" json:"kubewatch"`
	Echo         string `yaml:"Echo" json:"echo"`
	Routectl     string `yaml:"Routectl" json:"routectl"`
	Hactl        string `yaml:"Hactl" json:"hactl"`
}

type ApiRequestIperf struct {
	Endpoint string `json:"endpoint"`
}

// ApiRequestStruct : Struct for formatting various API calls
type ApiRequestStruct struct {
	ShellCommand RouterShellCommand `yaml:"ShellCommand" json:"shellcommand"`
}

type RouterShellCommand struct {
	Command string `yaml:"Command" json:"command"`
}

type BgpNeighborStruct struct {
	PeerAs               uint32         `yaml:"PeerAs" json:"peeras"`
	LocalAs              uint32         `yaml:"LocalAs" json:"localas"`
	NeighborAddress      string         `yaml:"NeighborAddress" json:"neighboraddress"`
	Family               []*api.AfiSafi `yaml:"Family" json:"family"`
	Vrf                  string         `yaml:"Vrf" json:"vrf"`
	RouteReflectorClient bool           `yaml:"RouteReflectorClient" json:"routereflectorclient"`
	RouteServerClient    bool           `yaml:"RouteServerClient" json:"routeserverclient"`
	RouteReflectorId     string         `yaml:"RouteReflectorId" json:"routereflectorid"`
	AllowOwnAs           bool           `yaml:"AllowOwnAs" json:"allowownas"`
	RemovePrivateAs      bool           `yaml:"RemovePrivateAs" json:"removeprivateas"`
	ReplacePeerAs        string         `yaml:"ReplacePeerAs" json:"replacepeeras"`
	PeerGroup            string         `yaml:"PeerGroup" json:"peergroup"`
}

// BgpConfig : Struct for Gobgp configuration file
type BgpConfig struct {
	Global            Global             `mapstructure:"global" json:"global"`
	Neighbors         []Neighbor         `mapstructure:"neighbors" json:"neighbors"`
	PeerGroups        []PeerGroup        `mapstructure:"peer-groups" json:"peer-groups"`
	RpkiServers       []RpkiServer       `mapstructure:"rpki-servers" json:"rpki-servers"`
	BmpServers        []BmpServer        `mapstructure:"bmp-servers" json:"bmp-servers"`
	MrtDump           []Mrt              `mapstructure:"mrt-dump" json:"mrt-dump"`
	Zebra             Zebra              `mapstructure:"zebra" json:"zebra"`
	Collector         Collector          `mapstructure:"collector" json:"collector"`
	DefinedSets       DefinedSets        `mapstructure:"defined-sets" json:"defined-sets"`
	PolicyDefinitions []PolicyDefinition `mapstructure:"policy-definitions" json:"policy-definitions"`
	DynamicNeighbors  []DynamicNeighbor  `mapstructure:"dynamic-neighbors" json:"dynamic-neighbors"`
}

type BgpCustomDynamicNeighbors struct {
	Name      string `yaml:"Name" json:"name"`
	Asn       uint32 `yaml:"Asn" json:"asn"`
	Prefix    string `yaml:"Prefix" json:"prefix"`
	MultiPath bool   `yaml:"MultiPath" json:"multipath"`
}

// typedef for typedef openconfig-types:std-regexp.
type StdRegexp string

// typedef for typedef openconfig-types:percentage.
type Percentage uint8

// typedef for typedef bgp-types:rr-cluster-id-type.
type RrClusterIdType string

// typedef for identity bgp-types:remove-private-as-option.
// set of options for configuring how private AS path numbers
// are removed from advertisements.
type RemovePrivateAsOption string

// typedef for typedef bgp-types:bgp-community-regexp-type.
type BgpCommunityRegexpType StdRegexp

// typedef for identity bgp-types:community-type.
// type describing variations of community attributes:
// STANDARD: standard BGP community [rfc1997]
// EXTENDED: extended BGP community [rfc4360]
// BOTH: both standard and extended community.
type CommunityType string

// typedef for typedef bgp-types:bgp-ext-community-type.
type BgpExtCommunityType string

// typedef for typedef bgp-types:bgp-std-community-type.
type BgpStdCommunityType string

// typedef for identity bgp-types:peer-type.
// labels a peer or peer group as explicitly internal or
// external.
type PeerType string

// typedef for identity bgp-types:bgp-session-direction.
// Type to describe the direction of NLRI transmission.
type BgpSessionDirection string

// typedef for identity bgp-types:bgp-origin-attr-type.
// Type definition for standard BGP origin attribute.
type BgpOriginAttrType string

// typedef for identity bgp-types:afi-safi-type.
// Base identity type for AFI,SAFI tuples for BGP-4.
type AfiSafiType string

// typedef for identity bgp-types:bgp-capability.
// Base identity for a BGP capability.
type BgpCapability string

// typedef for identity bgp-types:bgp-well-known-std-community.
// Reserved communities within the standard community space
// defined by RFC1997. These communities must fall within the
// range 0x00000000 to 0xFFFFFFFF.
type BgpWellKnownStdCommunity string

// typedef for identity ptypes:match-set-options-restricted-type.
// Options that govern the behavior of a match statement.  The
// default behavior is ANY, i.e., the given value matches any
// of the members of the defined set.  Note this type is a
// restricted version of the match-set-options-type.
type MatchSetOptionsRestrictedType string

// typedef for identity ptypes:match-set-options-type.
// Options that govern the behavior of a match statement.  The
// default behavior is ANY, i.e., the given value matches any
// of the members of the defined set.
type MatchSetOptionsType string

// typedef for typedef ptypes:tag-type.
type TagType string

// typedef for identity ptypes:install-protocol-type.
// Base type for protocols which can install prefixes into the
// RIB.
type InstallProtocolType string

// typedef for identity ptypes:attribute-comparison.
// base type for supported comparison operators on route
// attributes.
type AttributeComparison string

// typedef for identity rpol:route-disposition.
// Select the final disposition for the route, either
// accept or reject.
type RouteDisposition string

// typedef for identity rpol:route-type.
// Condition to check the route type in the route update.
type RouteType string

// typedef for identity rpol:default-policy-type.
// type used to specify default route disposition in
// a policy chain.
type DefaultPolicyType string

// typedef for identity bgp:session-state.
// Operational state of the BGP peer.
// type SessionState string
type SessionState int

// typedef for identity bgp:admin-state.
type AdminState string

// typedef for identity bgp:mode.
// Ths leaf indicates the mode of operation of BGP graceful
// restart with the peer.
type Mode string

// typedef for typedef bgp-pol:bgp-next-hop-type.
type BgpNextHopType string

// typedef for typedef bgp-pol:bgp-as-path-prepend-repeat.
type BgpAsPathPrependRepeat uint8

// typedef for typedef bgp-pol:bgp-set-med-type.
type BgpSetMedType string

// typedef for identity bgp-pol:bgp-set-community-option-type.
// Type definition for options when setting the community
// attribute in a policy action.
type BgpSetCommunityOptionType string

// typedef for identity gobgp:bmp-route-monitoring-policy-type.
type BmpRouteMonitoringPolicyType string

// typedef for identity gobgp:mrt-type.
type MrtType string

// typedef for identity gobgp:rpki-validation-result-type.
// indicate the validation result of RPKI based on ROA.
type RpkiValidationResultType string

// struct for container gobgp:state.
type DynamicNeighborState struct {
	// original -> gobgp:prefix
	Prefix string `mapstructure:"prefix" json:"prefix"`
	// original -> gobgp:peer-group
	PeerGroup string `mapstructure:"peer-group" json:"peer-group"`
}

// struct for container gobgp:config.
type DynamicNeighborConfig struct {
	// original -> gobgp:prefix
	Prefix string `mapstructure:"prefix" json:"prefix"`
	// original -> gobgp:peer-group
	PeerGroup string `mapstructure:"peer-group" json:"peer-group"`
}

// struct for container gobgp:dynamic-neighbor.
type DynamicNeighbor struct {
	// original -> gobgp:prefix
	// original -> gobgp:dynamic-neighbor-config
	Config DynamicNeighborConfig `mapstructure:"config" json:"config"`
	// original -> gobgp:dynamic-neighbor-state
	State DynamicNeighborState `mapstructure:"state" json:"state"`
}

// struct for container gobgp:state.
type CollectorState struct {
	// original -> gobgp:url
	Url string `mapstructure:"url" json:"url"`
	// original -> gobgp:db-name
	DbName string `mapstructure:"db-name" json:"db-name"`
	// original -> gobgp:table-dump-interval
	TableDumpInterval uint64 `mapstructure:"table-dump-interval" json:"table-dump-interval"`
}

// struct for container gobgp:config.
type CollectorConfig struct {
	// original -> gobgp:url
	Url string `mapstructure:"url" json:"url"`
	// original -> gobgp:db-name
	DbName string `mapstructure:"db-name" json:"db-name"`
	// original -> gobgp:table-dump-interval
	TableDumpInterval uint64 `mapstructure:"table-dump-interval" json:"table-dump-interval"`
}

// struct for container gobgp:collector.
type Collector struct {
	// original -> gobgp:collector-config
	Config CollectorConfig `mapstructure:"config" json:"config"`
	// original -> gobgp:collector-state
	State CollectorState `mapstructure:"state" json:"state"`
}

// struct for container gobgp:state.
type ZebraState struct {
	// original -> gobgp:enabled
	// gobgp:enabled's original type is boolean.
	// Configure enabling to connect to zebra.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> gobgp:url
	// Configure url for zebra.
	Url string `mapstructure:"url" json:"url"`
	// original -> gobgp:redistribute-route-type
	RedistributeRouteTypeList []InstallProtocolType `mapstructure:"redistribute-route-type-list" json:"redistribute-route-type-list"`
	// original -> gobgp:version
	// Configure version of zebra protocol.  Default is 2. Supported up to 3.
	Version uint8 `mapstructure:"version" json:"version"`
	// original -> gobgp:nexthop-trigger-enable
	// gobgp:nexthop-trigger-enable's original type is boolean.
	NexthopTriggerEnable bool `mapstructure:"nexthop-trigger-enable" json:"nexthop-trigger-enable"`
	// original -> gobgp:nexthop-trigger-delay
	NexthopTriggerDelay uint8 `mapstructure:"nexthop-trigger-delay" json:"nexthop-trigger-delay"`
}

// struct for container gobgp:config.
type ZebraConfig struct {
	// original -> gobgp:enabled
	// gobgp:enabled's original type is boolean.
	// Configure enabling to connect to zebra.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> gobgp:url
	// Configure url for zebra.
	Url string `mapstructure:"url" json:"url"`
	// original -> gobgp:redistribute-route-type
	RedistributeRouteTypeList []InstallProtocolType `mapstructure:"redistribute-route-type-list" json:"redistribute-route-type-list"`
	// original -> gobgp:version
	// Configure version of zebra protocol.  Default is 2. Supported up to 3.
	Version uint8 `mapstructure:"version" json:"version"`
	// original -> gobgp:nexthop-trigger-enable
	// gobgp:nexthop-trigger-enable's original type is boolean.
	NexthopTriggerEnable bool `mapstructure:"nexthop-trigger-enable" json:"nexthop-trigger-enable"`
	// original -> gobgp:nexthop-trigger-delay
	NexthopTriggerDelay uint8 `mapstructure:"nexthop-trigger-delay" json:"nexthop-trigger-delay"`
}

// struct for container gobgp:zebra.
type Zebra struct {
	// original -> gobgp:zebra-config
	Config ZebraConfig `mapstructure:"config" json:"config"`
	// original -> gobgp:zebra-state
	State ZebraState `mapstructure:"state" json:"state"`
}

// struct for container gobgp:config.
type MrtConfig struct {
	// original -> gobgp:dump-type
	DumpType MrtType `mapstructure:"dump-type" json:"dump-type"`
	// original -> gobgp:file-name
	// Configures a file name to be written.
	FileName string `mapstructure:"file-name" json:"file-name"`
	// original -> gobgp:table-name
	// specify the table name with route server setup.
	TableName string `mapstructure:"table-name" json:"table-name"`
	// original -> gobgp:dump-interval
	DumpInterval uint64 `mapstructure:"dump-interval" json:"dump-interval"`
	// original -> gobgp:rotation-interval
	RotationInterval uint64 `mapstructure:"rotation-interval" json:"rotation-interval"`
}

// struct for container gobgp:mrt.
type Mrt struct {
	// original -> gobgp:file-name
	// original -> gobgp:mrt-config
	Config MrtConfig `mapstructure:"config" json:"config"`
}

// struct for container gobgp:state.
// Configured states of VRF.
type VrfState struct {
	// original -> gobgp:name
	// Unique name among all VRF instances.
	Name string `mapstructure:"name" json:"name"`
	// original -> gobgp:id
	// Unique identifier among all VRF instances.
	Id uint32 `mapstructure:"id" json:"id"`
	// original -> gobgp:rd
	// Route Distinguisher for this VRF.
	Rd string `mapstructure:"rd" json:"rd"`
	// original -> gobgp:import-rt
	// List of import Route Targets for this VRF.
	ImportRtList []string `mapstructure:"import-rt-list" json:"import-rt-list"`
	// original -> gobgp:export-rt
	// List of export Route Targets for this VRF.
	ExportRtList []string `mapstructure:"export-rt-list" json:"export-rt-list"`
}

// struct for container gobgp:config.
// Configuration parameters for VRF.
type VrfConfig struct {
	// original -> gobgp:name
	// Unique name among all VRF instances.
	Name string `mapstructure:"name" json:"name"`
	// original -> gobgp:id
	// Unique identifier among all VRF instances.
	Id uint32 `mapstructure:"id" json:"id"`
	// original -> gobgp:rd
	// Route Distinguisher for this VRF.
	Rd string `mapstructure:"rd" json:"rd"`
	// original -> gobgp:import-rt
	// List of import Route Targets for this VRF.
	ImportRtList []string `mapstructure:"import-rt-list" json:"import-rt-list"`
	// original -> gobgp:export-rt
	// List of export Route Targets for this VRF.
	ExportRtList []string `mapstructure:"export-rt-list" json:"export-rt-list"`
	// original -> gobgp:both-rt
	// List of both import and export Route Targets for this VRF. Each
	// configuration for import and export Route Targets will be preferred.
	BothRtList []string `mapstructure:"both-rt-list" json:"both-rt-list"`
}

// struct for container gobgp:vrf.
// VRF instance configurations on the local system.
type Vrf struct {
	// original -> gobgp:name
	// original -> gobgp:vrf-config
	// Configuration parameters for VRF.
	Config VrfConfig `mapstructure:"config" json:"config"`
	// original -> gobgp:vrf-state
	// Configured states of VRF.
	State VrfState `mapstructure:"state" json:"state"`
}

// struct for container gobgp:state.
// Configuration parameters relating to BMP server.
type BmpServerState struct {
	// original -> gobgp:address
	// gobgp:address's original type is inet:ip-address.
	// Reference to the address of the BMP server used as
	// a key in the BMP server list.
	Address string `mapstructure:"address" json:"address"`
	// original -> gobgp:port
	// Reference to the port of the BMP server.
	Port uint32 `mapstructure:"port" json:"port"`
	// original -> gobgp:route-monitoring-policy
	RouteMonitoringPolicy BmpRouteMonitoringPolicyType `mapstructure:"route-monitoring-policy" json:"route-monitoring-policy"`
	// original -> gobgp:statistics-timeout
	// Interval seconds of statistics messages sent to BMP server.
	StatisticsTimeout uint16 `mapstructure:"statistics-timeout" json:"statistics-timeout"`
	// original -> gobgp:route-mirroring-enabled
	// gobgp:route-mirroring-enabled's original type is boolean.
	// Enable feature for mirroring of received BGP messages
	// mainly for debugging purpose.
	RouteMirroringEnabled bool `mapstructure:"route-mirroring-enabled" json:"route-mirroring-enabled"`
}

// struct for container gobgp:config.
// Configuration parameters relating to BMP server.
type BmpServerConfig struct {
	// original -> gobgp:address
	// gobgp:address's original type is inet:ip-address.
	// Reference to the address of the BMP server used as
	// a key in the BMP server list.
	Address string `mapstructure:"address" json:"address"`
	// original -> gobgp:port
	// Reference to the port of the BMP server.
	Port uint32 `mapstructure:"port" json:"port"`
	// original -> gobgp:route-monitoring-policy
	RouteMonitoringPolicy BmpRouteMonitoringPolicyType `mapstructure:"route-monitoring-policy" json:"route-monitoring-policy"`
	// original -> gobgp:statistics-timeout
	// Interval seconds of statistics messages sent to BMP server.
	StatisticsTimeout uint16 `mapstructure:"statistics-timeout" json:"statistics-timeout"`
	// original -> gobgp:route-mirroring-enabled
	// gobgp:route-mirroring-enabled's original type is boolean.
	// Enable feature for mirroring of received BGP messages
	// mainly for debugging purpose.
	RouteMirroringEnabled bool `mapstructure:"route-mirroring-enabled" json:"route-mirroring-enabled"`
}

// struct for container gobgp:bmp-server.
// List of BMP servers configured on the local system.
type BmpServer struct {
	// original -> gobgp:address
	// original -> gobgp:bmp-server-config
	// Configuration parameters relating to BMP server.
	Config BmpServerConfig `mapstructure:"config" json:"config"`
	// original -> gobgp:bmp-server-state
	// Configuration parameters relating to BMP server.
	State BmpServerState `mapstructure:"state" json:"state"`
}

// struct for container gobgp:rpki-received.
// Counters for reception RPKI Message types.
type RpkiReceived struct {
	// original -> gobgp:serial-notify
	// Number of serial notify message received from RPKI server.
	SerialNotify int64 `mapstructure:"serial-notify" json:"serial-notify"`
	// original -> gobgp:cache-reset
	// Number of cache reset message received from RPKI server.
	CacheReset int64 `mapstructure:"cache-reset" json:"cache-reset"`
	// original -> gobgp:cache-response
	// Number of cache response message received from RPKI server.
	CacheResponse int64 `mapstructure:"cache-response" json:"cache-response"`
	// original -> gobgp:ipv4-prefix
	// Number of ipv4 prefix message received from RPKI server.
	Ipv4Prefix int64 `mapstructure:"ipv4-prefix" json:"ipv4-prefix"`
	// original -> gobgp:ipv6-prefix
	// Number of ipv6 prefix message received from RPKI server.
	Ipv6Prefix int64 `mapstructure:"ipv6-prefix" json:"ipv6-prefix"`
	// original -> gobgp:end-of-data
	// Number of end of data message received from RPKI server.
	EndOfData int64 `mapstructure:"end-of-data" json:"end-of-data"`
	// original -> gobgp:error
	// Number of error message received from RPKI server.
	Error int64 `mapstructure:"error" json:"error"`
}

// struct for container gobgp:rpki-sent.
// Counters for transmission RPKI Message types.
type RpkiSent struct {
	// original -> gobgp:serial-query
	// Number of serial query message sent to RPKI server.
	SerialQuery int64 `mapstructure:"serial-query" json:"serial-query"`
	// original -> gobgp:reset-query
	// Number of reset query message sent to RPKI server.
	ResetQuery int64 `mapstructure:"reset-query" json:"reset-query"`
	// original -> gobgp:error
	// Number of error message sent to RPKI server.
	Error int64 `mapstructure:"error" json:"error"`
}

// struct for container gobgp:rpki-messages.
// Counters for transmission and reception RPKI Message types.
type RpkiMessages struct {
	// original -> gobgp:rpki-sent
	// Counters for transmission RPKI Message types.
	RpkiSent RpkiSent `mapstructure:"rpki-sent" json:"rpki-sent"`
	// original -> gobgp:rpki-received
	// Counters for reception RPKI Message types.
	RpkiReceived RpkiReceived `mapstructure:"rpki-received" json:"rpki-received"`
}

// struct for container gobgp:state.
// State information relating to RPKI server.
type RpkiServerState struct {
	// original -> gobgp:up
	// gobgp:up's original type is boolean.
	Up bool `mapstructure:"up" json:"up"`
	// original -> gobgp:serial-number
	SerialNumber uint32 `mapstructure:"serial-number" json:"serial-number"`
	// original -> gobgp:records-v4
	RecordsV4 uint32 `mapstructure:"records-v4" json:"records-v4"`
	// original -> gobgp:records-v6
	RecordsV6 uint32 `mapstructure:"records-v6" json:"records-v6"`
	// original -> gobgp:prefixes-v4
	PrefixesV4 uint32 `mapstructure:"prefixes-v4" json:"prefixes-v4"`
	// original -> gobgp:prefixes-v6
	PrefixesV6 uint32 `mapstructure:"prefixes-v6" json:"prefixes-v6"`
	// original -> gobgp:uptime
	// This timer determines the amount of time since the
	// RPKI last transitioned in of the Established state.
	Uptime int64 `mapstructure:"uptime" json:"uptime"`
	// original -> gobgp:downtime
	// This timer determines the amount of time since the
	// RPKI last transitioned out of the Established state.
	Downtime int64 `mapstructure:"downtime" json:"downtime"`
	// original -> gobgp:last-pdu-recv-time
	// last time the received an pdu message from RPKI server.
	LastPduRecvTime int64 `mapstructure:"last-pdu-recv-time" json:"last-pdu-recv-time"`
	// original -> gobgp:rpki-messages
	// Counters for transmission and reception RPKI Message types.
	RpkiMessages RpkiMessages `mapstructure:"rpki-messages" json:"rpki-messages"`
}

// struct for container gobgp:config.
// Configuration parameters relating to RPKI server.
type RpkiServerConfig struct {
	// original -> gobgp:address
	// gobgp:address's original type is inet:ip-address.
	// Reference to the address of the RPKI server used as
	// a key in the RPKI server list.
	Address string `mapstructure:"address" json:"address"`
	// original -> gobgp:port
	// Reference to the port of the RPKI server.
	Port uint32 `mapstructure:"port" json:"port"`
	// original -> gobgp:refresh-time
	// Check interval for a configured RPKI server.
	RefreshTime int64 `mapstructure:"refresh-time" json:"refresh-time"`
	// original -> gobgp:hold-time
	// Specify the length of time in seconds that the session between
	// the router and RPKI server is to be considered operational
	// without any activity.
	HoldTime int64 `mapstructure:"hold-time" json:"hold-time"`
	// original -> gobgp:record-lifetime
	// Indicate the expiration date of the route validation recode
	// received from RPKI server.
	RecordLifetime int64 `mapstructure:"record-lifetime" json:"record-lifetime"`
	// original -> gobgp:preference
	// RPKI server has a static preference.
	// Higher the preference values indicates a higher priority RPKI server.
	Preference uint8 `mapstructure:"preference" json:"preference"`
}

// struct for container gobgp:rpki-server.
// List of RPKI servers configured on the local system.
type RpkiServer struct {
	// original -> gobgp:address
	// original -> gobgp:rpki-server-config
	// Configuration parameters relating to RPKI server.
	Config RpkiServerConfig `mapstructure:"config" json:"config"`
	// original -> gobgp:rpki-server-state
	// State information relating to RPKI server.
	State RpkiServerState `mapstructure:"state" json:"state"`
}

// struct for container bgp:state.
// State information relating to the BGP neighbor or group.
type PeerGroupState struct {
	// original -> bgp:peer-as
	// bgp:peer-as's original type is inet:as-number.
	// AS number of the peer.
	PeerAs uint32 `mapstructure:"peer-as" json:"peer-as"`
	// original -> bgp:local-as
	// bgp:local-as's original type is inet:as-number.
	// The local autonomous system number that is to be used
	// when establishing sessions with the remote peer or peer
	// group, if this differs from the global BGP router
	// autonomous system number.
	LocalAs uint32 `mapstructure:"local-as" json:"local-as"`
	// original -> bgp:peer-type
	// Explicitly designate the peer or peer group as internal
	// (iBGP) or external (eBGP).
	PeerType PeerType `mapstructure:"peer-type" json:"peer-type"`
	// original -> bgp:auth-password
	// Configures an MD5 authentication password for use with
	// neighboring devices.
	AuthPassword string `mapstructure:"auth-password" json:"auth-password"`
	// original -> bgp:remove-private-as
	// Remove private AS numbers from updates sent to peers.
	RemovePrivateAs RemovePrivateAsOption `mapstructure:"remove-private-as" json:"remove-private-as"`
	// original -> bgp:route-flap-damping
	// bgp:route-flap-damping's original type is boolean.
	// Enable route flap damping.
	RouteFlapDamping bool `mapstructure:"route-flap-damping" json:"route-flap-damping"`
	// original -> bgp:send-community
	// Specify which types of community should be sent to the
	// neighbor or group. The default is to not send the
	// community attribute.
	SendCommunity CommunityType `mapstructure:"send-community" json:"send-community"`
	// original -> bgp:description
	// An optional textual description (intended primarily for use
	// with a peer or group.
	Description string `mapstructure:"description" json:"description"`
	// original -> bgp:peer-group-name
	// Name of the BGP peer-group.
	PeerGroupName string `mapstructure:"peer-group-name" json:"peer-group-name"`
	// original -> bgp-op:total-paths
	// Total number of BGP paths within the context.
	TotalPaths uint32 `mapstructure:"total-paths" json:"total-paths"`
	// original -> bgp-op:total-prefixes
	// .
	TotalPrefixes uint32 `mapstructure:"total-prefixes" json:"total-prefixes"`
}

// struct for container bgp:config.
// Configuration parameters relating to the BGP neighbor or
// group.
type PeerGroupConfig struct {
	// original -> bgp:peer-as
	// bgp:peer-as's original type is inet:as-number.
	// AS number of the peer.
	PeerAs uint32 `mapstructure:"peer-as" json:"peer-as"`
	// original -> bgp:local-as
	// bgp:local-as's original type is inet:as-number.
	// The local autonomous system number that is to be used
	// when establishing sessions with the remote peer or peer
	// group, if this differs from the global BGP router
	// autonomous system number.
	LocalAs uint32 `mapstructure:"local-as" json:"local-as"`
	// original -> bgp:peer-type
	// Explicitly designate the peer or peer group as internal
	// (iBGP) or external (eBGP).
	PeerType PeerType `mapstructure:"peer-type" json:"peer-type"`
	// original -> bgp:auth-password
	// Configures an MD5 authentication password for use with
	// neighboring devices.
	AuthPassword string `mapstructure:"auth-password" json:"auth-password"`
	// original -> bgp:remove-private-as
	// Remove private AS numbers from updates sent to peers.
	RemovePrivateAs RemovePrivateAsOption `mapstructure:"remove-private-as" json:"remove-private-as"`
	// original -> bgp:route-flap-damping
	// bgp:route-flap-damping's original type is boolean.
	// Enable route flap damping.
	RouteFlapDamping bool `mapstructure:"route-flap-damping" json:"route-flap-damping"`
	// original -> bgp:send-community
	// Specify which types of community should be sent to the
	// neighbor or group. The default is to not send the
	// community attribute.
	SendCommunity CommunityType `mapstructure:"send-community" json:"send-community"`
	// original -> bgp:description
	// An optional textual description (intended primarily for use
	// with a peer or group.
	Description string `mapstructure:"description" json:"description"`
	// original -> bgp:peer-group-name
	// Name of the BGP peer-group.
	PeerGroupName string `mapstructure:"peer-group-name" json:"peer-group-name"`
}

// struct for container bgp:peer-group.
// List of BGP peer-groups configured on the local system -
// uniquely identified by peer-group name.
type PeerGroup struct {
	// original -> bgp:peer-group-name
	// original -> bgp:peer-group-config
	// Configuration parameters relating to the BGP neighbor or
	// group.
	Config PeerGroupConfig `mapstructure:"config" json:"config"`
	// original -> bgp:peer-group-state
	// State information relating to the BGP neighbor or group.
	State PeerGroupState `mapstructure:"state" json:"state"`
	// original -> bgp:timers
	// Timers related to a BGP neighbor or group.
	Timers Timers `mapstructure:"timers" json:"timers"`
	// original -> bgp:transport
	// Transport session parameters for the BGP neighbor or group.
	Transport Transport `mapstructure:"transport" json:"transport"`
	// original -> bgp:error-handling
	// Error handling parameters used for the BGP neighbor or
	// group.
	ErrorHandling ErrorHandling `mapstructure:"error-handling" json:"error-handling"`
	// original -> bgp:logging-options
	// Logging options for events related to the BGP neighbor or
	// group.
	LoggingOptions LoggingOptions `mapstructure:"logging-options" json:"logging-options"`
	// original -> bgp:ebgp-multihop
	// eBGP multi-hop parameters for the BGP neighbor or group.
	EbgpMultihop EbgpMultihop `mapstructure:"ebgp-multihop" json:"ebgp-multihop"`
	// original -> bgp:route-reflector
	// Route reflector parameters for the BGP neighbor or group.
	RouteReflector RouteReflector `mapstructure:"route-reflector" json:"route-reflector"`
	// original -> bgp:as-path-options
	// AS_PATH manipulation parameters for the BGP neighbor or
	// group.
	AsPathOptions AsPathOptions `mapstructure:"as-path-options" json:"as-path-options"`
	// original -> bgp:add-paths
	// Parameters relating to the advertisement and receipt of
	// multiple paths for a single NLRI (add-paths).
	AddPaths AddPaths `mapstructure:"add-paths" json:"add-paths"`
	// original -> bgp:afi-safis
	// Per-address-family configuration parameters associated with
	// the neighbor or group.
	AfiSafis []AfiSafi `mapstructure:"afi-safis" json:"afi-safis"`
	// original -> bgp:graceful-restart
	// Parameters relating the graceful restart mechanism for BGP.
	GracefulRestart GracefulRestart `mapstructure:"graceful-restart" json:"graceful-restart"`
	// original -> rpol:apply-policy
	// Anchor point for routing policies in the model.
	// Import and export policies are with respect to the local
	// routing table, i.e., export (send) and import (receive),
	// depending on the context.
	ApplyPolicy ApplyPolicy `mapstructure:"apply-policy" json:"apply-policy"`
	// original -> bgp-mp:use-multiple-paths
	// Parameters related to the use of multiple paths for the
	// same NLRI.
	UseMultiplePaths UseMultiplePaths `mapstructure:"use-multiple-paths" json:"use-multiple-paths"`
	// original -> gobgp:route-server
	// Configure the local router as a route server.
	RouteServer RouteServer `mapstructure:"route-server" json:"route-server"`
	// original -> gobgp:ttl-security
	// Configure TTL Security feature.
	TtlSecurity TtlSecurity `mapstructure:"ttl-security" json:"ttl-security"`
}

// struct for container gobgp:state.
// State information for TTL Security.
type TtlSecurityState struct {
	// original -> gobgp:enabled
	// gobgp:enabled's original type is boolean.
	// Enable features for TTL Security.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> gobgp:ttl-min
	// Reference to the port of the BMP server.
	TtlMin uint8 `mapstructure:"ttl-min" json:"ttl-min"`
}

// struct for container gobgp:config.
// Configuration parameters for TTL Security.
type TtlSecurityConfig struct {
	// original -> gobgp:enabled
	// gobgp:enabled's original type is boolean.
	// Enable features for TTL Security.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> gobgp:ttl-min
	// Reference to the port of the BMP server.
	TtlMin uint8 `mapstructure:"ttl-min" json:"ttl-min"`
}

// struct for container gobgp:ttl-security.
// Configure TTL Security feature.
type TtlSecurity struct {
	// original -> gobgp:ttl-security-config
	// Configuration parameters for TTL Security.
	Config TtlSecurityConfig `mapstructure:"config" json:"config"`
	// original -> gobgp:ttl-security-state
	// State information for TTL Security.
	State TtlSecurityState `mapstructure:"state" json:"state"`
}

// struct for container gobgp:state.
// State information relating to route server
// client(s) used for the BGP neighbor.
type RouteServerState struct {
	// original -> gobgp:route-server-client
	// gobgp:route-server-client's original type is boolean.
	// Configure the neighbor as a route server client.
	RouteServerClient bool `mapstructure:"route-server-client" json:"route-server-client"`
}

// struct for container gobgp:config.
// Configuration parameters relating to route server
// client(s) used for the BGP neighbor.
type RouteServerConfig struct {
	// original -> gobgp:route-server-client
	// gobgp:route-server-client's original type is boolean.
	// Configure the neighbor as a route server client.
	RouteServerClient bool `mapstructure:"route-server-client" json:"route-server-client"`
}

// struct for container gobgp:route-server.
// Configure the local router as a route server.
type RouteServer struct {
	// original -> gobgp:route-server-config
	// Configuration parameters relating to route server
	// client(s) used for the BGP neighbor.
	Config RouteServerConfig `mapstructure:"config" json:"config"`
	// original -> gobgp:route-server-state
	// State information relating to route server
	// client(s) used for the BGP neighbor.
	State RouteServerState `mapstructure:"state" json:"state"`
}

// struct for container bgp-op:prefixes.
// Prefix counters for the BGP session.
type Prefixes struct {
	// original -> bgp-op:received
	// The number of prefixes received from the neighbor.
	Received uint32 `mapstructure:"received" json:"received"`
	// original -> bgp-op:sent
	// The number of prefixes advertised to the neighbor.
	Sent uint32 `mapstructure:"sent" json:"sent"`
	// original -> bgp-op:installed
	// The number of advertised prefixes installed in the
	// Loc-RIB.
	Installed uint32 `mapstructure:"installed" json:"installed"`
}

// struct for container bgp:state.
// State information associated with ADD_PATHS.
type AddPathsState struct {
	// original -> bgp:receive
	// bgp:receive's original type is boolean.
	// Enable ability to receive multiple path advertisements
	// for an NLRI from the neighbor or group.
	Receive bool `mapstructure:"receive" json:"receive"`
	// original -> bgp:send-max
	// The maximum number of paths to advertise to neighbors
	// for a single NLRI.
	SendMax uint8 `mapstructure:"send-max" json:"send-max"`
}

// struct for container bgp:config.
// Configuration parameters relating to ADD_PATHS.
type AddPathsConfig struct {
	// original -> bgp:receive
	// bgp:receive's original type is boolean.
	// Enable ability to receive multiple path advertisements
	// for an NLRI from the neighbor or group.
	Receive bool `mapstructure:"receive" json:"receive"`
	// original -> bgp:send-max
	// The maximum number of paths to advertise to neighbors
	// for a single NLRI.
	SendMax uint8 `mapstructure:"send-max" json:"send-max"`
}

// struct for container bgp:add-paths.
// Parameters relating to the advertisement and receipt of
// multiple paths for a single NLRI (add-paths).
type AddPaths struct {
	// original -> bgp:add-paths-config
	// Configuration parameters relating to ADD_PATHS.
	Config AddPathsConfig `mapstructure:"config" json:"config"`
	// original -> bgp:add-paths-state
	// State information associated with ADD_PATHS.
	State AddPathsState `mapstructure:"state" json:"state"`
}

// struct for container bgp:state.
// State information relating to the AS_PATH manipulation
// mechanisms for the BGP peer or group.
type AsPathOptionsState struct {
	// original -> bgp:allow-own-as
	// Specify the number of occurrences of the local BGP speaker's
	// AS that can occur within the AS_PATH before it is rejected.
	AllowOwnAs uint8 `mapstructure:"allow-own-as" json:"allow-own-as"`
	// original -> bgp:replace-peer-as
	// bgp:replace-peer-as's original type is boolean.
	// Replace occurrences of the peer's AS in the AS_PATH
	// with the local autonomous system number.
	ReplacePeerAs bool `mapstructure:"replace-peer-as" json:"replace-peer-as"`
}

// struct for container bgp:config.
// Configuration parameters relating to AS_PATH manipulation
// for the BGP peer or group.
type AsPathOptionsConfig struct {
	// original -> bgp:allow-own-as
	// Specify the number of occurrences of the local BGP speaker's
	// AS that can occur within the AS_PATH before it is rejected.
	AllowOwnAs uint8 `mapstructure:"allow-own-as" json:"allow-own-as"`
	// original -> bgp:replace-peer-as
	// bgp:replace-peer-as's original type is boolean.
	// Replace occurrences of the peer's AS in the AS_PATH
	// with the local autonomous system number.
	ReplacePeerAs bool `mapstructure:"replace-peer-as" json:"replace-peer-as"`
}

// struct for container bgp:as-path-options.
// AS_PATH manipulation parameters for the BGP neighbor or
// group.
type AsPathOptions struct {
	// original -> bgp:as-path-options-config
	// Configuration parameters relating to AS_PATH manipulation
	// for the BGP peer or group.
	Config AsPathOptionsConfig `mapstructure:"config" json:"config"`
	// original -> bgp:as-path-options-state
	// State information relating to the AS_PATH manipulation
	// mechanisms for the BGP peer or group.
	State AsPathOptionsState `mapstructure:"state" json:"state"`
}

// struct for container bgp:state.
// State information relating to route reflection for the
// BGP neighbor or group.
type RouteReflectorState struct {
	// original -> bgp:route-reflector-cluster-id
	// route-reflector cluster id to use when local router is
	// configured as a route reflector.  Commonly set at the group
	// level, but allows a different cluster
	// id to be set for each neighbor.
	RouteReflectorClusterId RrClusterIdType `mapstructure:"route-reflector-cluster-id" json:"route-reflector-cluster-id"`
	// original -> bgp:route-reflector-client
	// bgp:route-reflector-client's original type is boolean.
	// Configure the neighbor as a route reflector client.
	RouteReflectorClient bool `mapstructure:"route-reflector-client" json:"route-reflector-client"`
}

// struct for container bgp:config.
// Configuraton parameters relating to route reflection
// for the BGP neighbor or group.
type RouteReflectorConfig struct {
	// original -> bgp:route-reflector-cluster-id
	// route-reflector cluster id to use when local router is
	// configured as a route reflector.  Commonly set at the group
	// level, but allows a different cluster
	// id to be set for each neighbor.
	RouteReflectorClusterId RrClusterIdType `mapstructure:"route-reflector-cluster-id" json:"route-reflector-cluster-id"`
	// original -> bgp:route-reflector-client
	// bgp:route-reflector-client's original type is boolean.
	// Configure the neighbor as a route reflector client.
	RouteReflectorClient bool `mapstructure:"route-reflector-client" json:"route-reflector-client"`
}

// struct for container bgp:route-reflector.
// Route reflector parameters for the BGP neighbor or group.
type RouteReflector struct {
	// original -> bgp:route-reflector-config
	// Configuraton parameters relating to route reflection
	// for the BGP neighbor or group.
	Config RouteReflectorConfig `mapstructure:"config" json:"config"`
	// original -> bgp:route-reflector-state
	// State information relating to route reflection for the
	// BGP neighbor or group.
	State RouteReflectorState `mapstructure:"state" json:"state"`
}

// struct for container bgp:state.
// State information for eBGP multihop, for the BGP neighbor
// or group.
type EbgpMultihopState struct {
	// original -> bgp:enabled
	// bgp:enabled's original type is boolean.
	// When enabled the referenced group or neighbors are permitted
	// to be indirectly connected - including cases where the TTL
	// can be decremented between the BGP peers.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> bgp:multihop-ttl
	// Time-to-live value to use when packets are sent to the
	// referenced group or neighbors and ebgp-multihop is enabled.
	MultihopTtl uint8 `mapstructure:"multihop-ttl" json:"multihop-ttl"`
}

// struct for container bgp:config.
// Configuration parameters relating to eBGP multihop for the
// BGP neighbor or group.
type EbgpMultihopConfig struct {
	// original -> bgp:enabled
	// bgp:enabled's original type is boolean.
	// When enabled the referenced group or neighbors are permitted
	// to be indirectly connected - including cases where the TTL
	// can be decremented between the BGP peers.
	Enabled bool `mapstructure:"enabled" json:"enabled" yaml:"Enabled"`
	// original -> bgp:multihop-ttl
	// Time-to-live value to use when packets are sent to the
	// referenced group or neighbors and ebgp-multihop is enabled.
	MultihopTtl uint8 `mapstructure:"multihop-ttl" json:"multihop-ttl" yaml:"MultihopTtl"`
}

// struct for container bgp:ebgp-multihop.
// eBGP multi-hop parameters for the BGP neighbor or group.
type EbgpMultihop struct {
	// original -> bgp:ebgp-multihop-config
	// Configuration parameters relating to eBGP multihop for the
	// BGP neighbor or group.
	Config EbgpMultihopConfig `mapstructure:"config" json:"config" yaml:"EbgpMultihopConfig"`
	// original -> bgp:ebgp-multihop-state
	// State information for eBGP multihop, for the BGP neighbor
	// or group.
	State EbgpMultihopState `mapstructure:"state" json:"state" yaml:"EbgpMultihopState"`
}

// struct for container bgp:state.
// State information relating to logging for the BGP neighbor
// or group.
type LoggingOptionsState struct {
	// original -> bgp:log-neighbor-state-changes
	// bgp:log-neighbor-state-changes's original type is boolean.
	// Configure logging of peer state changes.  Default is
	// to enable logging of peer state changes.
	LogNeighborStateChanges bool `mapstructure:"log-neighbor-state-changes" json:"log-neighbor-state-changes"`
}

// struct for container bgp:config.
// Configuration parameters enabling or modifying logging
// for events relating to the BGP neighbor or group.
type LoggingOptionsConfig struct {
	// original -> bgp:log-neighbor-state-changes
	// bgp:log-neighbor-state-changes's original type is boolean.
	// Configure logging of peer state changes.  Default is
	// to enable logging of peer state changes.
	LogNeighborStateChanges bool `mapstructure:"log-neighbor-state-changes" json:"log-neighbor-state-changes"`
}

// struct for container bgp:logging-options.
// Logging options for events related to the BGP neighbor or
// group.
type LoggingOptions struct {
	// original -> bgp:logging-options-config
	// Configuration parameters enabling or modifying logging
	// for events relating to the BGP neighbor or group.
	Config LoggingOptionsConfig `mapstructure:"config" json:"config"`
	// original -> bgp:logging-options-state
	// State information relating to logging for the BGP neighbor
	// or group.
	State LoggingOptionsState `mapstructure:"state" json:"state"`
}

// struct for container bgp:state.
// State information relating to enhanced error handling
// mechanisms for the BGP neighbor or group.
type ErrorHandlingState struct {
	// original -> bgp:treat-as-withdraw
	// bgp:treat-as-withdraw's original type is boolean.
	// Specify whether erroneous UPDATE messages for which the
	// NLRI can be extracted are reated as though the NLRI is
	// withdrawn - avoiding session reset.
	TreatAsWithdraw bool `mapstructure:"treat-as-withdraw" json:"treat-as-withdraw"`
	// original -> bgp-op:erroneous-update-messages
	// The number of BGP UPDATE messages for which the
	// treat-as-withdraw mechanism has been applied based
	// on erroneous message contents.
	ErroneousUpdateMessages uint32 `mapstructure:"erroneous-update-messages" json:"erroneous-update-messages"`
}

// struct for container bgp:config.
// Configuration parameters enabling or modifying the
// behavior or enhanced error handling mechanisms for the BGP
// neighbor or group.
type ErrorHandlingConfig struct {
	// original -> bgp:treat-as-withdraw
	// bgp:treat-as-withdraw's original type is boolean.
	// Specify whether erroneous UPDATE messages for which the
	// NLRI can be extracted are reated as though the NLRI is
	// withdrawn - avoiding session reset.
	TreatAsWithdraw bool `mapstructure:"treat-as-withdraw" json:"treat-as-withdraw"`
}

// struct for container bgp:error-handling.
// Error handling parameters used for the BGP neighbor or
// group.
type ErrorHandling struct {
	// original -> bgp:error-handling-config
	// Configuration parameters enabling or modifying the
	// behavior or enhanced error handling mechanisms for the BGP
	// neighbor or group.
	Config ErrorHandlingConfig `mapstructure:"config" json:"config"`
	// original -> bgp:error-handling-state
	// State information relating to enhanced error handling
	// mechanisms for the BGP neighbor or group.
	State ErrorHandlingState `mapstructure:"state" json:"state"`
}

// struct for container bgp:state.
// State information relating to the transport session(s)
// used for the BGP neighbor or group.
type TransportState struct {
	// original -> bgp:tcp-mss
	// Sets the max segment size for BGP TCP sessions.
	TcpMss uint16 `mapstructure:"tcp-mss" json:"tcp-mss"`
	// original -> bgp:mtu-discovery
	// bgp:mtu-discovery's original type is boolean.
	// Turns path mtu discovery for BGP TCP sessions on (true)
	// or off (false).
	MtuDiscovery bool `mapstructure:"mtu-discovery" json:"mtu-discovery"`
	// original -> bgp:passive-mode
	// bgp:passive-mode's original type is boolean.
	// Wait for peers to issue requests to open a BGP session,
	// rather than initiating sessions from the local router.
	PassiveMode bool `mapstructure:"passive-mode" json:"passive-mode"`
	// original -> bgp:local-address
	// bgp:local-address's original type is union.
	// Set the local IP (either IPv4 or IPv6) address to use
	// for the session when sending BGP update messages.  This
	// may be expressed as either an IP address or reference
	// to the name of an interface.
	LocalAddress string `mapstructure:"local-address" json:"local-address"`
	// original -> bgp-op:local-port
	// bgp-op:local-port's original type is inet:port-number.
	// Local TCP port being used for the TCP session supporting
	// the BGP session.
	LocalPort uint16 `mapstructure:"local-port" json:"local-port"`
	// original -> bgp-op:remote-address
	// bgp-op:remote-address's original type is inet:ip-address.
	// Remote address to which the BGP session has been
	// established.
	RemoteAddress string `mapstructure:"remote-address" json:"remote-address"`
	// original -> bgp-op:remote-port
	// bgp-op:remote-port's original type is inet:port-number.
	// Remote port being used by the peer for the TCP session
	// supporting the BGP session.
	RemotePort uint16 `mapstructure:"remote-port" json:"remote-port"`
}

// struct for container bgp:config.
// Configuration parameters relating to the transport
// session(s) used for the BGP neighbor or group.
type TransportConfig struct {
	// original -> bgp:tcp-mss
	// Sets the max segment size for BGP TCP sessions.
	TcpMss uint16 `mapstructure:"tcp-mss" json:"tcp-mss"`
	// original -> bgp:mtu-discovery
	// bgp:mtu-discovery's original type is boolean.
	// Turns path mtu discovery for BGP TCP sessions on (true)
	// or off (false).
	MtuDiscovery bool `mapstructure:"mtu-discovery" json:"mtu-discovery"`
	// original -> bgp:passive-mode
	// bgp:passive-mode's original type is boolean.
	// Wait for peers to issue requests to open a BGP session,
	// rather than initiating sessions from the local router.
	PassiveMode bool `mapstructure:"passive-mode" json:"passive-mode"`
	// original -> bgp:local-address
	// bgp:local-address's original type is union.
	// Set the local IP (either IPv4 or IPv6) address to use
	// for the session when sending BGP update messages.  This
	// may be expressed as either an IP address or reference
	// to the name of an interface.
	LocalAddress string `mapstructure:"local-address" json:"local-address"`
	// original -> gobgp:remote-port
	// gobgp:remote-port's original type is inet:port-number.
	RemotePort uint16 `mapstructure:"remote-port" json:"remote-port"`
	// original -> gobgp:ttl
	// TTL value for BGP packets.
	Ttl uint8 `mapstructure:"ttl" json:"ttl"`
}

// struct for container bgp:transport.
// Transport session parameters for the BGP neighbor or group.
type Transport struct {
	// original -> bgp:transport-config
	// Configuration parameters relating to the transport
	// session(s) used for the BGP neighbor or group.
	Config TransportConfig `mapstructure:"config" json:"config"`
	// original -> bgp:transport-state
	// State information relating to the transport session(s)
	// used for the BGP neighbor or group.
	State TransportState `mapstructure:"state" json:"state"`
}

// struct for container bgp:state.
// State information relating to the timers used for the BGP
// neighbor or group.
type TimersState struct {
	// original -> bgp:connect-retry
	// bgp:connect-retry's original type is decimal64.
	// Time interval in seconds between attempts to establish a
	// session with the peer.
	ConnectRetry float64 `mapstructure:"connect-retry" json:"connect-retry"`
	// original -> bgp:hold-time
	// bgp:hold-time's original type is decimal64.
	// Time interval in seconds that a BGP session will be
	// considered active in the absence of keepalive or other
	// messages from the peer.  The hold-time is typically
	// set to 3x the keepalive-interval.
	HoldTime float64 `mapstructure:"hold-time" json:"hold-time"`
	// original -> bgp:keepalive-interval
	// bgp:keepalive-interval's original type is decimal64.
	// Time interval in seconds between transmission of keepalive
	// messages to the neighbor.  Typically set to 1/3 the
	// hold-time.
	KeepaliveInterval float64 `mapstructure:"keepalive-interval" json:"keepalive-interval"`
	// original -> bgp:minimum-advertisement-interval
	// bgp:minimum-advertisement-interval's original type is decimal64.
	// Minimum time which must elapse between subsequent UPDATE
	// messages relating to a common set of NLRI being transmitted
	// to a peer. This timer is referred to as
	// MinRouteAdvertisementIntervalTimer by RFC 4721 and serves to
	// reduce the number of UPDATE messages transmitted when a
	// particular set of NLRI exhibit instability.
	MinimumAdvertisementInterval float64 `mapstructure:"minimum-advertisement-interval" json:"minimum-advertisement-interval"`
	// original -> bgp-op:uptime
	// bgp-op:uptime's original type is yang:timeticks.
	// This timer determines the amount of time since the
	// BGP last transitioned in or out of the Established
	// state.
	// Uptime int64 `mapstructure:"uptime" json:"uptime"`
	// original -> bgp-op:negotiated-hold-time
	// bgp-op:negotiated-hold-time's original type is decimal64.
	// The negotiated hold-time for the BGP session.
	NegotiatedHoldTime float64 `mapstructure:"negotiated-hold-time" json:"negotiated-hold-time"`
	// original -> gobgp:idle-hold-time-after-reset
	// gobgp:idle-hold-time-after-reset's original type is decimal64.
	// Time interval in seconds that a BGP session will be
	// in idle state after neighbor reset operation.
	IdleHoldTimeAfterReset float64 `mapstructure:"idle-hold-time-after-reset" json:"idle-hold-time-after-reset"`
	// original -> gobgp:downtime
	// gobgp:downtime's original type is yang:timeticks.
	// This timer determines the amount of time since the
	// BGP last transitioned out of the Established state.
	// Downtime int64 `mapstructure:"downtime" json:"downtime"`
	// original -> gobgp:update-recv-time
	// The number of seconds elasped since January 1, 1970 UTC
	// last time the BGP session received an UPDATE message.
	UpdateRecvTime int64 `mapstructure:"update-recv-time" json:"update-recv-time"`
}

type TimeSeconds struct {
	Seconds int64 `json:"seconds"`
}

// struct for container bgp:config.
// Configuration parameters relating to timers used for the
// BGP neighbor or group.
type TimersConfig struct {
	// original -> bgp:connect-retry
	// bgp:connect-retry's original type is decimal64.
	// Time interval in seconds between attempts to establish a
	// session with the peer.
	ConnectRetry float64 `mapstructure:"connect-retry" json:"connect-retry"`
	// original -> bgp:hold-time
	// bgp:hold-time's original type is decimal64.
	// Time interval in seconds that a BGP session will be
	// considered active in the absence of keepalive or other
	// messages from the peer.  The hold-time is typically
	// set to 3x the keepalive-interval.
	HoldTime float64 `mapstructure:"hold-time" json:"hold-time"`
	// original -> bgp:keepalive-interval
	// bgp:keepalive-interval's original type is decimal64.
	// Time interval in seconds between transmission of keepalive
	// messages to the neighbor.  Typically set to 1/3 the
	// hold-time.
	KeepaliveInterval float64 `mapstructure:"keepalive-interval" json:"keepalive-interval"`
	// original -> bgp:minimum-advertisement-interval
	// bgp:minimum-advertisement-interval's original type is decimal64.
	// Minimum time which must elapse between subsequent UPDATE
	// messages relating to a common set of NLRI being transmitted
	// to a peer. This timer is referred to as
	// MinRouteAdvertisementIntervalTimer by RFC 4721 and serves to
	// reduce the number of UPDATE messages transmitted when a
	// particular set of NLRI exhibit instability.
	MinimumAdvertisementInterval float64 `mapstructure:"minimum-advertisement-interval" json:"minimum-advertisement-interval"`
	// original -> gobgp:idle-hold-time-after-reset
	// gobgp:idle-hold-time-after-reset's original type is decimal64.
	// Time interval in seconds that a BGP session will be
	// in idle state after neighbor reset operation.
	IdleHoldTimeAfterReset float64 `mapstructure:"idle-hold-time-after-reset" json:"idle-hold-time-after-reset"`
}

// struct for container bgp:timers.
// Timers related to a BGP neighbor or group.
type Timers struct {
	// original -> bgp:timers-config
	// Configuration parameters relating to timers used for the
	// BGP neighbor or group.
	Config TimersConfig `mapstructure:"config" json:"config"`
	// original -> bgp:timers-state
	// State information relating to the timers used for the BGP
	// neighbor or group.
	State TimersState `mapstructure:"state" json:"state"`
}

// struct for container gobgp:adj-table.
type AdjTable struct {
	// original -> gobgp:ADVERTISED
	Advertised uint32 `mapstructure:"advertised" json:"advertised"`
	// original -> gobgp:FILTERED
	Filtered uint32 `mapstructure:"filtered" json:"filtered"`
	// original -> gobgp:RECEIVED
	Received uint32 `mapstructure:"received" json:"received"`
	// original -> gobgp:ACCEPTED
	Accepted uint32 `mapstructure:"accepted" json:"accepted"`
}

// struct for container bgp:queues.
// Counters related to queued messages associated with the
// BGP neighbor.
type Queues struct {
	// original -> bgp-op:input
	// The number of messages received from the peer currently
	// queued.
	Input uint32 `mapstructure:"input" json:"input"`
	// original -> bgp-op:output
	// The number of messages queued to be sent to the peer.
	Output uint32 `mapstructure:"output" json:"output"`
}

// struct for container bgp:received.
// Counters for BGP messages received from the neighbor.
type Received struct {
	// original -> bgp-op:UPDATE
	// Number of BGP UPDATE messages announcing, withdrawing
	// or modifying paths exchanged.
	Update uint64 `mapstructure:"update" json:"update"`
	// original -> bgp-op:NOTIFICATION
	// Number of BGP NOTIFICATION messages indicating an
	// error condition has occurred exchanged.
	Notification uint64 `mapstructure:"notification" json:"notification"`
	// original -> gobgp:OPEN
	// Number of BGP open messages announcing, withdrawing
	// or modifying paths exchanged.
	Open uint64 `mapstructure:"open" json:"open"`
	// original -> gobgp:REFRESH
	// Number of BGP Route-Refresh messages indicating an
	// error condition has occurred exchanged.
	Refresh uint64 `mapstructure:"refresh" json:"refresh"`
	// original -> gobgp:KEEPALIVE
	// Number of BGP Keepalive messages indicating an
	// error condition has occurred exchanged.
	Keepalive uint64 `mapstructure:"keepalive" json:"keepalive"`
	// original -> gobgp:DYNAMIC-CAP
	// Number of BGP dynamic-cap messages indicating an
	// error condition has occurred exchanged.
	DynamicCap uint64 `mapstructure:"dynamic-cap" json:"dynamic-cap"`
	// original -> gobgp:WITHDRAW-UPDATE
	// Number of updates subjected to treat-as-withdraw treatment.
	WithdrawUpdate uint32 `mapstructure:"withdraw-update" json:"withdraw-update"`
	// original -> gobgp:WITHDRAW-PREFIX
	// Number of prefixes subjected to treat-as-withdraw treatment.
	WithdrawPrefix uint32 `mapstructure:"withdraw-prefix" json:"withdraw-prefix"`
	// original -> gobgp:DISCARDED
	// Number of discarded messages indicating an
	// error condition has occurred exchanged.
	Discarded uint64 `mapstructure:"discarded" json:"discarded"`
	// original -> gobgp:TOTAL
	// Number of total messages indicating an
	// error condition has occurred exchanged.
	Total uint64 `mapstructure:"total" json:"total"`
}

// struct for container bgp:sent.
// Counters relating to BGP messages sent to the neighbor.
type Sent struct {
	// original -> bgp-op:UPDATE
	// Number of BGP UPDATE messages announcing, withdrawing
	// or modifying paths exchanged.
	Update uint64 `mapstructure:"update" json:"update"`
	// original -> bgp-op:NOTIFICATION
	// Number of BGP NOTIFICATION messages indicating an
	// error condition has occurred exchanged.
	Notification uint64 `mapstructure:"notification" json:"notification"`
	// original -> gobgp:OPEN
	// Number of BGP open messages announcing, withdrawing
	// or modifying paths exchanged.
	Open uint64 `mapstructure:"open" json:"open"`
	// original -> gobgp:REFRESH
	// Number of BGP Route-Refresh messages indicating an
	// error condition has occurred exchanged.
	Refresh uint64 `mapstructure:"refresh" json:"refresh"`
	// original -> gobgp:KEEPALIVE
	// Number of BGP Keepalive messages indicating an
	// error condition has occurred exchanged.
	Keepalive uint64 `mapstructure:"keepalive" json:"keepalive"`
	// original -> gobgp:DYNAMIC-CAP
	// Number of BGP dynamic-cap messages indicating an
	// error condition has occurred exchanged.
	DynamicCap uint64 `mapstructure:"dynamic-cap" json:"dynamic-cap"`
	// original -> gobgp:WITHDRAW-UPDATE
	// Number of updates subjected to treat-as-withdraw treatment.
	WithdrawUpdate uint32 `mapstructure:"withdraw-update" json:"withdraw-update"`
	// original -> gobgp:WITHDRAW-PREFIX
	// Number of prefixes subjected to treat-as-withdraw treatment.
	WithdrawPrefix uint32 `mapstructure:"withdraw-prefix" json:"withdraw-prefix"`
	// original -> gobgp:DISCARDED
	// Number of discarded messages indicating an
	// error condition has occurred exchanged.
	Discarded uint64 `mapstructure:"discarded" json:"discarded"`
	// original -> gobgp:TOTAL
	// Number of total messages indicating an
	// error condition has occurred exchanged.
	Total uint64 `mapstructure:"total" json:"total"`
}

// struct for container bgp:messages.
// Counters for BGP messages sent and received from the
// neighbor.
type Messages struct {
	// original -> bgp:sent
	// Counters relating to BGP messages sent to the neighbor.
	Sent Sent `mapstructure:"sent" json:"sent"`
	// original -> bgp:received
	// Counters for BGP messages received from the neighbor.
	Received Received `mapstructure:"received" json:"received"`
}

// struct for container bgp:state.
// State information relating to the BGP neighbor or group.
type NeighborState struct {
	// original -> bgp:peer-as
	// bgp:peer-as's original type is inet:as-number.
	// AS number of the peer.
	PeerAs uint32 `mapstructure:"peer-as" json:"peer-as"`
	// original -> bgp:local-as
	// bgp:local-as's original type is inet:as-number.
	// The local autonomous system number that is to be used
	// when establishing sessions with the remote peer or peer
	// group, if this differs from the global BGP router
	// autonomous system number.
	LocalAs uint32 `mapstructure:"local-as" json:"local-as"`
	// original -> bgp:peer-type
	// Explicitly designate the peer or peer group as internal
	// (iBGP) or external (eBGP).
	PeerType PeerType `mapstructure:"peer-type" json:"peer-type"`
	// original -> bgp:auth-password
	// Configures an MD5 authentication password for use with
	// neighboring devices.
	AuthPassword string `mapstructure:"auth-password" json:"auth-password"`
	// original -> bgp:remove-private-as
	// Remove private AS numbers from updates sent to peers.
	RemovePrivateAs RemovePrivateAsOption `mapstructure:"remove-private-as" json:"remove-private-as"`
	// original -> bgp:route-flap-damping
	// bgp:route-flap-damping's original type is boolean.
	// Enable route flap damping.
	RouteFlapDamping bool `mapstructure:"route-flap-damping" json:"route-flap-damping"`
	// original -> bgp:send-community
	// Specify which types of community should be sent to the
	// neighbor or group. The default is to not send the
	// community attribute.
	SendCommunity CommunityType `mapstructure:"send-community" json:"send-community"`
	// original -> bgp:description
	// An optional textual description (intended primarily for use
	// with a peer or group.
	Description string `mapstructure:"description" json:"description"`
	// original -> bgp:peer-group
	// The peer-group with which this neighbor is associated.
	PeerGroup string `mapstructure:"peer-group" json:"peer-group"`
	// original -> bgp:neighbor-address
	// bgp:neighbor-address's original type is inet:ip-address.
	// Address of the BGP peer, either in IPv4 or IPv6.
	NeighborAddress string `mapstructure:"neighbor-address" json:"neighbor-address"`
	// original -> bgp-op:session-state
	// Operational state of the BGP peer.
	SessionState SessionState `mapstructure:"session-state" json:"session-state"`
	// original -> bgp-op:supported-capabilities
	// BGP capabilities negotiated as supported with the peer.
	SupportedCapabilitiesList []BgpCapability `mapstructure:"supported-capabilities-list" json:"supported-capabilities-list"`
	// original -> bgp:messages
	// Counters for BGP messages sent and received from the
	// neighbor.
	Messages Messages `mapstructure:"messages" json:"messages"`
	// original -> bgp:queues
	// Counters related to queued messages associated with the
	// BGP neighbor.
	Queues Queues `mapstructure:"queues" json:"queues"`
	// original -> gobgp:adj-table
	AdjTable AdjTable `mapstructure:"adj-table" json:"adj-table"`
	// original -> gobgp:remote-capability
	// original type is list of bgp-capability
	RemoteCapabilityList []bgp.ParameterCapabilityInterface `mapstructure:"remote-capability-list" json:"remote-capability-list"`
	// original -> gobgp:local-capability
	// original type is list of bgp-capability
	LocalCapabilityList []bgp.ParameterCapabilityInterface `mapstructure:"local-capability-list" json:"local-capability-list"`
	// original -> gobgp:received-open-message
	// gobgp:received-open-message's original type is bgp-open-message.
	ReceivedOpenMessage *bgp.BGPMessage `mapstructure:"received-open-message" json:"received-open-message"`
	// original -> gobgp:admin-down
	// gobgp:admin-down's original type is boolean.
	// The state of administrative operation. If the state is true, it indicates the neighbor is disabled by the administrator.
	AdminDown bool `mapstructure:"admin-down" json:"admin-down"`
	// original -> gobgp:admin-state
	AdminState AdminState `mapstructure:"admin-state" json:"admin-state"`
	// original -> gobgp:established-count
	// The number of how many the peer became established state.
	EstablishedCount uint32 `mapstructure:"established-count" json:"established-count"`
	// original -> gobgp:flops
	// The number of flip-flops.
	Flops uint32 `mapstructure:"flops" json:"flops"`
	// original -> gobgp:neighbor-interface
	NeighborInterface string `mapstructure:"neighbor-interface" json:"neighbor-interface"`
	// original -> gobgp:vrf
	Vrf string `mapstructure:"vrf" json:"vrf"`
	// original -> gobgp:remote-router-id
	RemoteRouterId string `mapstructure:"remote-router-id" json:"remote-router-id"`
}

// struct for container bgp:config.
// Configuration parameters relating to the BGP neighbor or
// group.
type NeighborConfig struct {
	// original -> bgp:peer-as
	// bgp:peer-as's original type is inet:as-number.
	// AS number of the peer.
	PeerAs uint32 `mapstructure:"peer-as" json:"peer-as" yaml:"PeerAs"`
	// original -> bgp:local-as
	// bgp:local-as's original type is inet:as-number.
	// The local autonomous system number that is to be used
	// when establishing sessions with the remote peer or peer
	// group, if this differs from the global BGP router
	// autonomous system number.
	LocalAs uint32 `mapstructure:"local-as" json:"local-as" yaml:"LocalAs"`
	// original -> bgp:peer-type
	// Explicitly designate the peer or peer group as internal
	// (iBGP) or external (eBGP).
	PeerType PeerType `mapstructure:"peer-type" json:"peer-type" yaml:"PeerType"`
	// original -> bgp:auth-password
	// Configures an MD5 authentication password for use with
	// neighboring devices.
	AuthPassword string `mapstructure:"auth-password" json:"auth-password" yaml:"AuthPassword"`
	// original -> bgp:remove-private-as
	// Remove private AS numbers from updates sent to peers.
	RemovePrivateAs RemovePrivateAsOption `mapstructure:"remove-private-as" json:"remove-private-as" yaml:"RemovePrivateAs"`
	// original -> bgp:route-flap-damping
	// bgp:route-flap-damping's original type is boolean.
	// Enable route flap damping.
	RouteFlapDamping bool `mapstructure:"route-flap-damping" json:"route-flap-damping" yaml:"RouteFlapDamping"`
	// original -> bgp:send-community
	// Specify which types of community should be sent to the
	// neighbor or group. The default is to not send the
	// community attribute.
	SendCommunity CommunityType `mapstructure:"send-community" json:"send-community" yaml:"SendCommunity"`
	// original -> bgp:description
	// An optional textual description (intended primarily for use
	// with a peer or group.
	Description string `mapstructure:"description" json:"description" yaml:"Descriptions"`
	// original -> bgp:peer-group
	// The peer-group with which this neighbor is associated.
	PeerGroup string `mapstructure:"peer-group" json:"peer-group" yaml:"PeerGroup"`
	// original -> bgp:neighbor-address
	// bgp:neighbor-address's original type is inet:ip-address.
	// Address of the BGP peer, either in IPv4 or IPv6.
	NeighborAddress string `mapstructure:"neighbor-address" json:"neighbor-address" yaml:"NeighborAddress"`
	// original -> gobgp:admin-down
	// gobgp:admin-down's original type is boolean.
	// The config of administrative operation. If state, indicates the neighbor is disabled by the administrator.
	AdminDown bool `mapstructure:"admin-down" json:"admin-down" yaml:"AdminDown"`
	// original -> gobgp:neighbor-interface
	NeighborInterface string `mapstructure:"neighbor-interface" json:"neighbor-interface" yaml:"NeighborInterface"`
	// original -> gobgp:vrf
	Vrf string `mapstructure:"vrf" json:"vrf" yaml:"Vrf"`
}

// struct for container bgp:neighbor.
// List of BGP neighbors configured on the local system,
// uniquely identified by peer IPv[46] address.
type Neighbor struct {
	// original -> bgp:neighbor-address
	// original -> bgp:neighbor-config
	// Configuration parameters relating to the BGP neighbor or
	// group.
	Config NeighborConfig `mapstructure:"config" json:"config" yaml:"Config"`
	// original -> bgp:neighbor-state
	// State information relating to the BGP neighbor or group.
	State NeighborState `mapstructure:"state" json:"state" yaml:"State"`
	// original -> bgp:timers
	// Timers related to a BGP neighbor or group.
	Timers Timers `mapstructure:"timers" json:"timers" yaml:"Timers"`
	// original -> bgp:transport
	// Transport session parameters for the BGP neighbor or group.
	Transport Transport `mapstructure:"transport" json:"transport" yaml:"Transport"`
	// original -> bgp:error-handling
	// Error handling parameters used for the BGP neighbor or
	// group.
	ErrorHandling ErrorHandling `mapstructure:"error-handling" json:"error-handling" yaml:"ErrorHandling"`
	// original -> bgp:logging-options
	// Logging options for events related to the BGP neighbor or
	// group.
	LoggingOptions LoggingOptions `mapstructure:"logging-options" json:"logging-options" yaml:"LoggingOptions"`
	// original -> bgp:ebgp-multihop
	// eBGP multi-hop parameters for the BGP neighbor or group.
	EbgpMultihop EbgpMultihop `mapstructure:"ebgp-multihop" json:"ebgp-multihop" yaml:"EbgpMultihop"`
	// original -> bgp:route-reflector
	// Route reflector parameters for the BGP neighbor or group.
	RouteReflector RouteReflector `mapstructure:"route-reflector" json:"route-reflector" yaml:"RouteReflector"`
	// original -> bgp:as-path-options
	// AS_PATH manipulation parameters for the BGP neighbor or
	// group.
	AsPathOptions AsPathOptions `mapstructure:"as-path-options" json:"as-path-options" yaml:"AsPathOptions"`
	// original -> bgp:add-paths
	// Parameters relating to the advertisement and receipt of
	// multiple paths for a single NLRI (add-paths).
	AddPaths AddPaths `mapstructure:"add-paths" json:"add-paths" yaml:"AddPaths"`
	// original -> bgp:afi-safis
	// Per-address-family configuration parameters associated with
	// the neighbor or group.
	AfiSafis []AfiSafi `mapstructure:"afi-safis" json:"afi-safis" yaml:"AfiSafis"`
	// original -> bgp:graceful-restart
	// Parameters relating the graceful restart mechanism for BGP.
	GracefulRestart GracefulRestart `mapstructure:"graceful-restart" json:"graceful-restart" yaml:"GracefulRestart"`
	// original -> rpol:apply-policy
	// Anchor point for routing policies in the model.
	// Import and export policies are with respect to the local
	// routing table, i.e., export (send) and import (receive),
	// depending on the context.
	ApplyPolicy ApplyPolicy `mapstructure:"apply-policy" json:"apply-policy" yaml:"ApplyPolicy"`
	// original -> bgp-mp:use-multiple-paths
	// Parameters related to the use of multiple-paths for the same
	// NLRI when they are received only from this neighbor.
	UseMultiplePaths UseMultiplePaths `mapstructure:"use-multiple-paths" json:"use-multiple-paths" yaml:"UseMultiplePaths"`
	// original -> gobgp:route-server
	// Configure the local router as a route server.
	RouteServer RouteServer `mapstructure:"route-server" json:"route-server" yaml:"RouteServer"`
	// original -> gobgp:ttl-security
	// Configure TTL Security feature.
	TtlSecurity TtlSecurity `mapstructure:"ttl-security" json:"ttl-security" yaml:"TtlSecurity"`
}

// struct for container gobgp:state.
type LongLivedGracefulRestartState struct {
	// original -> gobgp:enabled
	// gobgp:enabled's original type is boolean.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> gobgp:received
	// gobgp:received's original type is boolean.
	Received bool `mapstructure:"received" json:"received"`
	// original -> gobgp:advertised
	// gobgp:advertised's original type is boolean.
	Advertised bool `mapstructure:"advertised" json:"advertised"`
	// original -> gobgp:peer-restart-time
	PeerRestartTime uint32 `mapstructure:"peer-restart-time" json:"peer-restart-time"`
	// original -> gobgp:peer-restart-timer-expired
	// gobgp:peer-restart-timer-expired's original type is boolean.
	PeerRestartTimerExpired bool `mapstructure:"peer-restart-timer-expired" json:"peer-restart-timer-expired"`
}

// struct for container gobgp:config.
type LongLivedGracefulRestartConfig struct {
	// original -> gobgp:enabled
	// gobgp:enabled's original type is boolean.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> gobgp:restart-time
	RestartTime uint32 `mapstructure:"restart-time" json:"restart-time"`
}

// struct for container gobgp:long-lived-graceful-restart.
type LongLivedGracefulRestart struct {
	// original -> gobgp:long-lived-graceful-restart-config
	Config LongLivedGracefulRestartConfig `mapstructure:"config" json:"config"`
	// original -> gobgp:long-lived-graceful-restart-state
	State LongLivedGracefulRestartState `mapstructure:"state" json:"state"`
}

// struct for container gobgp:state.
type RouteTargetMembershipState struct {
	// original -> gobgp:deferral-time
	DeferralTime uint16 `mapstructure:"deferral-time" json:"deferral-time"`
}

// struct for container gobgp:config.
type RouteTargetMembershipConfig struct {
	// original -> gobgp:deferral-time
	DeferralTime uint16 `mapstructure:"deferral-time" json:"deferral-time"`
}

// struct for container gobgp:route-target-membership.
type RouteTargetMembership struct {
	// original -> gobgp:route-target-membership-config
	Config RouteTargetMembershipConfig `mapstructure:"config" json:"config"`
	// original -> gobgp:route-target-membership-state
	State RouteTargetMembershipState `mapstructure:"state" json:"state"`
}

// struct for container bgp-mp:l2vpn-evpn.
// BGP EVPN configuration options.
type L2vpnEvpn struct {
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
}

// struct for container bgp-mp:l2vpn-vpls.
// BGP-signalled VPLS configuration options.
type L2vpnVpls struct {
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
}

// struct for container bgp-mp:l3vpn-ipv6-multicast.
// Multicast IPv6 L3VPN configuration options.
type L3vpnIpv6Multicast struct {
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
}

// struct for container bgp-mp:l3vpn-ipv4-multicast.
// Multicast IPv4 L3VPN configuration options.
type L3vpnIpv4Multicast struct {
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
}

// struct for container bgp-mp:l3vpn-ipv6-unicast.
// Unicast IPv6 L3VPN configuration options.
type L3vpnIpv6Unicast struct {
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
}

// struct for container bgp-mp:l3vpn-ipv4-unicast.
// Unicast IPv4 L3VPN configuration options.
type L3vpnIpv4Unicast struct {
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
}

// struct for container bgp-mp:ipv6-labelled-unicast.
// IPv6 Labelled Unicast configuration options.
type Ipv6LabelledUnicast struct {
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
}

// struct for container bgp-mp:ipv4-labelled-unicast.
// IPv4 Labelled Unicast configuration options.
type Ipv4LabelledUnicast struct {
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
}

// struct for container bgp-mp:state.
// State information for common IPv4 and IPv6 unicast
// parameters.
type Ipv6UnicastState struct {
	// original -> bgp-mp:send-default-route
	// bgp-mp:send-default-route's original type is boolean.
	// If set to true, send the default-route to the neighbour(s).
	SendDefaultRoute bool `mapstructure:"send-default-route" json:"send-default-route"`
}

// struct for container bgp-mp:config.
// Configuration parameters for common IPv4 and IPv6 unicast
// AFI-SAFI options.
type Ipv6UnicastConfig struct {
	// original -> bgp-mp:send-default-route
	// bgp-mp:send-default-route's original type is boolean.
	// If set to true, send the default-route to the neighbour(s).
	SendDefaultRoute bool `mapstructure:"send-default-route" json:"send-default-route"`
}

// struct for container bgp-mp:ipv6-unicast.
// IPv6 unicast configuration options.
type Ipv6Unicast struct {
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
	// original -> bgp-mp:ipv6-unicast-config
	// Configuration parameters for common IPv4 and IPv6 unicast
	// AFI-SAFI options.
	Config Ipv6UnicastConfig `mapstructure:"config" json:"config"`
	// original -> bgp-mp:ipv6-unicast-state
	// State information for common IPv4 and IPv6 unicast
	// parameters.
	State Ipv6UnicastState `mapstructure:"state" json:"state"`
}

// struct for container bgp-mp:state.
// State information for common IPv4 and IPv6 unicast
// parameters.
type Ipv4UnicastState struct {
	// original -> bgp-mp:send-default-route
	// bgp-mp:send-default-route's original type is boolean.
	// If set to true, send the default-route to the neighbour(s).
	SendDefaultRoute bool `mapstructure:"send-default-route" json:"send-default-route"`
}

// struct for container bgp-mp:config.
// Configuration parameters for common IPv4 and IPv6 unicast
// AFI-SAFI options.
type Ipv4UnicastConfig struct {
	// original -> bgp-mp:send-default-route
	// bgp-mp:send-default-route's original type is boolean.
	// If set to true, send the default-route to the neighbour(s).
	SendDefaultRoute bool `mapstructure:"send-default-route" json:"send-default-route"`
}

// struct for container bgp-mp:state.
// State information relating to the prefix-limit for the
// AFI-SAFI.
type PrefixLimitState struct {
	// original -> bgp-mp:max-prefixes
	// Maximum number of prefixes that will be accepted
	// from the neighbour.
	MaxPrefixes uint32 `mapstructure:"max-prefixes" json:"max-prefixes"`
	// original -> bgp-mp:shutdown-threshold-pct
	// Threshold on number of prefixes that can be received
	// from a neighbour before generation of warning messages
	// or log entries. Expressed as a percentage of
	// max-prefixes.
	ShutdownThresholdPct Percentage `mapstructure:"shutdown-threshold-pct" json:"shutdown-threshold-pct"`
	// original -> bgp-mp:restart-timer
	// bgp-mp:restart-timer's original type is decimal64.
	// Time interval in seconds after which the BGP session
	// is re-established after being torn down due to exceeding
	// the max-prefix limit.
	RestartTimer float64 `mapstructure:"restart-timer" json:"restart-timer"`
}

// struct for container bgp-mp:config.
// Configuration parameters relating to the prefix
// limit for the AFI-SAFI.
type PrefixLimitConfig struct {
	// original -> bgp-mp:max-prefixes
	// Maximum number of prefixes that will be accepted
	// from the neighbour.
	MaxPrefixes uint32 `mapstructure:"max-prefixes" json:"max-prefixes"`
	// original -> bgp-mp:shutdown-threshold-pct
	// Threshold on number of prefixes that can be received
	// from a neighbour before generation of warning messages
	// or log entries. Expressed as a percentage of
	// max-prefixes.
	ShutdownThresholdPct Percentage `mapstructure:"shutdown-threshold-pct" json:"shutdown-threshold-pct"`
	// original -> bgp-mp:restart-timer
	// bgp-mp:restart-timer's original type is decimal64.
	// Time interval in seconds after which the BGP session
	// is re-established after being torn down due to exceeding
	// the max-prefix limit.
	RestartTimer float64 `mapstructure:"restart-timer" json:"restart-timer"`
}

// struct for container bgp-mp:prefix-limit.
// Configure the maximum number of prefixes that will be
// accepted from a peer.
type PrefixLimit struct {
	// original -> bgp-mp:prefix-limit-config
	// Configuration parameters relating to the prefix
	// limit for the AFI-SAFI.
	Config PrefixLimitConfig `mapstructure:"config" json:"config"`
	// original -> bgp-mp:prefix-limit-state
	// State information relating to the prefix-limit for the
	// AFI-SAFI.
	State PrefixLimitState `mapstructure:"state" json:"state"`
}

// struct for container bgp-mp:ipv4-unicast.
// IPv4 unicast configuration options.
type Ipv4Unicast struct {
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
	// original -> bgp-mp:ipv4-unicast-config
	// Configuration parameters for common IPv4 and IPv6 unicast
	// AFI-SAFI options.
	Config Ipv4UnicastConfig `mapstructure:"config" json:"config"`
	// original -> bgp-mp:ipv4-unicast-state
	// State information for common IPv4 and IPv6 unicast
	// parameters.
	State Ipv4UnicastState `mapstructure:"state" json:"state"`
}

// struct for container rpol:state.
// Operational state for routing policy.
type ApplyPolicyState struct {
	// original -> rpol:import-policy
	// list of policy names in sequence to be applied on
	// receiving a routing update in the current context, e.g.,
	// for the current peer group, neighbor, address family,
	// etc.
	ImportPolicyList []string `mapstructure:"import-policy-list" json:"import-policy-list"`
	// original -> rpol:default-import-policy
	// explicitly set a default policy if no policy definition
	// in the import policy chain is satisfied.
	DefaultImportPolicy DefaultPolicyType `mapstructure:"default-import-policy" json:"default-import-policy"`
	// original -> rpol:export-policy
	// list of policy names in sequence to be applied on
	// sending a routing update in the current context, e.g.,
	// for the current peer group, neighbor, address family,
	// etc.
	ExportPolicyList []string `mapstructure:"export-policy-list" json:"export-policy-list"`
	// original -> rpol:default-export-policy
	// explicitly set a default policy if no policy definition
	// in the export policy chain is satisfied.
	DefaultExportPolicy DefaultPolicyType `mapstructure:"default-export-policy" json:"default-export-policy"`
	// original -> gobgp:in-policy
	// list of policy names in sequence to be applied on
	// sending a routing update in the current context, e.g.,
	// for the current other route server clients.
	InPolicyList []string `mapstructure:"in-policy-list" json:"in-policy-list"`
	// original -> gobgp:default-in-policy
	// explicitly set a default policy if no policy definition
	// in the in-policy chain is satisfied.
	DefaultInPolicy DefaultPolicyType `mapstructure:"default-in-policy" json:"default-in-policy"`
}

// struct for container rpol:config.
// Policy configuration data.
type ApplyPolicyConfig struct {
	// original -> rpol:import-policy
	// list of policy names in sequence to be applied on
	// receiving a routing update in the current context, e.g.,
	// for the current peer group, neighbor, address family,
	// etc.
	ImportPolicyList []string `mapstructure:"import-policy-list" json:"import-policy-list"`
	// original -> rpol:default-import-policy
	// explicitly set a default policy if no policy definition
	// in the import policy chain is satisfied.
	DefaultImportPolicy DefaultPolicyType `mapstructure:"default-import-policy" json:"default-import-policy"`
	// original -> rpol:export-policy
	// list of policy names in sequence to be applied on
	// sending a routing update in the current context, e.g.,
	// for the current peer group, neighbor, address family,
	// etc.
	ExportPolicyList []string `mapstructure:"export-policy-list" json:"export-policy-list"`
	// original -> rpol:default-export-policy
	// explicitly set a default policy if no policy definition
	// in the export policy chain is satisfied.
	DefaultExportPolicy DefaultPolicyType `mapstructure:"default-export-policy" json:"default-export-policy"`
	// original -> gobgp:in-policy
	// list of policy names in sequence to be applied on
	// sending a routing update in the current context, e.g.,
	// for the current other route server clients.
	InPolicyList []string `mapstructure:"in-policy-list" json:"in-policy-list"`
	// original -> gobgp:default-in-policy
	// explicitly set a default policy if no policy definition
	// in the in-policy chain is satisfied.
	DefaultInPolicy DefaultPolicyType `mapstructure:"default-in-policy" json:"default-in-policy"`
}

// struct for container rpol:apply-policy.
// Anchor point for routing policies in the model.
// Import and export policies are with respect to the local
// routing table, i.e., export (send) and import (receive),
// depending on the context.
type ApplyPolicy struct {
	// original -> rpol:apply-policy-config
	// Policy configuration data.
	Config ApplyPolicyConfig `mapstructure:"config" json:"config"`
	// original -> rpol:apply-policy-state
	// Operational state for routing policy.
	State ApplyPolicyState `mapstructure:"state" json:"state"`
}

// struct for container bgp-mp:state.
// State information relating to the AFI-SAFI.
type AfiSafiState struct {
	// original -> bgp-mp:afi-safi-name
	// AFI,SAFI.
	AfiSafiName AfiSafiType `mapstructure:"afi-safi-name" json:"afi-safi-name"`
	// original -> bgp-mp:enabled
	// bgp-mp:enabled's original type is boolean.
	// This leaf indicates whether the IPv4 Unicast AFI,SAFI is
	// enabled for the neighbour or group.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> bgp-op:total-paths
	// Total number of BGP paths within the context.
	TotalPaths uint32 `mapstructure:"total-paths" json:"total-paths"`
	// original -> bgp-op:total-prefixes
	// .
	TotalPrefixes uint32 `mapstructure:"total-prefixes" json:"total-prefixes"`
	// original -> gobgp:family
	// gobgp:family's original type is route-family.
	// Address family value of AFI-SAFI pair translated from afi-safi-name.
	Family bgp.RouteFamily `mapstructure:"family" json:"family"`
}

// type RouteFamily int

// struct for container bgp-mp:config.
// Configuration parameters for the AFI-SAFI.
type AfiSafiConfig struct {
	// original -> bgp-mp:afi-safi-name
	// AFI,SAFI.
	AfiSafiName AfiSafiType `mapstructure:"afi-safi-name" json:"afi-safi-name"`
	// original -> bgp-mp:enabled
	// bgp-mp:enabled's original type is boolean.
	// This leaf indicates whether the IPv4 Unicast AFI,SAFI is
	// enabled for the neighbour or group.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
}

// struct for container bgp-mp:state.
// State information for BGP graceful-restart.
type MpGracefulRestartState struct {
	// original -> bgp-mp:enabled
	// bgp-mp:enabled's original type is boolean.
	// This leaf indicates whether graceful-restart is enabled for
	// this AFI-SAFI.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> bgp-op:received
	// bgp-op:received's original type is boolean.
	// This leaf indicates whether the neighbor advertised the
	// ability to support graceful-restart for this AFI-SAFI.
	Received bool `mapstructure:"received" json:"received"`
	// original -> bgp-op:advertised
	// bgp-op:advertised's original type is boolean.
	// This leaf indicates whether the ability to support
	// graceful-restart has been advertised to the peer.
	Advertised bool `mapstructure:"advertised" json:"advertised"`
	// original -> gobgp:end-of-rib-received
	// gobgp:end-of-rib-received's original type is boolean.
	EndOfRibReceived bool `mapstructure:"end-of-rib-received" json:"end-of-rib-received"`
	// original -> gobgp:end-of-rib-sent
	// gobgp:end-of-rib-sent's original type is boolean.
	EndOfRibSent bool `mapstructure:"end-of-rib-sent" json:"end-of-rib-sent"`
}

// struct for container bgp-mp:config.
// Configuration options for BGP graceful-restart.
type MpGracefulRestartConfig struct {
	// original -> bgp-mp:enabled
	// bgp-mp:enabled's original type is boolean.
	// This leaf indicates whether graceful-restart is enabled for
	// this AFI-SAFI.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
}

// struct for container bgp-mp:graceful-restart.
// Parameters relating to BGP graceful-restart.
type MpGracefulRestart struct {
	// original -> bgp-mp:mp-graceful-restart-config
	// Configuration options for BGP graceful-restart.
	Config MpGracefulRestartConfig `mapstructure:"config" json:"config"`
	// original -> bgp-mp:mp-graceful-restart-state
	// State information for BGP graceful-restart.
	State MpGracefulRestartState `mapstructure:"state" json:"state"`
}

type RouteFamily struct {
	Afi  int
	Safi int
}

// struct for container bgp-mp:afi-safi.
// AFI,SAFI configuration available for the
// neighbour or group.
type AfiSafi struct {
	// original -> bgp-mp:afi-safi-name
	// original -> bgp-mp:mp-graceful-restart
	// Parameters relating to BGP graceful-restart.
	MpGracefulRestart MpGracefulRestart `mapstructure:"mp-graceful-restart" json:"mp-graceful-restart"`
	// original -> bgp-mp:afi-safi-config
	// Configuration parameters for the AFI-SAFI.
	Config AfiSafiConfig `mapstructure:"config" json:"config"`
	// original -> bgp-mp:afi-safi-state
	// State information relating to the AFI-SAFI.
	State AfiSafiState `mapstructure:"state" json:"state"`
	// original -> rpol:apply-policy
	// Anchor point for routing policies in the model.
	// Import and export policies are with respect to the local
	// routing table, i.e., export (send) and import (receive),
	// depending on the context.
	ApplyPolicy ApplyPolicy `mapstructure:"apply-policy" json:"apply-policy"`
	// original -> bgp-mp:ipv4-unicast
	// IPv4 unicast configuration options.
	Ipv4Unicast Ipv4Unicast `mapstructure:"ipv4-unicast" json:"ipv4-unicast"`
	// original -> bgp-mp:ipv6-unicast
	// IPv6 unicast configuration options.
	Ipv6Unicast Ipv6Unicast `mapstructure:"ipv6-unicast" json:"ipv6-unicast"`
	// original -> bgp-mp:ipv4-labelled-unicast
	// IPv4 Labelled Unicast configuration options.
	Ipv4LabelledUnicast Ipv4LabelledUnicast `mapstructure:"ipv4-labelled-unicast" json:"ipv4-labelled-unicast"`
	// original -> bgp-mp:ipv6-labelled-unicast
	// IPv6 Labelled Unicast configuration options.
	Ipv6LabelledUnicast Ipv6LabelledUnicast `mapstructure:"ipv6-labelled-unicast" json:"ipv6-labelled-unicast"`
	// original -> bgp-mp:l3vpn-ipv4-unicast
	// Unicast IPv4 L3VPN configuration options.
	L3vpnIpv4Unicast L3vpnIpv4Unicast `mapstructure:"l3vpn-ipv4-unicast" json:"l3vpn-ipv4-unicast"`
	// original -> bgp-mp:l3vpn-ipv6-unicast
	// Unicast IPv6 L3VPN configuration options.
	L3vpnIpv6Unicast L3vpnIpv6Unicast `mapstructure:"l3vpn-ipv6-unicast" json:"l3vpn-ipv6-unicast"`
	// original -> bgp-mp:l3vpn-ipv4-multicast
	// Multicast IPv4 L3VPN configuration options.
	L3vpnIpv4Multicast L3vpnIpv4Multicast `mapstructure:"l3vpn-ipv4-multicast" json:"l3vpn-ipv4-multicast"`
	// original -> bgp-mp:l3vpn-ipv6-multicast
	// Multicast IPv6 L3VPN configuration options.
	L3vpnIpv6Multicast L3vpnIpv6Multicast `mapstructure:"l3vpn-ipv6-multicast" json:"l3vpn-ipv6-multicast"`
	// original -> bgp-mp:l2vpn-vpls
	// BGP-signalled VPLS configuration options.
	L2vpnVpls L2vpnVpls `mapstructure:"l2vpn-vpls" json:"l2vpn-vpls"`
	// original -> bgp-mp:l2vpn-evpn
	// BGP EVPN configuration options.
	L2vpnEvpn L2vpnEvpn `mapstructure:"l2vpn-evpn" json:"l2vpn-evpn"`
	// original -> bgp-mp:route-selection-options
	// Parameters relating to options for route selection.
	RouteSelectionOptions RouteSelectionOptions `mapstructure:"route-selection-options" json:"route-selection-options"`
	// original -> bgp-mp:use-multiple-paths
	// Parameters related to the use of multiple paths for the
	// same NLRI.
	UseMultiplePaths UseMultiplePaths `mapstructure:"use-multiple-paths" json:"use-multiple-paths"`
	// original -> bgp-mp:prefix-limit
	// Configure the maximum number of prefixes that will be
	// accepted from a peer.
	PrefixLimit PrefixLimit `mapstructure:"prefix-limit" json:"prefix-limit"`
	// original -> gobgp:route-target-membership
	RouteTargetMembership RouteTargetMembership `mapstructure:"route-target-membership" json:"route-target-membership"`
	// original -> gobgp:long-lived-graceful-restart
	LongLivedGracefulRestart LongLivedGracefulRestart `mapstructure:"long-lived-graceful-restart" json:"long-lived-graceful-restart"`
	// original -> gobgp:add-paths
	// add-paths configuration options related to a particular AFI-SAFI.
	AddPaths AddPaths `mapstructure:"add-paths" json:"add-paths"`
}

// struct for container bgp:state.
// State information associated with graceful-restart.
type GracefulRestartState struct {
	// original -> bgp:enabled
	// bgp:enabled's original type is boolean.
	// Enable or disable the graceful-restart capability.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> bgp:restart-time
	// Estimated time (in seconds) for the local BGP speaker to
	// restart a session. This value is advertise in the graceful
	// restart BGP capability.  This is a 12-bit value, referred to
	// as Restart Time in RFC4724.  Per RFC4724, the suggested
	// default value is <= the hold-time value.
	RestartTime uint16 `mapstructure:"restart-time" json:"restart-time"`
	// original -> bgp:stale-routes-time
	// bgp:stale-routes-time's original type is decimal64.
	// An upper-bound on the time thate stale routes will be
	// retained by a router after a session is restarted. If an
	// End-of-RIB (EOR) marker is received prior to this timer
	// expiring stale-routes will be flushed upon its receipt - if
	// no EOR is received, then when this timer expires stale paths
	// will be purged. This timer is referred to as the
	// Selection_Deferral_Timer in RFC4724.
	StaleRoutesTime float64 `mapstructure:"stale-routes-time" json:"stale-routes-time"`
	// original -> bgp:helper-only
	// bgp:helper-only's original type is boolean.
	// Enable graceful-restart in helper mode only. When this
	// leaf is set, the local system does not retain forwarding
	// its own state during a restart, but supports procedures
	// for the receiving speaker, as defined in RFC4724.
	HelperOnly bool `mapstructure:"helper-only" json:"helper-only"`
	// original -> bgp-op:peer-restart-time
	// The period of time (advertised by the peer) that
	// the peer expects a restart of a BGP session to
	// take.
	PeerRestartTime uint16 `mapstructure:"peer-restart-time" json:"peer-restart-time"`
	// original -> bgp-op:peer-restarting
	// bgp-op:peer-restarting's original type is boolean.
	// This flag indicates whether the remote neighbor is currently
	// in the process of restarting, and hence received routes are
	// currently stale.
	PeerRestarting bool `mapstructure:"peer-restarting" json:"peer-restarting"`
	// original -> bgp-op:local-restarting
	// bgp-op:local-restarting's original type is boolean.
	// This flag indicates whether the local neighbor is currently
	// restarting. The flag is unset after all NLRI have been
	// advertised to the peer, and the End-of-RIB (EOR) marker has
	// been unset.
	LocalRestarting bool `mapstructure:"local-restarting" json:"local-restarting"`
	// original -> bgp-op:mode
	// Ths leaf indicates the mode of operation of BGP graceful
	// restart with the peer.
	Mode Mode `mapstructure:"mode" json:"mode"`
	// original -> gobgp:deferral-time
	DeferralTime uint16 `mapstructure:"deferral-time" json:"deferral-time"`
	// original -> gobgp:notification-enabled
	// gobgp:notification-enabled's original type is boolean.
	NotificationEnabled bool `mapstructure:"notification-enabled" json:"notification-enabled"`
	// original -> gobgp:long-lived-enabled
	// gobgp:long-lived-enabled's original type is boolean.
	LongLivedEnabled bool `mapstructure:"long-lived-enabled" json:"long-lived-enabled"`
}

// struct for container bgp:config.
// Configuration parameters relating to graceful-restart.
type GracefulRestartConfig struct {
	// original -> bgp:enabled
	// bgp:enabled's original type is boolean.
	// Enable or disable the graceful-restart capability.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> bgp:restart-time
	// Estimated time (in seconds) for the local BGP speaker to
	// restart a session. This value is advertise in the graceful
	// restart BGP capability.  This is a 12-bit value, referred to
	// as Restart Time in RFC4724.  Per RFC4724, the suggested
	// default value is <= the hold-time value.
	RestartTime uint16 `mapstructure:"restart-time" json:"restart-time"`
	// original -> bgp:stale-routes-time
	// bgp:stale-routes-time's original type is decimal64.
	// An upper-bound on the time thate stale routes will be
	// retained by a router after a session is restarted. If an
	// End-of-RIB (EOR) marker is received prior to this timer
	// expiring stale-routes will be flushed upon its receipt - if
	// no EOR is received, then when this timer expires stale paths
	// will be purged. This timer is referred to as the
	// Selection_Deferral_Timer in RFC4724.
	StaleRoutesTime float64 `mapstructure:"stale-routes-time" json:"stale-routes-time"`
	// original -> bgp:helper-only
	// bgp:helper-only's original type is boolean.
	// Enable graceful-restart in helper mode only. When this
	// leaf is set, the local system does not retain forwarding
	// its own state during a restart, but supports procedures
	// for the receiving speaker, as defined in RFC4724.
	HelperOnly bool `mapstructure:"helper-only" json:"helper-only"`
	// original -> gobgp:deferral-time
	DeferralTime uint16 `mapstructure:"deferral-time" json:"deferral-time"`
	// original -> gobgp:notification-enabled
	// gobgp:notification-enabled's original type is boolean.
	NotificationEnabled bool `mapstructure:"notification-enabled" json:"notification-enabled"`
	// original -> gobgp:long-lived-enabled
	// gobgp:long-lived-enabled's original type is boolean.
	LongLivedEnabled bool `mapstructure:"long-lived-enabled" json:"long-lived-enabled"`
}

// struct for container bgp:graceful-restart.
// Parameters relating the graceful restart mechanism for BGP.
type GracefulRestart struct {
	// original -> bgp:graceful-restart-config
	// Configuration parameters relating to graceful-restart.
	Config GracefulRestartConfig `mapstructure:"config" json:"config"`
	// original -> bgp:graceful-restart-state
	// State information associated with graceful-restart.
	State GracefulRestartState `mapstructure:"state" json:"state"`
}

// struct for container bgp-mp:state.
// State information relating to iBGP multipath.
type IbgpState struct {
	// original -> bgp-mp:maximum-paths
	// Maximum number of parallel paths to consider when using
	// iBGP multipath. The default is to use a single path.
	MaximumPaths uint32 `mapstructure:"maximum-paths" json:"maximum-paths"`
}

// struct for container bgp-mp:config.
// Configuration parameters relating to iBGP multipath.
type IbgpConfig struct {
	// original -> bgp-mp:maximum-paths
	// Maximum number of parallel paths to consider when using
	// iBGP multipath. The default is to use a single path.
	MaximumPaths uint32 `mapstructure:"maximum-paths" json:"maximum-paths"`
}

// struct for container bgp-mp:ibgp.
// Multipath parameters for iBGP.
type Ibgp struct {
	// original -> bgp-mp:ibgp-config
	// Configuration parameters relating to iBGP multipath.
	Config IbgpConfig `mapstructure:"config" json:"config"`
	// original -> bgp-mp:ibgp-state
	// State information relating to iBGP multipath.
	State IbgpState `mapstructure:"state" json:"state"`
}

// struct for container bgp-mp:state.
// State information relating to eBGP multipath.
type EbgpState struct {
	// original -> bgp-mp:allow-multiple-as
	// bgp-mp:allow-multiple-as's original type is boolean.
	// Allow multipath to use paths from different neighbouring
	// ASes.  The default is to only consider multiple paths from
	// the same neighbouring AS.
	AllowMultipleAs bool `mapstructure:"allow-multiple-as" json:"allow-multiple-as"`
	// original -> bgp-mp:maximum-paths
	// Maximum number of parallel paths to consider when using
	// BGP multipath. The default is use a single path.
	MaximumPaths uint32 `mapstructure:"maximum-paths" json:"maximum-paths"`
}

// struct for container bgp-mp:config.
// Configuration parameters relating to eBGP multipath.
type EbgpConfig struct {
	// original -> bgp-mp:allow-multiple-as
	// bgp-mp:allow-multiple-as's original type is boolean.
	// Allow multipath to use paths from different neighbouring
	// ASes.  The default is to only consider multiple paths from
	// the same neighbouring AS.
	AllowMultipleAs bool `mapstructure:"allow-multiple-as" json:"allow-multiple-as"`
	// original -> bgp-mp:maximum-paths
	// Maximum number of parallel paths to consider when using
	// BGP multipath. The default is use a single path.
	MaximumPaths uint32 `mapstructure:"maximum-paths" json:"maximum-paths"`
}

// struct for container bgp-mp:ebgp.
// Multipath parameters for eBGP.
type Ebgp struct {
	// original -> bgp-mp:ebgp-config
	// Configuration parameters relating to eBGP multipath.
	Config EbgpConfig `mapstructure:"config" json:"config"`
	// original -> bgp-mp:ebgp-state
	// State information relating to eBGP multipath.
	State EbgpState `mapstructure:"state" json:"state"`
}

// struct for container bgp-mp:state.
// State parameters relating to multipath.
type UseMultiplePathsState struct {
	// original -> bgp-mp:enabled
	// bgp-mp:enabled's original type is boolean.
	// Whether the use of multiple paths for the same NLRI is
	// enabled for the neighbor. This value is overridden by
	// any more specific configuration value.
	Enabled bool `mapstructure:"enabled" json:"enabled" yaml:"Enabled"`
}

// struct for container bgp-mp:config.
// Configuration parameters relating to multipath.
type UseMultiplePathsConfig struct {
	// original -> bgp-mp:enabled
	// bgp-mp:enabled's original type is boolean.
	// Whether the use of multiple paths for the same NLRI is
	// enabled for the neighbor. This value is overridden by
	// any more specific configuration value.
	Enabled bool `mapstructure:"enabled" json:"enabled" yaml:"Enabled"`
}

// struct for container bgp-mp:use-multiple-paths.
// Parameters related to the use of multiple paths for the
// same NLRI.
type UseMultiplePaths struct {
	// original -> bgp-mp:use-multiple-paths-config
	// Configuration parameters relating to multipath.
	Config UseMultiplePathsConfig `mapstructure:"config" json:"config"`
	// original -> bgp-mp:use-multiple-paths-state
	// State parameters relating to multipath.
	State UseMultiplePathsState `mapstructure:"state" json:"state"`
	// original -> bgp-mp:ebgp
	// Multipath parameters for eBGP.
	Ebgp Ebgp `mapstructure:"ebgp" json:"ebgp"`
	// original -> bgp-mp:ibgp
	// Multipath parameters for iBGP.
	Ibgp Ibgp `mapstructure:"ibgp" json:"ibgp"`
}

// struct for container bgp:state.
// State information relating to the BGP confederations.
type ConfederationState struct {
	// original -> bgp:enabled
	// bgp:enabled's original type is boolean.
	// When this leaf is set to true it indicates that
	// the local-AS is part of a BGP confederation.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> bgp:identifier
	// bgp:identifier's original type is inet:as-number.
	// Confederation identifier for the autonomous system.
	Identifier uint32 `mapstructure:"identifier" json:"identifier"`
	// original -> bgp:member-as
	// original type is list of inet:as-number
	// Remote autonomous systems that are to be treated
	// as part of the local confederation.
	MemberAsList []uint32 `mapstructure:"member-as-list" json:"member-as-list"`
}

// struct for container bgp:config.
// Configuration parameters relating to BGP confederations.
type ConfederationConfig struct {
	// original -> bgp:enabled
	// bgp:enabled's original type is boolean.
	// When this leaf is set to true it indicates that
	// the local-AS is part of a BGP confederation.
	Enabled bool `mapstructure:"enabled" json:"enabled"`
	// original -> bgp:identifier
	// bgp:identifier's original type is inet:as-number.
	// Confederation identifier for the autonomous system.
	Identifier uint32 `mapstructure:"identifier" json:"identifier"`
	// original -> bgp:member-as
	// original type is list of inet:as-number
	// Remote autonomous systems that are to be treated
	// as part of the local confederation.
	MemberAsList []uint32 `mapstructure:"member-as-list" json:"member-as-list"`
}

// struct for container bgp:confederation.
// Parameters indicating whether the local system acts as part
// of a BGP confederation.
type Confederation struct {
	// original -> bgp:confederation-config
	// Configuration parameters relating to BGP confederations.
	Config ConfederationConfig `mapstructure:"config" json:"config"`
	// original -> bgp:confederation-state
	// State information relating to the BGP confederations.
	State ConfederationState `mapstructure:"state" json:"state"`
}

// struct for container bgp:state.
// State information relating to the default route distance.
type DefaultRouteDistanceState struct {
	// original -> bgp:external-route-distance
	// Administrative distance for routes learned from external
	// BGP (eBGP).
	ExternalRouteDistance uint8 `mapstructure:"external-route-distance" json:"external-route-distance"`
	// original -> bgp:internal-route-distance
	// Administrative distance for routes learned from internal
	// BGP (iBGP).
	InternalRouteDistance uint8 `mapstructure:"internal-route-distance" json:"internal-route-distance"`
}

// struct for container bgp:config.
// Configuration parameters relating to the default route
// distance.
type DefaultRouteDistanceConfig struct {
	// original -> bgp:external-route-distance
	// Administrative distance for routes learned from external
	// BGP (eBGP).
	ExternalRouteDistance uint8 `mapstructure:"external-route-distance" json:"external-route-distance"`
	// original -> bgp:internal-route-distance
	// Administrative distance for routes learned from internal
	// BGP (iBGP).
	InternalRouteDistance uint8 `mapstructure:"internal-route-distance" json:"internal-route-distance"`
}

// struct for container bgp:default-route-distance.
// Administrative distance (or preference) assigned to
// routes received from different sources
// (external, internal, and local).
type DefaultRouteDistance struct {
	// original -> bgp:default-route-distance-config
	// Configuration parameters relating to the default route
	// distance.
	Config DefaultRouteDistanceConfig `mapstructure:"config" json:"config"`
	// original -> bgp:default-route-distance-state
	// State information relating to the default route distance.
	State DefaultRouteDistanceState `mapstructure:"state" json:"state"`
}

// struct for container bgp-mp:state.
// State information for the route selection options.
type RouteSelectionOptionsState struct {
	// original -> bgp-mp:always-compare-med
	// bgp-mp:always-compare-med's original type is boolean.
	// Compare multi-exit discriminator (MED) value from
	// different ASes when selecting the best route.  The
	// default behavior is to only compare MEDs for paths
	// received from the same AS.
	AlwaysCompareMed bool `mapstructure:"always-compare-med" json:"always-compare-med"`
	// original -> bgp-mp:ignore-as-path-length
	// bgp-mp:ignore-as-path-length's original type is boolean.
	// Ignore the AS path length when selecting the best path.
	// The default is to use the AS path length and prefer paths
	// with shorter length.
	IgnoreAsPathLength bool `mapstructure:"ignore-as-path-length" json:"ignore-as-path-length"`
	// original -> bgp-mp:external-compare-router-id
	// bgp-mp:external-compare-router-id's original type is boolean.
	// When comparing similar routes received from external
	// BGP peers, use the router-id as a criterion to select
	// the active path.
	ExternalCompareRouterId bool `mapstructure:"external-compare-router-id" json:"external-compare-router-id"`
	// original -> bgp-mp:advertise-inactive-routes
	// bgp-mp:advertise-inactive-routes's original type is boolean.
	// Advertise inactive routes to external peers.  The
	// default is to only advertise active routes.
	AdvertiseInactiveRoutes bool `mapstructure:"advertise-inactive-routes" json:"advertise-inactive-routes"`
	// original -> bgp-mp:enable-aigp
	// bgp-mp:enable-aigp's original type is boolean.
	// Flag to enable sending / receiving accumulated IGP
	// attribute in routing updates.
	EnableAigp bool `mapstructure:"enable-aigp" json:"enable-aigp"`
	// original -> bgp-mp:ignore-next-hop-igp-metric
	// bgp-mp:ignore-next-hop-igp-metric's original type is boolean.
	// Ignore the IGP metric to the next-hop when calculating
	// BGP best-path. The default is to select the route for
	// which the metric to the next-hop is lowest.
	IgnoreNextHopIgpMetric bool `mapstructure:"ignore-next-hop-igp-metric" json:"ignore-next-hop-igp-metric"`
	// original -> gobgp:disable-best-path-selection
	// gobgp:disable-best-path-selection's original type is boolean.
	// Disables best path selection process.
	// DisableBestPathSelection bool `mapstructure:"disable-best-path-selection" json:"disable-best-path-selection"`
}

// struct for container bgp-mp:config.
// Configuration parameters relating to route selection
// options.
type RouteSelectionOptionsConfig struct {
	// original -> bgp-mp:always-compare-med
	// bgp-mp:always-compare-med's original type is boolean.
	// Compare multi-exit discriminator (MED) value from
	// different ASes when selecting the best route.  The
	// default behavior is to only compare MEDs for paths
	// received from the same AS.
	AlwaysCompareMed bool `mapstructure:"always-compare-med" json:"always-compare-med"`
	// original -> bgp-mp:ignore-as-path-length
	// bgp-mp:ignore-as-path-length's original type is boolean.
	// Ignore the AS path length when selecting the best path.
	// The default is to use the AS path length and prefer paths
	// with shorter length.
	IgnoreAsPathLength bool `mapstructure:"ignore-as-path-length" json:"ignore-as-path-length"`
	// original -> bgp-mp:external-compare-router-id
	// bgp-mp:external-compare-router-id's original type is boolean.
	// When comparing similar routes received from external
	// BGP peers, use the router-id as a criterion to select
	// the active path.
	ExternalCompareRouterId bool `mapstructure:"external-compare-router-id" json:"external-compare-router-id"`
	// original -> bgp-mp:advertise-inactive-routes
	// bgp-mp:advertise-inactive-routes's original type is boolean.
	// Advertise inactive routes to external peers.  The
	// default is to only advertise active routes.
	AdvertiseInactiveRoutes bool `mapstructure:"advertise-inactive-routes" json:"advertise-inactive-routes"`
	// original -> bgp-mp:enable-aigp
	// bgp-mp:enable-aigp's original type is boolean.
	// Flag to enable sending / receiving accumulated IGP
	// attribute in routing updates.
	EnableAigp bool `mapstructure:"enable-aigp" json:"enable-aigp"`
	// original -> bgp-mp:ignore-next-hop-igp-metric
	// bgp-mp:ignore-next-hop-igp-metric's original type is boolean.
	// Ignore the IGP metric to the next-hop when calculating
	// BGP best-path. The default is to select the route for
	// which the metric to the next-hop is lowest.
	IgnoreNextHopIgpMetric bool `mapstructure:"ignore-next-hop-igp-metric" json:"ignore-next-hop-igp-metric"`
	// original -> gobgp:disable-best-path-selection
	// gobgp:disable-best-path-selection's original type is boolean.
	// Disables best path selection process.
	// DisableBestPathSelection bool `mapstructure:"disable-best-path-selection" json:"disable-best-path-selection"`
}

// struct for container bgp-mp:route-selection-options.
// Parameters relating to options for route selection.
type RouteSelectionOptions struct {
	// original -> bgp-mp:route-selection-options-config
	// Configuration parameters relating to route selection
	// options.
	Config RouteSelectionOptionsConfig `mapstructure:"config" json:"config"`
	// original -> bgp-mp:route-selection-options-state
	// State information for the route selection options.
	State RouteSelectionOptionsState `mapstructure:"state" json:"state"`
}

// struct for container bgp:state.
// State information relating to the global BGP router.
type GlobalState struct {
	// original -> bgp:as
	// bgp:as's original type is inet:as-number.
	// Local autonomous system number of the router.  Uses
	// the 32-bit as-number type from the model in RFC 6991.
	As uint32 `mapstructure:"as" json:"as"`
	// original -> bgp:router-id
	// bgp:router-id's original type is inet:ipv4-address.
	// Router id of the router, expressed as an
	// 32-bit value, IPv4 address.
	RouterId string `mapstructure:"router-id" json:"router-id"`
	// original -> bgp-op:total-paths
	// Total number of BGP paths within the context.
	TotalPaths uint32 `mapstructure:"total-paths" json:"total-paths"`
	// original -> bgp-op:total-prefixes
	// .
	TotalPrefixes uint32 `mapstructure:"total-prefixes" json:"total-prefixes"`
	// original -> gobgp:port
	Port int32 `mapstructure:"port" json:"port"`
	// original -> gobgp:local-address
	LocalAddressList []string `mapstructure:"local-address-list" json:"local-address-list"`
}

// struct for container bgp:config.
// Configuration parameters relating to the global BGP router.
type GlobalConfig struct {
	// original -> bgp:as
	// bgp:as's original type is inet:as-number.
	// Local autonomous system number of the router.  Uses
	// the 32-bit as-number type from the model in RFC 6991.
	As uint32 `mapstructure:"as" json:"as" yaml:"As"`
	// original -> bgp:router-id
	// bgp:router-id's original type is inet:ipv4-address.
	// Router id of the router, expressed as an
	// 32-bit value, IPv4 address.
	RouterId string `mapstructure:"router-id" json:"router-id" yaml:"RouterId"`
	// original -> gobgp:port
	Port int32 `mapstructure:"port" json:"port" yaml:"Port"`
	// original -> gobgp:local-address
	LocalAddressList []string `mapstructure:"local-address-list" json:"local-address-list" yaml:"LocalAddressList"`
}

// struct for container bgp:global.
// Global configuration for the BGP router.
type Global struct {
	// original -> bgp:global-config
	// Configuration parameters relating to the global BGP router.
	Config GlobalConfig `mapstructure:"config" json:"config" yaml:"Config"`
	// original -> bgp:global-state
	// State information relating to the global BGP router.
	State GlobalState `mapstructure:"state" json:"state" yaml:"State"`
	// original -> bgp-mp:route-selection-options
	// Parameters relating to options for route selection.
	RouteSelectionOptions RouteSelectionOptions `mapstructure:"route-selection-options" json:"route-selection-options" yaml:"RouteSelectionOptions"`
	// original -> bgp:default-route-distance
	// Administrative distance (or preference) assigned to
	// routes received from different sources
	// (external, internal, and local).
	DefaultRouteDistance DefaultRouteDistance `mapstructure:"default-route-distance" json:"default-route-distance" yaml:"DefaultRouteDistance"`
	// original -> bgp:confederation
	// Parameters indicating whether the local system acts as part
	// of a BGP confederation.
	Confederation Confederation `mapstructure:"confederation" json:"confederation" yaml:"Confederation"`
	// original -> bgp-mp:use-multiple-paths
	// Parameters related to the use of multiple paths for the
	// same NLRI.
	UseMultiplePaths UseMultiplePaths `mapstructure:"use-multiple-paths" json:"use-multiple-paths" yaml:"UseMultiplePaths"`
	// original -> bgp:graceful-restart
	// Parameters relating the graceful restart mechanism for BGP.
	GracefulRestart GracefulRestart `mapstructure:"graceful-restart" json:"graceful-restart" yaml:"GracefulRestart"`
	// original -> bgp:afi-safis
	// Address family specific configuration.
	AfiSafis []AfiSafi `mapstructure:"afi-safis" json:"afi-safis" yaml:"AfiSafis"`
	// original -> rpol:apply-policy
	// Anchor point for routing policies in the model.
	// Import and export policies are with respect to the local
	// routing table, i.e., export (send) and import (receive),
	// depending on the context.
	ApplyPolicy ApplyPolicy `mapstructure:"apply-policy" json:"apply-policy" yaml:"ApplyPolicy"`
}

// struct for container bgp:bgp.
// Top-level configuration and state for the BGP router.
type Bgp struct {
	// original -> bgp:global
	// Global configuration for the BGP router.
	Global Global `mapstructure:"global" json:"global" yaml:"Global"`
	// original -> bgp:neighbors
	// Configuration for BGP neighbors.
	Neighbors []Neighbor `mapstructure:"neighbors" json:"neighbors" yaml:"Neighbors"`
	// original -> bgp:peer-groups
	// Configuration for BGP peer-groups.
	PeerGroups []PeerGroup `mapstructure:"peer-groups" json:"peer-groups" yaml:"PeerGroups"`
	// original -> gobgp:rpki-servers
	RpkiServers []RpkiServer `mapstructure:"rpki-servers" json:"rpki-servers" yaml:"RpkiServers"`
	// original -> gobgp:bmp-servers
	BmpServers []BmpServer `mapstructure:"bmp-servers" json:"bmp-servers" yaml:"BmpServers"`
	// original -> gobgp:vrfs
	Vrfs []Vrf `mapstructure:"vrfs" json:"vrfs" yaml:"Vrfs"`
	// original -> gobgp:mrt-dump
	MrtDump []Mrt `mapstructure:"mrt-dump" json:"mrt-dump" yaml:"MrtDump"`
	// original -> gobgp:zebra
	Zebra Zebra `mapstructure:"zebra" json:"zebra" yaml:"Zebra"`
	// original -> gobgp:collector
	Collector Collector `mapstructure:"collector" json:"collector" yaml:"Collector"`
	// original -> gobgp:dynamic-neighbors
	DynamicNeighbors []DynamicNeighbor `mapstructure:"dynamic-neighbors" json:"dynamic-neighbors" yaml:"DynamicNeighbors"`
}

// struct for container gobgp:set-large-community-method.
type SetLargeCommunityMethod struct {
	// original -> gobgp:communities
	CommunitiesList []string `mapstructure:"communities-list" json:"communities-list" yaml:"CommunitiesList"`
}

// struct for container gobgp:set-large-community.
type SetLargeCommunity struct {
	// original -> gobgp:set-large-community-method
	SetLargeCommunityMethod SetLargeCommunityMethod `mapstructure:"set-large-community-method" json:"set-large-community-method" yaml:"SetLargeCommunityMethod"`
	// original -> gobgp:options
	Options BgpSetCommunityOptionType `mapstructure:"options" json:"options" yaml:"BgpSetCommunityOptionType"`
}

// struct for container bgp-pol:set-ext-community-method.
// Option to set communities using an inline list or
// reference to an existing defined set.
type SetExtCommunityMethod struct {
	// original -> bgp-pol:communities
	// original type is list of union
	// Set the community values for the update inline with
	// a list.
	CommunitiesList []string `mapstructure:"communities-list" json:"communities-list" yaml:"CommunitiesList"`
	// original -> bgp-pol:ext-community-set-ref
	// References a defined extended community set by
	// name.
	ExtCommunitySetRef string `mapstructure:"ext-community-set-ref" json:"ext-community-set-ref" yaml:"ExtCommunitySetRef"`
}

// struct for container bgp-pol:set-ext-community.
// Action to set the extended community attributes of the
// route, along with options to modify how the community is
// modified.
type SetExtCommunity struct {
	// original -> bgp-pol:set-ext-community-method
	// Option to set communities using an inline list or
	// reference to an existing defined set.
	SetExtCommunityMethod SetExtCommunityMethod `mapstructure:"set-ext-community-method" json:"set-ext-community-method" yaml:"SetExtCommunityMethod"`
	// original -> bgp-pol:options
	// bgp-pol:options's original type is bgp-set-community-option-type.
	// options for modifying the extended community
	// attribute with the specified values. These options
	// apply to both methods of setting the community
	// attribute.
	Options string `mapstructure:"options" json:"options" yaml:"Options"`
}

// struct for container bgp-pol:set-community-method.
// Option to set communities using an inline list or
// reference to an existing defined set.
type SetCommunityMethod struct {
	// original -> bgp-pol:communities
	// original type is list of union
	// Set the community values for the update inline with
	// a list.
	CommunitiesList []string `mapstructure:"communities-list" json:"communities-list" yaml:"CommunitiesList"`
	// original -> bgp-pol:community-set-ref
	// References a defined community set by name.
	CommunitySetRef string `mapstructure:"community-set-ref" json:"community-set-ref" yaml:"CommunitySetRef"`
}

// struct for container bgp-pol:set-community.
// action to set the community attributes of the route, along
// with options to modify how the community is modified.
type SetCommunity struct {
	// original -> bgp-pol:set-community-method
	// Option to set communities using an inline list or
	// reference to an existing defined set.
	SetCommunityMethod SetCommunityMethod `mapstructure:"set-community-method" json:"set-community-method" yaml:"SetCommunityMethod"`
	// original -> bgp-pol:options
	// bgp-pol:options's original type is bgp-set-community-option-type.
	// Options for modifying the community attribute with
	// the specified values.  These options apply to both
	// methods of setting the community attribute.
	Options string `mapstructure:"options" json:"options" yaml:"Options"`
}

// struct for container bgp-pol:set-as-path-prepend.
// action to prepend local AS number to the AS-path a
// specified number of times.
type SetAsPathPrepend struct {
	// original -> bgp-pol:repeat-n
	// number of times to prepend the local AS
	// number.
	RepeatN uint8 `mapstructure:"repeat-n" json:"repeat-n" yaml:"RepeatN"`
	// original -> gobgp:as
	// gobgp:as's original type is union.
	// autonomous system number or 'last-as' which means
	// the leftmost as number in the AS-path to be prepended.
	As string `mapstructure:"as" json:"as" yaml:"As"`
}

// struct for container bgp-pol:bgp-actions.
// Definitions for policy action statements that
// change BGP-specific attributes of the route.
type BgpActions struct {
	// original -> bgp-pol:set-as-path-prepend
	// action to prepend local AS number to the AS-path a
	// specified number of times.
	SetAsPathPrepend SetAsPathPrepend `mapstructure:"set-as-path-prepend" json:"set-as-path-prepend" yaml:"SetAsPathPrepend"`
	// original -> bgp-pol:set-community
	// action to set the community attributes of the route, along
	// with options to modify how the community is modified.
	SetCommunity SetCommunity `mapstructure:"set-community" json:"set-community" yaml:"SetCommunity"`
	// original -> bgp-pol:set-ext-community
	// Action to set the extended community attributes of the
	// route, along with options to modify how the community is
	// modified.
	SetExtCommunity SetExtCommunity `mapstructure:"set-ext-community" json:"set-ext-community" yaml:"SetExtCommunity"`
	// original -> bgp-pol:set-route-origin
	// set the origin attribute to the specified
	// value.
	SetRouteOrigin BgpOriginAttrType `mapstructure:"set-route-origin" json:"set-route-origin" yaml:"SetRouteOrigin"`
	// original -> bgp-pol:set-local-pref
	// set the local pref attribute on the route
	// update.
	SetLocalPref uint32 `mapstructure:"set-local-pref" json:"set-local-pref" yaml:"SetLocalPref"`
	// original -> bgp-pol:set-next-hop
	// set the next-hop attribute in the route update.
	SetNextHop BgpNextHopType `mapstructure:"set-next-hop" json:"set-next-hop" yaml:"SetNextHop"`
	// original -> bgp-pol:set-med
	// set the med metric attribute in the route
	// update.
	SetMed BgpSetMedType `mapstructure:"set-med" json:"set-med" yaml:"SetMed"`
	// original -> gobgp:set-large-community
	SetLargeCommunity SetLargeCommunity `mapstructure:"set-large-community" json:"set-large-community" yaml:"SetLargeCommunity"`
}

// struct for container rpol:igp-actions.
// Actions to set IGP route attributes; these actions
// apply to multiple IGPs.
type IgpActions struct {
	// original -> rpol:set-tag
	// Set the tag value for OSPF or IS-IS routes.
	SetTag TagType `mapstructure:"set-tag" json:"set-tag" yaml:"SetTag"`
}

// struct for container rpol:actions.
// Action statements for this policy
// statement.
type Actions struct {
	// original -> rpol:route-disposition
	// Select the final disposition for the route, either
	// accept or reject.
	RouteDisposition RouteDisposition `mapstructure:"route-disposition" json:"route-disposition" yaml:"RouteDisposition"`
	// original -> rpol:igp-actions
	// Actions to set IGP route attributes; these actions
	// apply to multiple IGPs.
	IgpActions IgpActions `mapstructure:"igp-actions" json:"igp-actions" yaml:"IgpActions"`
	// original -> bgp-pol:bgp-actions
	// Definitions for policy action statements that
	// change BGP-specific attributes of the route.
	BgpActions BgpActions `mapstructure:"bgp-actions" json:"bgp-actions" yaml:"BgpActions"`
}

// struct for container gobgp:match-large-community-set.
type MatchLargeCommunitySet struct {
	// original -> gobgp:large-community-set
	LargeCommunitySet string `mapstructure:"large-community-set" json:"large-community-set" yaml:"LargeCommunitySet"`
	// original -> rpol:match-set-options
	// Optional parameter that governs the behaviour of the
	// match operation.
	MatchSetOptions MatchSetOptionsType `mapstructure:"match-set-options" json:"match-set-options" yaml:"MatchSetOptions"`
}

// struct for container bgp-pol:as-path-length.
// Value and comparison operations for conditions based on the
// length of the AS path in the route update.
type AsPathLength struct {
	// original -> ptypes:operator
	// type of comparison to be performed.
	Operator AttributeComparison `mapstructure:"operator" json:"operator" yaml:"Operator"`
	// original -> ptypes:value
	// value to compare with the community count.
	Value uint32 `mapstructure:"value" json:"value" yaml:"Value"`
}

// struct for container bgp-pol:community-count.
// Value and comparison operations for conditions based on the
// number of communities in the route update.
type CommunityCount struct {
	// original -> ptypes:operator
	// type of comparison to be performed.
	Operator AttributeComparison `mapstructure:"operator" json:"operator" yaml:"Operator"`
	// original -> ptypes:value
	// value to compare with the community count.
	Value uint32 `mapstructure:"value" json:"value" yaml:"Value"`
}

// struct for container bgp-pol:match-as-path-set.
// Match a referenced as-path set according to the logic
// defined in the match-set-options leaf.
type MatchAsPathSet struct {
	// original -> bgp-pol:as-path-set
	// References a defined AS path set.
	AsPathSet string `mapstructure:"as-path-set" json:"as-path-set" yaml:"AsPathSet"`
	// original -> rpol:match-set-options
	// Optional parameter that governs the behaviour of the
	// match operation.
	MatchSetOptions MatchSetOptionsType `mapstructure:"match-set-options" json:"match-set-options" yaml:"MatchSetOptions"`
}

// struct for container bgp-pol:match-ext-community-set.
// Match a referenced extended community-set according to the
// logic defined in the match-set-options leaf.
type MatchExtCommunitySet struct {
	// original -> bgp-pol:ext-community-set
	// References a defined extended community set.
	ExtCommunitySet string `mapstructure:"ext-community-set" json:"ext-community-set" yaml:"ExtCommunitySet"`
	// original -> rpol:match-set-options
	// Optional parameter that governs the behaviour of the
	// match operation.
	MatchSetOptions MatchSetOptionsType `mapstructure:"match-set-options" json:"match-set-options" yaml:"MatchSetOptions"`
}

// struct for container bgp-pol:match-community-set.
// Match a referenced community-set according to the logic
// defined in the match-set-options leaf.
type MatchCommunitySet struct {
	// original -> bgp-pol:community-set
	// References a defined community set.
	CommunitySet string `mapstructure:"community-set" json:"community-set" yaml:"CommunitySet"`
	// original -> rpol:match-set-options
	// Optional parameter that governs the behaviour of the
	// match operation.
	MatchSetOptions MatchSetOptionsType `mapstructure:"match-set-options" json:"match-set-options" yaml:"MatchSetOptions"`
}

// struct for container bgp-pol:bgp-conditions.
// Policy conditions for matching
// BGP-specific defined sets or comparing BGP-specific
// attributes.
type BgpConditions struct {
	// original -> bgp-pol:match-community-set
	// Match a referenced community-set according to the logic
	// defined in the match-set-options leaf.
	MatchCommunitySet MatchCommunitySet `mapstructure:"match-community-set" json:"match-community-set" yaml:"MatchCommunitySet"`
	// original -> bgp-pol:match-ext-community-set
	// Match a referenced extended community-set according to the
	// logic defined in the match-set-options leaf.
	MatchExtCommunitySet MatchExtCommunitySet `mapstructure:"match-ext-community-set" json:"match-ext-community-set" yaml:"MatchExtCommunitySet"`
	// original -> bgp-pol:match-as-path-set
	// Match a referenced as-path set according to the logic
	// defined in the match-set-options leaf.
	MatchAsPathSet MatchAsPathSet `mapstructure:"match-as-path-set" json:"match-as-path-set" yaml:"MatchAsPathSet"`
	// original -> bgp-pol:med-eq
	// Condition to check if the received MED value is equal to
	// the specified value.
	MedEq uint32 `mapstructure:"med-eq" json:"med-eq" yaml:"MedEq"`
	// original -> bgp-pol:origin-eq
	// Condition to check if the route origin is equal to the
	// specified value.
	OriginEq BgpOriginAttrType `mapstructure:"origin-eq" json:"origin-eq" yaml:"OriginEq"`
	// original -> bgp-pol:next-hop-in
	// original type is list of inet:ip-address
	// List of next hop addresses to check for in the route
	// update.
	NextHopInList []string `mapstructure:"next-hop-in-list" json:"next-hop-in-list" yaml:"NextHopInList"`
	// original -> bgp-pol:afi-safi-in
	// List of address families which the NLRI may be
	// within.
	AfiSafiInList []AfiSafiType `mapstructure:"afi-safi-in-list" json:"afi-safi-in-list" yaml:"AfiSafiInList"`
	// original -> bgp-pol:local-pref-eq
	// Condition to check if the local pref attribute is equal to
	// the specified value.
	LocalPrefEq uint32 `mapstructure:"local-pref-eq" json:"local-pref-eq" yaml:"LocalPrefEq"`
	// original -> bgp-pol:community-count
	// Value and comparison operations for conditions based on the
	// number of communities in the route update.
	CommunityCount CommunityCount `mapstructure:"community-count" json:"community-count" yaml:"CommunityCount"`
	// original -> bgp-pol:as-path-length
	// Value and comparison operations for conditions based on the
	// length of the AS path in the route update.
	AsPathLength AsPathLength `mapstructure:"as-path-length" json:"as-path-length" yaml:"AsPathLength"`
	// original -> bgp-pol:route-type
	// Condition to check the route type in the route update.
	RouteType RouteType `mapstructure:"route-type" json:"route-type" yaml:"RouteType"`
	// original -> gobgp:rpki-validation-result
	// specify the validation result of RPKI based on ROA as conditions.
	RpkiValidationResult RpkiValidationResultType `mapstructure:"rpki-validation-result" json:"rpki-validation-result" yaml:"RpkiValidationResult"`
	// original -> gobgp:match-large-community-set
	MatchLargeCommunitySet MatchLargeCommunitySet `mapstructure:"match-large-community-set" json:"match-large-community-set" yaml:"MatchLargeCommunitySet"`
}

// struct for container rpol:igp-conditions.
// Policy conditions for IGP attributes.
type IgpConditions struct {
}

func (lhs *IgpConditions) Equal(rhs *IgpConditions) bool {
	if lhs == nil || rhs == nil {
		return false
	}
	return true
}

// struct for container rpol:match-tag-set.
// Match a referenced tag set according to the logic defined
// in the match-options-set leaf.
type MatchTagSet struct {
	// original -> rpol:tag-set
	// References a defined tag set.
	TagSet string `mapstructure:"tag-set" json:"tag-set" yaml:"TagSet"`
	// original -> rpol:match-set-options
	// Optional parameter that governs the behaviour of the
	// match operation.  This leaf only supports matching on ANY
	// member of the set or inverting the match.  Matching on ALL is
	// not supported).
	MatchSetOptions MatchSetOptionsRestrictedType `mapstructure:"match-set-options" json:"match-set-options" yaml:"MatchSetOptions"`
}

// struct for container rpol:match-neighbor-set.
// Match a referenced neighbor set according to the logic
// defined in the match-set-options-leaf.
type MatchNeighborSet struct {
	// original -> rpol:neighbor-set
	// References a defined neighbor set.
	NeighborSet string `mapstructure:"neighbor-set" json:"neighbor-set" yaml:"NeighborSet"`
	// original -> rpol:match-set-options
	// Optional parameter that governs the behaviour of the
	// match operation.  This leaf only supports matching on ANY
	// member of the set or inverting the match.  Matching on ALL is
	// not supported).
	MatchSetOptions MatchSetOptionsRestrictedType `mapstructure:"match-set-options" json:"match-set-options" yaml:"MatchSetOptions"`
}

// struct for container rpol:match-prefix-set.
// Match a referenced prefix-set according to the logic
// defined in the match-set-options leaf.
type MatchPrefixSet struct {
	// original -> rpol:prefix-set
	// References a defined prefix set.
	PrefixSet string `mapstructure:"prefix-set" json:"prefix-set" yaml:"PrefixSet"`
	// original -> rpol:match-set-options
	// Optional parameter that governs the behaviour of the
	// match operation.  This leaf only supports matching on ANY
	// member of the set or inverting the match.  Matching on ALL is
	// not supported).
	MatchSetOptions MatchSetOptionsRestrictedType `mapstructure:"match-set-options" json:"match-set-options" yaml:"MatchSetOptions"`
}

// struct for container rpol:conditions.
// Condition statements for this
// policy statement.
type Conditions struct {
	// original -> rpol:call-policy
	// Applies the statements from the specified policy
	// definition and then returns control the current
	// policy statement. Note that the called policy may
	// itself call other policies (subject to
	// implementation limitations). This is intended to
	// provide a policy 'subroutine' capability.  The
	// called policy should contain an explicit or a
	// default route disposition that returns an
	// effective true (accept-route) or false
	// (reject-route), otherwise the behavior may be
	// ambiguous and implementation dependent.
	CallPolicy string `mapstructure:"call-policy" json:"call-policy" yaml:"CallPolicy"`
	// original -> rpol:match-prefix-set
	// Match a referenced prefix-set according to the logic
	// defined in the match-set-options leaf.
	MatchPrefixSet MatchPrefixSet `mapstructure:"match-prefix-set" json:"match-prefix-set" yaml:"MatchPrefixSet"`
	// original -> rpol:match-neighbor-set
	// Match a referenced neighbor set according to the logic
	// defined in the match-set-options-leaf.
	MatchNeighborSet MatchNeighborSet `mapstructure:"match-neighbor-set" json:"match-neighbor-set" yaml:"MatchNeighborSet"`
	// original -> rpol:match-tag-set
	// Match a referenced tag set according to the logic defined
	// in the match-options-set leaf.
	MatchTagSet MatchTagSet `mapstructure:"match-tag-set" json:"match-tag-set" yaml:"MatchTagSet"`
	// original -> rpol:install-protocol-eq
	// Condition to check the protocol / method used to install
	// which installed the route into the local routing table.
	InstallProtocolEq InstallProtocolType `mapstructure:"install-protocol-eq" json:"install-protocol-eq" yaml:"InstallProtocolEq"`
	// original -> rpol:igp-conditions
	// Policy conditions for IGP attributes.
	IgpConditions IgpConditions `mapstructure:"igp-conditions" json:"igp-conditions" yaml:"IgpConditions"`
	// original -> bgp-pol:bgp-conditions
	// Policy conditions for matching
	// BGP-specific defined sets or comparing BGP-specific
	// attributes.
	BgpConditions BgpConditions `mapstructure:"bgp-conditions" json:"bgp-conditions" yaml:"BgpConditions"`
}

// struct for container rpol:statement.
// Policy statements group conditions and actions
// within a policy definition.  They are evaluated in
// the order specified (see the description of policy
// evaluation at the top of this module.
type Statement struct {
	// original -> rpol:name
	// name of the policy statement.
	Name string `mapstructure:"name" json:"name" yaml:"Name"`
	// original -> rpol:conditions
	// Condition statements for this
	// policy statement.
	Conditions Conditions `mapstructure:"conditions" json:"conditions" yaml:"Conditions"`
	// original -> rpol:actions
	// Action statements for this policy
	// statement.
	Actions Actions `mapstructure:"actions" json:"actions" yaml:"Actions"`
}

// struct for container rpol:policy-definition.
// List of top-level policy definitions, keyed by unique
// name.  These policy definitions are expected to be
// referenced (by name) in policy chains specified in import
// or export configuration statements.
type PolicyDefinition struct {
	// original -> rpol:name
	// Name of the top-level policy definition -- this name
	//  is used in references to the current policy.
	Name string `mapstructure:"name" json:"name" yaml:"Name"`
	// original -> rpol:statements
	// Enclosing container for policy statements.
	Statements []Statement `mapstructure:"statements" json:"statements" yaml:"Statements"`
}

// struct for container gobgp:large-community-set.
type LargeCommunitySet struct {
	// original -> gobgp:large-community-set-name
	LargeCommunitySetName string `mapstructure:"large-community-set-name" json:"large-community-set-name" yaml:"LargeCommunitySetName"`
	// original -> gobgp:large-community
	// extended community set member.
	LargeCommunityList []string `mapstructure:"large-community-list" json:"large-community-list" yaml:"LargeCommunityList"`
}

// struct for container bgp-pol:as-path-set.
// Definitions for AS path sets.
type AsPathSet struct {
	// original -> bgp-pol:as-path-set-name
	// name of the AS path set -- this is used to reference
	// the set in match conditions.
	AsPathSetName string `mapstructure:"as-path-set-name" json:"as-path-set-name" yaml:"AsPathSetName"`
	// original -> gobgp:as-path
	// AS path expression.
	AsPathList []string `mapstructure:"as-path-list" json:"as-path-list" yaml:"AsPathList"`
}

// struct for container bgp-pol:ext-community-set.
// Definitions for extended community sets.
type ExtCommunitySet struct {
	// original -> bgp-pol:ext-community-set-name
	// name / label of the extended community set -- this is
	// used to reference the set in match conditions.
	ExtCommunitySetName string `mapstructure:"ext-community-set-name" json:"ext-community-set-name" yaml:"ExtCommunitySetName"`
	// original -> gobgp:ext-community
	// extended community set member.
	ExtCommunityList []string `mapstructure:"ext-community-list" json:"ext-community-list" yaml:"ExtCommunityList"`
}

// struct for container bgp-pol:community-set.
// Definitions for community sets.
type CommunitySet struct {
	// original -> bgp-pol:community-set-name
	// name / label of the community set -- this is used to
	// reference the set in match conditions.
	CommunitySetName string `mapstructure:"community-set-name" json:"community-set-name" yaml:"CommunitySetName"`
	// original -> gobgp:community
	// community set member.
	CommunityList []string `mapstructure:"community-list" json:"community-list" yaml:"CommunityList"`
}

// struct for container bgp-pol:bgp-defined-sets.
// BGP-related set definitions for policy match conditions.
type BgpDefinedSets struct {
	// original -> bgp-pol:community-sets
	// Enclosing container for community sets.
	CommunitySets []CommunitySet `mapstructure:"community-sets" json:"community-sets" yaml:"CommunitySets"`
	// original -> bgp-pol:ext-community-sets
	// Enclosing container for extended community sets.
	ExtCommunitySets []ExtCommunitySet `mapstructure:"ext-community-sets" json:"ext-community-sets" yaml:"ExtCommunitySets"`
	// original -> bgp-pol:as-path-sets
	// Enclosing container for AS path sets.
	AsPathSets []AsPathSet `mapstructure:"as-path-sets" json:"as-path-sets" yaml:"AsPathSets"`
	// original -> gobgp:large-community-sets
	LargeCommunitySets []LargeCommunitySet `mapstructure:"large-community-sets" json:"large-community-sets" yaml:"LargeCommunitySets"`
}

// struct for container rpol:tag.
// list of tags that are part of the tag set.
type Tag struct {
	// original -> rpol:value
	// Value of the tag set member.
	Value TagType `mapstructure:"value" json:"value" yaml:"Value"`
}

// struct for container rpol:tag-set.
// Definitions for tag sets.
type TagSet struct {
	// original -> rpol:tag-set-name
	// name / label of the tag set -- this is used to reference
	// the set in match conditions.
	TagSetName string `mapstructure:"tag-set-name" json:"tag-set-name" yaml:"TagSetName"`
	// original -> rpol:tag
	// list of tags that are part of the tag set.
	TagList []Tag `mapstructure:"tag-list" json:"tag-list" yaml:"TagList"`
}

// struct for container rpol:neighbor-set.
// Definitions for neighbor sets.
type NeighborSet struct {
	// original -> rpol:neighbor-set-name
	// name / label of the neighbor set -- this is used to
	// reference the set in match conditions.
	NeighborSetName string `mapstructure:"neighbor-set-name" json:"neighbor-set-name" yaml:"NeighborSetName"`
	// original -> gobgp:neighbor-info
	// original type is list of inet:ip-address
	// neighbor ip address or prefix.
	NeighborInfoList []string `mapstructure:"neighbor-info-list" json:"neighbor-info-list" yaml:"NeighborInfoList"`
}

// struct for container rpol:prefix.
// List of prefix expressions that are part of the set.
type Prefix struct {
	// original -> rpol:ip-prefix
	// rpol:ip-prefix's original type is inet:ip-prefix.
	// The prefix member in CIDR notation -- while the
	// prefix may be either IPv4 or IPv6, most
	// implementations require all members of the prefix set
	// to be the same address family.  Mixing address types in
	// the same prefix set is likely to cause an error.
	IpPrefix string `mapstructure:"ip-prefix" json:"ip-prefix" yaml:"IpPrefix"`
	// original -> rpol:masklength-range
	// Defines a range for the masklength, or 'exact' if
	// the prefix has an exact length.
	//
	// Example: 10.3.192.0/21 through 10.3.192.0/24 would be
	// expressed as prefix: 10.3.192.0/21,
	// masklength-range: 21..24.
	//
	// Example: 10.3.192.0/21 would be expressed as
	// prefix: 10.3.192.0/21,
	// masklength-range: exact.
	MasklengthRange string `mapstructure:"masklength-range" json:"masklength-range" yaml:"MasklengthRange"`
}

// struct for container rpol:prefix-set.
// List of the defined prefix sets.
type PrefixSet struct {
	// original -> rpol:prefix-set-name
	// name / label of the prefix set -- this is used to
	// reference the set in match conditions.
	PrefixSetName string `mapstructure:"prefix-set-name" json:"prefix-set-name" yaml:"PrefixSetName"`
	// original -> rpol:prefix
	// List of prefix expressions that are part of the set.
	PrefixList []Prefix `mapstructure:"prefix-list" json:"prefix-list" yaml:"PrefixList"`
}

// struct for container rpol:defined-sets.
// Predefined sets of attributes used in policy match
// statements.
type DefinedSets struct {
	// original -> rpol:prefix-sets
	// Enclosing container for defined prefix sets for matching.
	PrefixSets []PrefixSet `mapstructure:"prefix-sets" json:"prefix-sets" yaml:"PrefixSets"`
	// original -> rpol:neighbor-sets
	// Enclosing container for defined neighbor sets for matching.
	NeighborSets []NeighborSet `mapstructure:"neighbor-sets" json:"neighbor-sets" yaml:"NeighborSets"`
	// original -> rpol:tag-sets
	// Enclosing container for defined tag sets for matching.
	TagSets []TagSet `mapstructure:"tag-sets" json:"tag-sets" yaml:"TagSets"`
	// original -> bgp-pol:bgp-defined-sets
	// BGP-related set definitions for policy match conditions.
	BgpDefinedSets BgpDefinedSets `mapstructure:"bgp-defined-sets" json:"bgp-defined-sets" yaml:"BgpDefinedSets"`
}

// struct for container rpol:routing-policy.
// top-level container for all routing policy configuration.
type RoutingPolicy struct {
	// original -> rpol:defined-sets
	// Predefined sets of attributes used in policy match
	// statements.
	DefinedSets DefinedSets `mapstructure:"defined-sets" json:"defined-sets" yaml:"DefinedSets"`
	// original -> rpol:policy-definitions
	// Enclosing container for the list of top-level policy
	// definitions.
	PolicyDefinitions []PolicyDefinition `mapstructure:"policy-definitions" json:"policy-definitions" yaml:"PolicyDefinitions"`
}

const (
	AFI_SAFI_TYPE_IPV4_UNICAST          AfiSafiType = "ipv4-unicast"
	AFI_SAFI_TYPE_IPV6_UNICAST          AfiSafiType = "ipv6-unicast"
	AFI_SAFI_TYPE_IPV4_LABELLED_UNICAST AfiSafiType = "ipv4-labelled-unicast"
	AFI_SAFI_TYPE_IPV6_LABELLED_UNICAST AfiSafiType = "ipv6-labelled-unicast"
	AFI_SAFI_TYPE_L3VPN_IPV4_UNICAST    AfiSafiType = "l3vpn-ipv4-unicast"
	AFI_SAFI_TYPE_L3VPN_IPV6_UNICAST    AfiSafiType = "l3vpn-ipv6-unicast"
	AFI_SAFI_TYPE_L3VPN_IPV4_MULTICAST  AfiSafiType = "l3vpn-ipv4-multicast"
	AFI_SAFI_TYPE_L3VPN_IPV6_MULTICAST  AfiSafiType = "l3vpn-ipv6-multicast"
	AFI_SAFI_TYPE_L2VPN_VPLS            AfiSafiType = "l2vpn-vpls"
	AFI_SAFI_TYPE_L2VPN_EVPN            AfiSafiType = "l2vpn-evpn"
	AFI_SAFI_TYPE_IPV4_MULTICAST        AfiSafiType = "ipv4-multicast"
	AFI_SAFI_TYPE_IPV6_MULTICAST        AfiSafiType = "ipv6-multicast"
	AFI_SAFI_TYPE_RTC                   AfiSafiType = "rtc"
	AFI_SAFI_TYPE_IPV4_ENCAP            AfiSafiType = "ipv4-encap"
	AFI_SAFI_TYPE_IPV6_ENCAP            AfiSafiType = "ipv6-encap"
	AFI_SAFI_TYPE_IPV4_FLOWSPEC         AfiSafiType = "ipv4-flowspec"
	AFI_SAFI_TYPE_L3VPN_IPV4_FLOWSPEC   AfiSafiType = "l3vpn-ipv4-flowspec"
	AFI_SAFI_TYPE_IPV6_FLOWSPEC         AfiSafiType = "ipv6-flowspec"
	AFI_SAFI_TYPE_L3VPN_IPV6_FLOWSPEC   AfiSafiType = "l3vpn-ipv6-flowspec"
	AFI_SAFI_TYPE_L2VPN_FLOWSPEC        AfiSafiType = "l2vpn-flowspec"
	AFI_SAFI_TYPE_OPAQUE                AfiSafiType = "opaque"
)

var AfiSafiTypeToIntMap = map[AfiSafiType]int{
	AFI_SAFI_TYPE_IPV4_UNICAST:          0,
	AFI_SAFI_TYPE_IPV6_UNICAST:          1,
	AFI_SAFI_TYPE_IPV4_LABELLED_UNICAST: 2,
	AFI_SAFI_TYPE_IPV6_LABELLED_UNICAST: 3,
	AFI_SAFI_TYPE_L3VPN_IPV4_UNICAST:    4,
	AFI_SAFI_TYPE_L3VPN_IPV6_UNICAST:    5,
	AFI_SAFI_TYPE_L3VPN_IPV4_MULTICAST:  6,
	AFI_SAFI_TYPE_L3VPN_IPV6_MULTICAST:  7,
	AFI_SAFI_TYPE_L2VPN_VPLS:            8,
	AFI_SAFI_TYPE_L2VPN_EVPN:            9,
	AFI_SAFI_TYPE_IPV4_MULTICAST:        10,
	AFI_SAFI_TYPE_IPV6_MULTICAST:        11,
	AFI_SAFI_TYPE_RTC:                   12,
	AFI_SAFI_TYPE_IPV4_ENCAP:            13,
	AFI_SAFI_TYPE_IPV6_ENCAP:            14,
	AFI_SAFI_TYPE_IPV4_FLOWSPEC:         15,
	AFI_SAFI_TYPE_L3VPN_IPV4_FLOWSPEC:   16,
	AFI_SAFI_TYPE_IPV6_FLOWSPEC:         17,
	AFI_SAFI_TYPE_L3VPN_IPV6_FLOWSPEC:   18,
	AFI_SAFI_TYPE_L2VPN_FLOWSPEC:        19,
	AFI_SAFI_TYPE_OPAQUE:                20,
}

var IntToAfiSafiTypeMap = map[int]AfiSafiType{
	0:  AFI_SAFI_TYPE_IPV4_UNICAST,
	1:  AFI_SAFI_TYPE_IPV6_UNICAST,
	2:  AFI_SAFI_TYPE_IPV4_LABELLED_UNICAST,
	3:  AFI_SAFI_TYPE_IPV6_LABELLED_UNICAST,
	4:  AFI_SAFI_TYPE_L3VPN_IPV4_UNICAST,
	5:  AFI_SAFI_TYPE_L3VPN_IPV6_UNICAST,
	6:  AFI_SAFI_TYPE_L3VPN_IPV4_MULTICAST,
	7:  AFI_SAFI_TYPE_L3VPN_IPV6_MULTICAST,
	8:  AFI_SAFI_TYPE_L2VPN_VPLS,
	9:  AFI_SAFI_TYPE_L2VPN_EVPN,
	10: AFI_SAFI_TYPE_IPV4_MULTICAST,
	11: AFI_SAFI_TYPE_IPV6_MULTICAST,
	12: AFI_SAFI_TYPE_RTC,
	13: AFI_SAFI_TYPE_IPV4_ENCAP,
	14: AFI_SAFI_TYPE_IPV6_ENCAP,
	15: AFI_SAFI_TYPE_IPV4_FLOWSPEC,
	16: AFI_SAFI_TYPE_L3VPN_IPV4_FLOWSPEC,
	17: AFI_SAFI_TYPE_IPV6_FLOWSPEC,
	18: AFI_SAFI_TYPE_L3VPN_IPV6_FLOWSPEC,
	19: AFI_SAFI_TYPE_L2VPN_FLOWSPEC,
	20: AFI_SAFI_TYPE_OPAQUE,
}

type LbConfig struct {
	Logging  LoggingConfig     `toml:"logging" json:"logging"`
	Api      ApiConfig         `toml:"api" json:"api"`
	Defaults ConnectionOptions `toml:"defaults" json:"defaults"`
	Acme     *AcmeConfig       `toml:"acme" json:"acme"`
	Servers  map[string]Server `toml:"servers" json:"servers"`
}

/**
 * Logging config section
 */
type LoggingConfig struct {
	Level  string `toml:"level" json:"level"`
	Output string `toml:"output" json:"output"`
}

/**
 * Api config section
 */
type ApiConfig struct {
	Enabled   bool                `toml:"enabled" json:"enabled"`
	Bind      string              `toml:"bind" json:"bind"`
	BasicAuth *ApiBasicAuthConfig `toml:"basic_auth" json:"basic_auth"`
	Tls       *ApiTlsConfig       `toml:"tls" json:"tls"`
	Cors      bool                `toml:"cors" json:"cors"`
}

/**
 * Api Basic Auth Config
 */
type ApiBasicAuthConfig struct {
	Login    string `toml:"login" json:"login"`
	Password string `toml:"password" json:"password"`
}

/**
 * Api TLS server Config
 */
type ApiTlsConfig struct {
	CertPath string `toml:"cert_path" json:"cert_path"`
	KeyPath  string `toml:"key_path" json:"key_path"`
}

/**
 * Default values can be overridden in server
 */
type ConnectionOptions struct {
	MaxConnections           *int    `toml:"max_connections" json:"max_connections"`
	ClientIdleTimeout        *string `toml:"client_idle_timeout" json:"client_idle_timeout"`
	BackendIdleTimeout       *string `toml:"backend_idle_timeout" json:"backend_idle_timeout"`
	BackendConnectionTimeout *string `toml:"backend_connection_timeout" json:"backend_connection_timeout"`
}

/**
 * Acme config
 */
type AcmeConfig struct {
	Challenge string `toml:"challenge" json:"challenge"`
	HttpBind  string `toml:"http_bind" json:"http_bind"`
	CacheDir  string `toml:"cache_dir" json:"cache_dir"`
}

/**
 * Server section config
 */
type Server struct {
	ConnectionOptions

	// hostname:port
	Bind string `toml:"bind" json:"bind"`

	// tcp | udp | tls
	Protocol string `toml:"protocol" json:"protocol"`

	// weight | leastconn | roundrobin
	Balance string `toml:"balance" json:"balance"`

	// Optional configuration for server name indication
	Sni *Sni `toml:"sni" json:"sni"`

	// Optional configuration for protocol = tls
	Tls *Tls `toml:"tls" json:"tls"`

	// Optional configuration for backend_tls_enabled = true
	BackendsTls *BackendsTls `toml:"backends_tls" json:"backends_tls"`

	// Optional configuration for protocol = udp
	Udp *Udp `toml:"udp" json:"udp"`

	// Access configuration
	Access *AccessConfig `toml:"access" json:"access"`

	// ProxyProtocol configuration
	ProxyProtocol *ProxyProtocol `toml:"proxy_protocol" json:"proxy_protocol"`

	// Discovery configuration
	Discovery *DiscoveryConfig `toml:"discovery" json:"discovery"`

	// Healthcheck configuration
	Healthcheck *HealthcheckConfig `toml:"healthcheck" json:"healthcheck"`
}

/**
 * ProxyProtocol configurtion
 */
type ProxyProtocol struct {
	Version string `toml:"version" json:"version"`
}

/**
 * Server Sni options
 */
type Sni struct {
	HostnameMatchingStrategy   string `toml:"hostname_matching_strategy" json:"hostname_matching_strategy"`
	UnexpectedHostnameStrategy string `toml:"unexpected_hostname_strategy" json:"unexpected_hostname_strategy"`
	ReadTimeout                string `toml:"read_timeout" json:"read_timeout"`
}

/**
 * Common part of Tls and BackendTls types
 */
type tlsCommon struct {
	Ciphers             []string `toml:"ciphers" json:"ciphers"`
	PreferServerCiphers bool     `toml:"prefer_server_ciphers" json:"prefer_server_ciphers"`
	MinVersion          string   `toml:"min_version" json:"min_version"`
	MaxVersion          string   `toml:"max_version" json:"max_version"`
	SessionTickets      bool     `toml:"session_tickets" json:"session_tickets"`
}

/**
 * Server Tls options
 * for protocol = "tls"
 */
type Tls struct {
	AcmeHosts []string `toml:"acme_hosts" json:"acme_hosts"`
	CertPath  string   `toml:"cert_path" json:"cert_path"`
	KeyPath   string   `toml:"key_path" json:"key_path"`
	tlsCommon
}

type BackendsTls struct {
	IgnoreVerify   bool    `toml:"ignore_verify" json:"ignore_verify"`
	RootCaCertPath *string `toml:"root_ca_cert_path" json:"root_ca_cert_path"`
	CertPath       *string `toml:"cert_path" json:"cert_path"`
	KeyPath        *string `toml:"key_path" json:"key_path"`
	tlsCommon
}

/**
 * Server udp options
 * for protocol = "udp"
 */
type Udp struct {
	MaxRequests  uint64 `toml:"max_requests" json:"max_requests"`
	MaxResponses uint64 `toml:"max_responses" json:"max_responses"`
}

/**
 * Access configuration
 */
type AccessConfig struct {
	Default string   `toml:"default" json:"default"`
	Rules   []string `toml:"rules" json:"rules"`
}

/**
 * Discovery configuration
 */
type DiscoveryConfig struct {
	Kind       string `toml:"kind" json:"kind"`
	Failpolicy string `toml:"failpolicy" json:"failpolicy"`
	Interval   string `toml:"interval" json:"interval"`
	Timeout    string `toml:"timeout" json:"timeout"`

	/* Depends on Kind */

	*StaticDiscoveryConfig
	*SrvDiscoveryConfig
	*DockerDiscoveryConfig
	*JsonDiscoveryConfig
	*ExecDiscoveryConfig
	*PlaintextDiscoveryConfig
	*ConsulDiscoveryConfig
	*LXDDiscoveryConfig
}

type StaticDiscoveryConfig struct {
	StaticList []string `toml:"static_list" json:"static_list"`
}

type SrvDiscoveryConfig struct {
	SrvLookupServer  string `toml:"srv_lookup_server" json:"srv_lookup_server"`
	SrvLookupPattern string `toml:"srv_lookup_pattern" json:"srv_lookup_pattern"`
	SrvDnsProtocol   string `toml:"srv_dns_protocol" json:"srv_dns_protocol"`
}

type ExecDiscoveryConfig struct {
	ExecCommand []string `toml:"exec_command" json:"exec_command"`
}

type JsonDiscoveryConfig struct {
	JsonEndpoint        string `toml:"json_endpoint" json:"json_endpoint"`
	JsonHostPattern     string `toml:"json_host_pattern" json:"json_host_pattern"`
	JsonPortPattern     string `toml:"json_port_pattern" json:"json_port_pattern"`
	JsonWeightPattern   string `toml:"json_weight_pattern" json:"json_weight_pattern"`
	JsonPriorityPattern string `toml:"json_priority_pattern" json:"json_priority_pattern"`
	JsonSniPattern      string `toml:"json_sni_pattern" json:"json_sni_pattern"`
}

type PlaintextDiscoveryConfig struct {
	PlaintextEndpoint      string `toml:"plaintext_endpoint" json:"plaintext_endpoint"`
	PlaintextRegexpPattern string `toml:"plaintext_regex_pattern" json:"plaintext_regex_pattern"`
}

type DockerDiscoveryConfig struct {
	DockerEndpoint             string `toml:"docker_endpoint" json:"docker_endpoint"`
	DockerContainerLabel       string `toml:"docker_container_label" json:"docker_container_label"`
	DockerContainerPrivatePort int64  `toml:"docker_container_private_port" json:"docker_container_private_port"`
	DockerContainerHostEnvVar  string `toml:"docker_container_host_env_var" json:"docker_container_host_env_var"`

	DockerTlsEnabled    bool   `toml:"docker_tls_enabled" json:"docker_tls_enabled"`
	DockerTlsCertPath   string `toml:"docker_tls_cert_path" json:"docker_tls_cert_path"`
	DockerTlsKeyPath    string `toml:"docker_tls_key_path" json:"docker_tls_key_path"`
	DockerTlsCacertPath string `toml:"docker_tls_cacert_path" json:"docker_tls_cacert_path"`
}

type ConsulDiscoveryConfig struct {
	ConsulHost               string `toml:"consul_host" json:"consul_host"`
	ConsulServiceName        string `toml:"consul_service_name" json:"consul_service_name"`
	ConsulServiceTag         string `toml:"consul_service_tag" json:"consul_service_tag"`
	ConsulServicePassingOnly bool   `toml:"consul_service_passing_only" json:"consul_service_passing_only"`
	ConsulDatacenter         string `toml:"consul_datacenter" json:"consul_datacenter"`

	ConsulAuthUsername string `toml:"consul_auth_username" json:"consul_auth_username"`
	ConsulAuthPassword string `toml:"consul_auth_password" json:"consul_auth_password"`

	ConsulTlsEnabled    bool   `toml:"consul_tls_enabled" json:"consul_tls_enabled"`
	ConsulTlsCertPath   string `toml:"consul_tls_cert_path" json:"consul_tls_cert_path"`
	ConsulTlsKeyPath    string `toml:"consul_tls_key_path" json:"consul_tls_key_path"`
	ConsulTlsCacertPath string `toml:"consul_tls_cacert_path" json:"consul_tls_cacert_path"`
}

type LXDDiscoveryConfig struct {
	LXDServerAddress        string `toml:"lxd_server_address" json:"lxd_server_address"`
	LXDServerRemoteName     string `toml:"lxd_server_remote_name" json:"lxd_server_remote_name"`
	LXDServerRemotePassword string `toml:"lxd_server_remote_password" json:"lxd_server_remote_password"`

	LXDConfigDirectory     string `toml:"lxd_config_directory" json:"lxd_config_directory"`
	LXDGenerateClientCerts bool   `toml:"lxd_generate_client_certs" json:"lxd_generate_client_certs"`
	LXDAcceptServerCert    bool   `toml:"lxd_accept_server_cert" json:"lxd_accept_server_cert"`

	LXDContainerLabelKey   string `toml:"lxd_container_label_key" json:"lxd_container_label_key"`
	LXDContainerLabelValue string `toml:"lxd_container_label_value" json:"lxd_container_label_value"`

	LXDContainerPort    int    `toml:"lxd_container_port" json:"lxd_container_port"`
	LXDContainerPortKey string `toml:"lxd_container_port_key" json:"lxd_container_port_key"`

	LXDContainerInterface    string `toml:"lxd_container_interface" json:"lxd_container_interface"`
	LXDContainerInterfaceKey string `toml:"lxd_container_interface_key" json:"lxd_container_interface_key"`

	LXDContainerSNIKey      string `toml:"lxd_container_sni_key" json:"lxd_container_sni_key"`
	LXDContainerAddressType string `toml:"lxd_container_address_type" json:"lxd_container_address_type"`
}

/**
 * Healthcheck configuration
 */
type HealthcheckConfig struct {
	Kind     string `toml:"kind" json:"kind"`
	Interval string `toml:"interval" json:"interval"`
	Passes   int    `toml:"passes" json:"passes"`
	Fails    int    `toml:"fails" json:"fails"`
	Timeout  string `toml:"timeout" json:"timeout"`

	/* Depends on Kind */

	*PingHealthcheckConfig
	*ExecHealthcheckConfig
}

type PingHealthcheckConfig struct{}

type ExecHealthcheckConfig struct {
	ExecCommand                string `toml:"exec_command" json:"exec_command"`
	ExecExpectedPositiveOutput string `toml:"exec_expected_positive_output" json:"exec_expected_positive_output"`
	ExecExpectedNegativeOutput string `toml:"exec_expected_negative_output" json:"exec_expected_negative_output"`
}

type PubsubMessage struct {
	ID             string `json:"id"`
	Name           string `json:"name"`
	Bucket         string `json:"bucket"`
	Generation     string `json:"generation"`
	MetaGeneration string `json:"metageneration"`
	ContentType    string `json:"contentType"`
	TimeCreated    string `json:"timeCreated"`
	Updated        string `json:"updated"`
	StorageClass   string `json:"storageClass"`
	Crc32c         string `json:"crc32c"`
	Topic          string `json:"topic"`
	Etag           string `json:"etag"`
}

type K8sArtifactStruct struct {
	Payload        interface{}       `yaml:"Payload" json:"payload"`
	EncData        string            `yaml:"EncData" json:"encdata"`
	Labels         map[string]string `yaml:"Labels" json:"labels"`
	Name           string            `yaml:"Name" json:"name"`
	Namespace      string            `yaml:"Namespace" json:"namespace"`
	KubeconfigPath string            `yaml:"KubeconfigPath" json:"kubeconfigpath"`
	Format         string            `yaml:"Format" json:"format"`
	FileName       string            `yaml:"FileName" json:"filename"`
}

type K8sRespStruct struct {
	ApiVersion string      `yaml:"ApiVersion" json:"api-version"`
	Data       string      `yaml:"Data" json:"data"`
	Kind       string      `yaml:"Kind" json:"kind"`
	Metadata   K8sMetadata `yaml:"Metadata" json:"metadata"`
	Type       string      `yaml:"Type" json:"type"`
}

type K8sMetadata struct {
	Annotations       string `yaml:"Annotations" json:"annotations"`
	CreationTimestamp string `yaml:"CreationTimestamp" json:"creation-timestamp"`
	Namespace         string `yaml:"Namespace" json:"namespace"`
	ResourceVersion   string `yaml:"ResourceVersion" json:"resource-version"`
	SelfLink          string `yaml:"SelfLink" json:"self-link"`
	Uid               string `yaml:"Uid" json:"uid"`
	Name              string `yaml:"Name" json:"name"`
}

func K8sConfigmapTemplate(cm K8sArtifactStruct) (string, error) {
	var serialized []byte
	var err error
	// var l []string
	// var ls string

	// fmt.Println("converting to a configmap: ", cm)
	if cm.Format == "yaml" {
		serialized, err = yaml.Marshal(cm.Payload)
		if err != nil {
			fmt.Println("failed to marshal interface: ", err)
			return "", err
		}
	} else if cm.Format == "json" {
		serialized, err = json.Marshal(cm.Payload)
		if err != nil {
			fmt.Println("failed to marshal interface: ", err)
			return "", err
		}

	}

	var data strings.Builder
	scanner := bufio.NewScanner(strings.NewReader(string(serialized)))
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		// fmt.Println(scanner.Text())
		line := string("    " + scanner.Text() + "\n")
		data.WriteString(line)

	}

	// fmt.Println("data:: ", data.String())
	config := `apiVersion: v1
kind: ConfigMap
metadata:
  name: ` + cm.Name + `
  namespace: ` + cm.Namespace + `
data:
  ` + cm.FileName + `: |
` + strings.Trim(data.String(), "\n")
	return config, nil
}

func K8sOpaqueSecretTemplate(cm K8sArtifactStruct) (string, error) {

	config := `apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: ` + cm.Name + `
  namespace: ` + cm.Namespace + `
data:
  payload: ` + cm.EncData
	return config, nil
}

type KubeconfigStruct struct {
	ApiVersion     string         `yaml:"ApiVersion" json:"apiversion"`
	Clusters       []K8sClusters  `yaml:"Clusters" json:"clusters"`
	Contexts       []K8sContexts  `yaml:"Contexts" json:"contexts"`
	Users          []K8sUsers     `yaml:"Users" json:"users"`
	CurrentContext string         `yaml:"Current-context" json:"currentcontext"`
	Kind           string         `yaml:"Kind" json:"kind"`
	Preferences    K8sPreferences `yaml:"Preferences" json:"preferences"`
}

type K8sPreferences struct {
}
type K8sClusters struct {
	Cluster K8sCluster `yaml:"Cluster" json:"cluster"`
	Name    string     `yaml:"Name" json:"name"`
}

type K8sCluster struct {
	CertificateAuthorityData string `yaml:"Certificate-authority-data" json:"certificateauthoritydata"`
	Server                   string `yaml:"Server" json:"server"`
}

type K8sContexts struct {
	Context K8sContext `yaml:"Context" json:"context"`
	Name    string     `yaml:"Name" json:"name"`
}

type K8sContext struct {
	Cluster string `yaml:"Cluster" json:"cluster"`
	User    string `yaml:"User" json:"user"`
}
type K8sUsers struct {
	Name string  `yaml:"Name" json:"name"`
	User K8sUser `yaml:"User" json:"user"`
}

type K8sUser struct {
	AuthProvider K8sAuthProvider `yaml:"Auth-provider" json:"authprovider"`
}

type K8sAuthProvider struct {
	Config K8sAuthConfig `yaml:"Config" json:"config"`
	Name   string        `yaml:"Name" json:"name"`
}

type K8sAuthConfig struct {
	CmdArgs   string `yaml:"Cmd-args" json:"cmdargs"`
	CmdPath   string `yaml:"Cmd-path" json:"cmdpath"`
	ExpiryKey string `yaml:"Expiry-key" json:"expirykey"`
	TokenKey  string `yaml:"Token-key" json:"tokenkey"`
}

type GobgpRouteState struct {
	Prefix string       `yaml:"Prefix" json:"prefix"`
	Route  []RouteState `yaml:"Route" json:"route"`
	Routes interface{}  `yaml:"Routes" json:"routes"`
}

type RouteState struct {
	Nlri       RouteNlri    `yaml:"Nlri" json:"nlri"`
	Age        int64        `yaml:"Prefix" json:"prefix"`
	Best       bool         `yaml:"Best" json:"best"`
	Attrs      []RouteAttrs `yaml:"Attrs" json:"attrs"`
	Stale      bool         `yaml:"Stale" json:"stale"`
	SourceID   string       `yaml:"SourceID" json:"source_id"`
	NeighborIP string       `yaml:"NeighborIP" json:"neighbor_ip"`
}

type RouteAttrs struct {
	Type        int      `yaml:"Type" json:"type"`
	Value       int      `yaml:"Value" json:"value"`
	Communities []uint32 `yaml:"Communities" json:"communities"`
	Nexthop     string   `yaml:"Nexthop" json:"nexthop"`
	AsPaths     []uint32 `yaml:"AsPaths" json:"as_paths"`
	Metric      int      `yaml:"Metric" json:"metric"`
	LocalPref   int      `yaml:"LocalPref" json:"localpref"`
}

type RouteNlri struct {
	Prefix string `yaml:"Prefix" json:"prefix"`
}

type GobgpNeighborState struct {
	State NeighborState `yaml:"State" json:"state"`
}

type BgpNeighborState struct {
	Messages        NeighborStateMessages `yaml:"Messages" json:"messages"`
	NeighborAddress string                `yaml:"NeighborAddress" json:"neighboraddress"`
	PeerAs          uint64                `yaml:"PeerAs" json:"peeras"`
	Queues          NeighborStateQueues   `yaml:"Queues" json:"queues"`
	SessionState    int                   `yaml:"SessionState" json:"sessionstate"`
	RemoteCap       []StateCap            `yaml:"RemoteCap" json:"remotecap"`
	LocalCap        []StateCap            `yaml:"LocalCap" json:"localcap"`
}

type NeighborStateMessages struct {
	Received StateMessages `yaml:"Received" json:"received"`
	Sent     StateMessages `yaml:"Sent" json:"sent"`
}

type StateMessages struct {
	Open      int `yaml:"Open" json:"open"`
	KeepAlive int `yaml:"KeepAlive" json:"keepalive"`
	Total     int `yaml:"Total" json:"total"`
}

type NeighborStateQueues struct {
}

type StateCap struct {
	TypeUrl string `Yaml:"TypeUrl" json:"typeurl"`
	Value   string `Yaml:"Value" json:"value"`
}
