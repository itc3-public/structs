package structs

import (
	"time"

	"firebase.google.com/go/auth"
	v1 "k8s.io/api/core/v1"
)

// type DataStruct struct {
// 	CustomerID               string   `json:"customerid,omitempty"`
// 	CustomerName             string   `json:"customername,omitempty"`
// 	Gcp                      Gcp      `json:"gcp,omitempty"`
// 	Aws                      Aws      `json:"aws,omitempty"`
// 	Azure                    Azure    `json:"azure,omitempty"`
// 	AcceptedCommunityStrings []string `yaml:"ACCEPTEDCOMMUNITYSTRINGS,omitempty" json:"acceptedcommunitystrings,omitempty"`
// 	InstanceName             string   `json:"instancename"`
// 	Router                   Router   `json:"router,omitempty"`
// 	Branches                 []Branch `json:"branchs,omitempty"`
// }

// type Branch struct {
// 	ProjectName string `json:"gitlabprojectname,omitempty"`
// 	ProjectID   string `json:"gitlabprojectid,omitempty"`
// 	BranchName  string `json:"gitlabbranchname,omitempty"`
// 	BranchID    string `json:"gitlabbranchid,omitempty"`
// }

// type Router struct {
// 	PrivateIP string `json:"privateip,omitempty"`
// 	ApiKey    string `json:"apikey,omitempty"`
// 	Url       string `json:"url,omitempty"`
// }

// type Azure struct {
// }
// type Aws struct {
// 	Vars              AwsVarsStruct `json:"vars,omitempty"`
// 	PipelineID        string        `json:"pipelineid,omitempty"`
// 	JobID             string        `json:"jobid,omitempty"`
// 	ServiceUUID       string        `json:"serviceuuid,omitempty"`
// 	BranchID          string        `json:"branchid,omitempty"`
// 	Branch            string        `json:"branch,omitempty"`
// 	GitlabProjectName string        `json:"gitlabprojectname,omitempty"`
// 	GitlabProjectID   string        `json:"gitlabprojectid,omitempty"`
// 	GetVariable       string        `json:"getvariable,omitempty"`
// 	DeleteVariable    string        `json:"deletevariable,omitempty"`
// 	Variables         []GcpVariable `json:"variables,omitempty"`
// 	AddVariable       GcpVariable   `json:"addvariable,omitempty"`
// 	AccessKey         string        `json:"accesskey,omitempty"`
// 	SecretKey         string        `json:"secretkey,omitempty"`
// 	Region            string        `json:"region,omitempty"`
// }

// type AwsVarsStruct struct {
// 	Provider                 string   `yaml:"PROVIDER,omitempty" json:"provider,omitempty"`
// 	Repo                     string   `yaml:"REPO,omitempty" json:"gitlabprojectname,omitempty"`
// 	Branch                   string   `yaml:"BRANCH,omitempty" json:"gitlabbranchname,omitempty"`
// 	GitlabProjectName        string   `json:"gitlabprojectname,omitempty"`
// 	GitlabProjectID          string   `json:"gitlabprojectid,omitempty"`
// 	AccessKey                string   `json:"accesskey,omitempty"`
// 	SecretKey                string   `json:"secretkey,omitempty"`
// 	Zones                    []string `yaml:"ZONES,omitempty" json:"zones,omitempty"`
// 	Vpc                      string   `yaml:"VPC,omitempty" json:"vpc,omitempty"`
// 	Region                   string   `yaml:"REGION,omitempty" json:"region,omitempty"`
// 	Network                  string   `yaml:"NETWORKNAME,omitempty" json:"network,omitempty"`
// 	Count                    string   `yaml:"COUNT,omitempty" json:"hacount,omitempty"`
// 	Type                     string   `yaml:"TYPE,omitempty" json:"instancetype,omitempty"`
// 	CIDR                     string   `yaml:"CIDR,omitempty" json:"cidr,omitempty"`
// 	CommunityString          string   `yaml:"COMMUNITYSTRING" json:"communitystring,omitempty"`
// 	CustomerName             string   `yaml:"CUSTOMERNAME,omitempty" json:"customername,omitempty"`
// 	BuildName                string   `yaml:"BUILDNAME,omitempty" json:"buildname,omitempty"`
// 	UUID                     string   `yaml:"UUID,omitempty" json:"uuid,omitempty"`
// 	CustomerID               string   `yaml:"CUSTOMERID,omitempty" json:"customerid,omitempty"`
// 	PipelineID               string   `json:"pipelineid,omitempty"`
// 	JobID                    string   `json:"jobid,omitempty"`
// 	AcceptedCommunityStrings []string `yaml:"ACCEPTEDCOMMUNITYSTRINGS,omitempty" json:"acceptedcommunitystrings,omitempty"`
// }

// type Gcp struct {
// 	Vars              GcpVarsStruct `json:"vars,omitempty"`
// 	PipelineID        string        `json:"pipelineid,omitempty"`
// 	JobID             string        `json:"jobid,omitempty"`
// 	ProjectID      string        `json:"projectid,omitempty"`
// 	ServiceUUID       string        `json:"serviceuuid,omitempty"`
// 	BranchID          string        `json:"branchid,omitempty"`
// 	Branch            string        `json:"branch,omitempty"`
// 	GitlabProjectName string        `json:"gitlabprojectname,omitempty"`
// 	GitlabProjectID   string        `json:"gitlabprojectid,omitempty"`
// 	GetVariable       string        `json:"getvariable,omitempty"`
// 	DeleteVariable    string        `json:"deletevariable,omitempty"`
// 	Variables         []GcpVariable `json:"variables,omitempty"`
// 	AddVariable       GcpVariable   `json:"addvariable,omitempty"`
// 	Authentication    string        `json:"authentication,omitempty"`
// }

// type GcpVariable struct {
// 	Key              string `json:"key,omitempty"`
// 	Value            string `json:"value,omitempty"`
// 	Protected        bool   `json:"protected,omitempty"`
// 	EnvironmentScope string `json:"environment_scope,omitempty"`
// }

// type Pipeline struct {
// 	ID     int    `json:"id"`
// 	Status string `json:"status"`
// }
// type GcpVarsStruct struct {
// 	Provider                 string   `yaml:"PROVIDER,omitempty" json:"provider,omitempty"`
// 	Repo                     string   `yaml:"REPO,omitempty" json:"gitlabprojectname,omitempty"`
// 	Branch                   string   `yaml:"BRANCH,omitempty" json:"gitlabbranchname,omitempty"`
// 	ProjectID             string   `yaml:"PROJECT,omitempty" json:"projectid,omitempty"`
// 	GitlabProjectName        string   `json:"gitlabprojectname,omitempty"`
// 	GitlabProjectID          string   `json:"gitlabprojectid,omitempty"`
// 	Zones                    []string `yaml:"ZONES,omitempty" json:"zones,omitempty"`
// 	Region                   string   `yaml:"REGION,omitempty" json:"region,omitempty"`
// 	Network                  string   `yaml:"NETWORKNAME,omitempty" json:"network,omitempty"`
// 	Subnetwork               string   `yaml:"SUBNETNAME,omitempty" json:"subnetwork,omitempty"`
// 	Count                    string   `yaml:"COUNT,omitempty" json:"hacount,omitempty"`
// 	Type                     string   `yaml:"TYPE,omitempty" json:"instancetype,omitempty"`
// 	CIDR                     string   `yaml:"CIDR,omitempty" json:"cidr,omitempty"`
// 	CommunityStrings         []string `yaml:"COMMUNITYSTRINGS" json:"communitystrings,omitempty"`
// 	CustomerName             string   `yaml:"CUSTOMERNAME,omitempty" json:"customername,omitempty"`
// 	BuildName                string   `yaml:"BUILDNAME,omitempty" json:"buildname,omitempty"`
// 	UUID                     string   `yaml:"UUID,omitempty" json:"uuid,omitempty"`
// 	CustomerID               string   `yaml:"CUSTOMERID,omitempty" json:"customerid,omitempty"`
// 	PipelineID               string   `json:"pipelineid,omitempty"`
// 	JobID                    string   `json:"jobid,omitempty"`
// 	AcceptedCommunityStrings []string `yaml:"ACCEPTEDCS,omitempty" json:"acceptedcommunitystrings,omitempty"`
// 	RejectedCommunityStrings []string `yaml:"REJECTEDCS,omitempty" json:"rejectedcommunitystrings,omitempty"`
// }

// type AcceptedCommunityStrings struct {
// 	CIDR             string   `json:"cidr,omitempty"`
// 	CommunityStrings []string `json:"communitystrings,omitempty"`
// 	CustomerID       string   `json:"customerid,omitempty"`
// 	Provider         string   `json:"provider,omitempty"`
// 	Region           string   `json:",omitempty"`
// 	UUID             string   `json:",omitempty"`
// 	Zones            []string `json:",omitempty"`
// }

var hostOs string
var DockerHost string
var hostName []string

type ConfigStruct struct {
	Domain         string             `yaml:"Domain" json:"domain" survey:"domain"`
	AuthBackend    string             `yaml:"AuthBackend" json:"authbackend" survey:"authBackend"`
	StorageBackend string             `yaml:"StorageBackend" json:"storagebackend" survey:"storageBackend"`
	Registry       ContainerRegistry  `yaml:"Registry" json:"registry"`
	Debug          string             `yaml:"Debug" json:"debug"`
	GcpAuth        string             `yaml:"GcpAuth" json:"gcpauth"`
	Cluster        ClusterStruct      `yaml:"Cluster" json:"cluster"`
	Certificates   CertificatesStruct `yaml:"Certificates" json:"certificates"`
	Version        string             `yaml:"Version" json:"version"`
	HomeDir        string             `yaml:"HomeDir" json:"homedir"`
	ConfigFilePath string             `yaml:"ConfigFilePath" json:"configfilepath"`
	Status         StatusStruct       `yaml:"Status" json:"status"`
	ApiAddress     string             `yaml:"ApiAddress" json:"apiaddress"`
	CustomerID     string             `yaml:"CustomerID" json:"customerid"`
	CfgStringTest  string             `yaml:"CfgStringTest" json:"cfgstringtest"`
	DNSIP          string             `yaml:"DNSIP" json:"dnsip"`
	FirebaseUser   *auth.UserRecord   `yaml:"FirebaseUser" json:"firebaseuser"`
	PublicIP       string             `yaml:"PublicIP" json:"publicip"`
	UrlCode        string             `yaml:"UrlCode" json:"urlcode"`
	CfgBoolTest    bool               `yaml:"CfgBoolTest" json:"cfgbooltest"`
	Backups        BackupConfig       `yaml:"Backup" json:"backup"`
	RunVersion     string             `yaml:"RunVersion" json:"runversion"`
	CfgctlVersion  string             `yaml:"CfgctlVersion" json:"cfgctlversion"`
	CectlVersion   string             `yaml:"CectlVersion" json:"cectlversion"`
	PrivateOnly    bool               `yaml:"PrivateOnly" json:"privateonly"`
	PublicPrivate  string             `yaml:"PublicPrivate" json:"publicprivate"`
	Dev            bool               `yaml:"Dev" json:"dev"`
	// MasterRouter              RouterBootstrap     `yaml:"MasterRouter" json:"masterrouter"`
	// EdgeRouter                RouterBootstrap     `yaml:"EdgeRouter" json:"edgerouter"`
	VrouterK8sArtifacts       VrouterK8sArtifacts `yaml:"VrouterK8sArtifacts" json:"vrouterk8sartifacts"`
	VrouterUsername           string              `yaml:"VrouterUsername" json:"vrouterusername"`
	VrouterPublicKeyMaterial  string              `yaml:"VrouterPublicKeyMaterial" json:"vrouterpublickeymaterial"`
	VrouterPrivateKeyMaterial string              `yaml:"VrouterPrivateKeyMaterial" json:"vrouterprivatekeymaterial"`
	VrouterKeyPath            string              `yaml:"VrouterKeyPath" json:"vrouterkeypath"`
}

type VrouterK8sArtifacts struct {
	WeavePskSecret             string `yaml:"WeavePskSecret" json:"weavepsksecret"`
	ScopeSecret                string `yaml:"ScopeSecret" json:"scopesecret"`
	VrouterApiSecret           string `yaml:"VrouterApiSecret" json:"vrouterapisecret"`
	ControllerApiSecret        string `yaml:"ControllerApiSecret" json:"controllerapisecret"`
	Namespace                  string `yaml:"Namespace" json:"namespace"`
	MasterHelmDeploymentName   string `yaml:"MasterHelmDeploymentName" json:"masterhelmdeploymentname"`
	VrouterConfigmapName       string `yaml:"VrouterConfigmapName" json:"vrouterconfigmapname"`
	MasterVrouterConfigmapName string `yaml:"MasterVrouterConfigmapName" json:"mastervrouterconfigmapname"`
}
type BackupConfig struct {
	Bucket     string `yaml:"Bucket" json:"bucket"`
	Provider   string `yaml:"Provider" json:"provider"`
	SecretData string `yaml:"SecretData" json:"secretdata"`
	Enabled    bool   `yaml:"Enabled" json:"enabled"`

	GcpBucket     string `yaml:"GcpBucket" json:"gcpbucket"`
	ProviderGcp   bool   `yaml:"ProviderGcp" json:"providergcp"`
	GcpSecretData string `yaml:"GcpSecretData" json:"gcpsecretdata"`
	GcpEnabled    bool   `yaml:"GcpEnabled" json:"gcpenabled"`
}

type BackupApiCommands struct {
	BackupName string `json:"backupname"`
}

type ControllerCommand struct {
	Command string `json:"command"`
}

type StatusStruct struct {
	Ready                     bool     `yaml:"Ready" json:"ready"`
	ApiServerStatus           string   `yaml:"ApiServerStatus" json:"apiserverstatus"`
	ControllerStatus          string   `yaml:"ControllerStatus" json:"controllerstatus"`
	NetdataStatus             string   `yaml:"NetdataStatus" json:"netdatastatus"`
	TraefikStatus             string   `yaml:"TraefikStatus" json:"traefikstatus"`
	LoadbalancerExternalIP    []string `yaml:"LoadbalancerExternalIP" json:"loadbalancerexternalip`
	LocalIP                   string   `yaml:"LocalIP" json:"localip"`
	APIUrl                    string   `yaml:"APIUrl" json:"apiurl"`
	APIServiceEndpoint        string   `yaml:"APIServiceEndpoint" json:"apiserviceendpoint"`
	ControllerServiceEndpoint string   `yaml:"ControllerServiceEndpoint" json:"controllerserviceendpoint"`
	NetdataServiceEndpoint    string   `yaml:"NetdataServiceEndpoint" json:"netdataserviceendpoint"`
	TraefikServiceEndpoint    string   `yaml:"TraefikServiceEndpoint" json:"traefikserviceendpoint"`
	K8sApiServiceEndpoint     string   `yaml:"K8sApiServiceEndpoint" json:"k8sapiserviceendpoint"`
	ScopeServiceEndpoint      string   `yaml:"ScopeServiceEndpoint" json:"scopeserviceendpoint"`
	K8sApiIPAddress           string   `yaml:"K8sApiIPAddress" json:"k8sapiipaddress"`
	PublicIP                  string   `yaml:"PublicIP" json:"publicip"`
	PrivateOnlyMode           bool     `yaml:"PrivateOnlyMode" json:"privateonlymode"`

	// ControllerUrl string `yaml:"ControllerUrl" json:"controllerurl"`
	// TraefikUrl    string `yaml:"TraefikUrl" json:"traefikurl"`
}

type CertificatesStruct struct {
	Host         []string `yaml:"Host" json:"host"`
	Organization string   `yaml:"Organization" json:"organization"`
	ValidFor     *string  `yaml:"ValidFor" json:"validfor"`
	ValidFrom    *string  `yaml:"ValidFrom" json:"validfrom"`
}

type ClusterStruct struct {
	NodeCount string `yaml:"NodeCount" json:"nodecount"`
	Secret    string `yaml:"Secret" json:"secret"`
	Version   string `yaml:"Version" json:"version"`
}

type ContainerRegistry struct {
	Address    string         `yaml:"Address" json:"address"`
	Controller ContainerImage `yaml:"Controller" json:"controller"`
	API        ContainerImage `yaml:"API" json:"api"`
	IPam       ContainerImage `yaml:"IPam" json:"ipam"`
	Mysql      ContainerImage `yaml:"Mysql" json:"mysql"`
	Traefik    ContainerImage `yaml:"Traefik" json:"traefik"`
}

type ContainerImage struct {
	Image string `yaml:"Image" json:"image"`
	Tag   string `yaml:"Tag" json:"tag"`
}

type ExternalVrouterService struct {
	Name      string        `yaml:"Name" json:"name"`
	Service   *v1.Service   `yaml:"Service" json:"service"`
	Endpoint  *v1.Endpoints `yaml:"Endpoint" json:"endpoint"`
	HelmChart string        `yaml:"HelmChart" json:"helmchart"`
	ConfigMap string        `yaml:"ConfigMap" json:"configmap"`
}

type K8sRespStructList struct {
	ApiVersion string                 `yaml:"apiVersion" json:"apiVersion"`
	Data       string                 `yaml:"data" json:"data"`
	Kind       string                 `yaml:"kind" json:"kind"`
	Metadata   K8sMetadata            `yaml:"metadata" json:"metadata"`
	Type       string                 `yaml:"Type" json:"type"`
	Items      map[string]interface{} `yaml:"Items"  json:"items"`
}

type Cecfg struct {
	UserID                 string
	CompanyID              string
	LastLogin              time.Time
	ServiceCard            ConnectedEdgeService
	ProviderCard           ConnectedEdgeProvider
	ServiceCards           []ConnectedEdgeService
	ProviderCards          []ConnectedEdgeProvider
	RouterCard             RouterBootstrap
	RouterCards            []RouterBootstrap
	GlobalCommunityStrings []string
	LocalAPICall           LocalAPICall
}

type LocalAPICall struct {
	Url       string
	ApiKey    string
	PrivateIP string
}

type LicenseActivationResp struct {
	ActivationsLeft int    `yaml:"ActivationsLeft" json:"activations_left"`
	Checksum        string `yaml:"Checksum" json:"checksum"`
	CustomerEmail   string `yaml:"CustomerEmail" json:"customer_email"`
	CustomerName    string `yaml:"CustomerName" json:"customer_name"`
	Expires         string `yaml:"Expires" json:"expires"`
	ItemID          int    `yaml:"ItemID" json:"item_id"`
	ItemName        string `yaml:"ItemName" json:"item_name"`
	License         string `yaml:"License" json:"license"`
	LicenseLimit    int    `yaml:"LicenseLimit" json:"license_limit"`
	PaymentID       int    `yaml:"PaymentID" json:"payment_id"`
	PriceID         bool   `yaml:"PriceID" json:"price_id"`
	SiteCount       int    `yaml:"SiteCount" json:"site_count"`
	Success         bool   `yaml:"Success" json:"success"`
	Error           string `yaml:"Error" json:"error"`
}
type ConnectedEdgeService struct {
	UUID         string                `json:"uuid"`
	ShortName    string                `json:"shortname"`
	Icon         string                `yaml:"Icon" json:"icon"`
	Show         bool                  `json:"show"`
	Active       bool                  `json:"active"`
	Image        string                `json:"image"`
	Provider     string                `json:"provider"`
	Type         string                `json:"type"`
	Popular      string                `json:"popular"`
	BestSeller   bool                  `json:"bestseller"`
	DemoVideoURL string                `json:"demoVideoUrl"`
	Name         string                `json:"name"`
	Link         string                `json:"link"`
	Price        string                `json:"price"`
	OldPrice     string                `json:"oldPrice"`
	Discount     int                   `json:"discount"`
	LastUpdate   string                `json:"lastUpdates"`
	BestSell     string                `json:"bestSell"`
	Rating       int                   `json:"rating"`
	Level        string                `json:"level"`
	Configure    ServiceConfigure      `json:"configure"`
	Describe     string                `json:"describe"`
	Features     []string              `json:"features"`
	SerialNumber string                `json:"serialnumber"`
	RunCmd       ApiCommand            `json:"runcmd"`
	License      LicenseActivationResp `json:"license"`
}

type ApiCommand struct {
	Command string `json:"command"`
}

type ConnectedEdgeBucket struct {
	Name             string   `yaml:"Name" json:"name"`
	Provider         string   `yaml:"Provider" json:"provider"`
	Region           string   `yaml:"Region" json:"region"`
	Location         string   `yaml:"Location" json:"location"`
	StorageClass     string   `yaml:"StorageClass" json:"storageclass"`
	EnableVersioning bool     `yaml:"EnableVersioning" json:"enableversioning"`
	Acl              []string `yaml:"Acl" json:"acl"`
	ProjectID        string   `yaml:"ProjectID" json:"projectid"`
}

type ConnectedEdgeProvider struct {
	Name                string   `yaml:"Name" json:"name"`
	Icon                string   `yaml:"Icon" json:"icon"`
	Bucket              string   `yaml:"Bucket" json:"bucket"`
	BucketPrefix        string   `yaml:"BucketPrefix" json:"bucketprefix"`
	Region              string   `yaml:"Region" json:"region"`
	Description         string   `yaml:"Description" json:"description"`
	Email               string   `yaml:"Email" json:"email"`
	CertificateMaterial string   `yaml:"CertificateMaterial" json:"certificatematerial"`
	CertificatePath     string   `yaml:"CertificatePath" json:"certificatepath"`
	Default             string   `yaml:"Default" json:"default"`
	BackupProvider      string   `yaml:"BackupProvider" json:"backupprovider"`
	BackupBucket        string   `yaml:"BackupBucket" json:"backupbucket"`
	BackupBucketPrefix  string   `yaml:"BackupBucketPrefix" json:"backupbucketprefix"`
	RemoteProvider      string   `yaml:"RemoteProvider" json:"remoteprovider"`
	RemoteBucket        string   `yaml:"RemoteBucket" json:"remotebucket"`
	RemoteBucketPrefix  string   `yaml:"RemoteBucketPrefix" json:"remotebucketprefix"`
	RemoteProjectID     string   `yaml:"RemoteProjectID" json:"remoteprojectid"`
	RemoteRegion        string   `yaml:"RemoteRegion" json:"remoteregion"`
	BackupAccessKey     string   `yaml:"BackupAccessKey" json:"backupaccesskey"`
	BackupSecretKey     string   `yaml:"BackupSecretKey" json:"backupsecretkey"`
	AccessKey           string   `yaml:"AccessKey" json:"accesskey"`
	SecretKey           string   `yaml:"SecretKey" json:"secretkey"`
	ApplicationID       string   `yaml:"ApplicationID" json:"applicationid"`
	DirectoryID         string   `yaml:"DirectoryID" json:"directoryid"`
	Secret              string   `yaml:"Secret" json:"secret"`
	Provider            string   `yaml:"Provider" json:"provider"`
	ProjectID           string   `yaml:"ProjectID" json:"projectid"`
	Subnetwork          string   `yaml:"Subnetwork" json:"subnetwork"`
	Network             string   `yaml:"Network" json:"network"`
	Zones               []string `yaml:"Zones" json:"zones"`
	Vpc                 string   `yaml:"Vpc" json:"vpc"`
	Uuid                string   `yaml:"Uuid" json:"uuid"`
	Link                string   `yaml:"Link" json:"link"`
	SSHUsername         string   `yaml:"SSHUsername" json:"sshusername"`
	SSHKey              string   `yaml:"SSHKey" json:"sshkey"`
	SSHPassword         string   `yaml:"SSHPassword" json:"sshpassword"`
}

type ConnectedEdgeSite struct {
	Location                  string               `yaml:"Location" json:"location"`
	SimpleName                string               `yaml:"SimpleName" json:"simplename"`
	Tags                      map[string]string    `yaml:"Tags" json:"tags"`
	Description               string               `yaml:"Description" json:"description:"`
	Password                  string               `yaml:"Password" json:"password"`
	Icon                      string               `yaml:"Icon" json:"icon"`
	CertificateMaterial       string               `yaml:"CertificateMaterial" json:"certificatematerial"`
	CertificatePath           string               `yaml:"CertificatePath" json:"certificatepath"`
	Name                      string               `yaml:"Name" json:"name"`
	IPAddress                 string               `yaml:"IPAddress" json:"ipaddress"`
	Username                  string               `yaml:"Username" json:"username"`
	KubeconfigData            *string              `yaml:"KubeconfigData" json:"kubeconfigdata"`
	SSHMode                   string               `yaml:"SSHMode" json:"sshmode"`
	RouterConfigString        *string              `yaml:"RouterConfigString" json:"routerconfigstring"`
	CIDR                      string               `json:"cidr"`
	Enabled                   bool                 `json:"enabled"`
	HACount                   string               `json:"hacount"`
	InstanceCount             string               `json:"instancecount"`
	ProjectID                 string               `json:"projectid"`
	Region                    interface{}          `json:"region"`
	Zones                     []interface{}        `json:"zones"`
	Network                   interface{}          `json:"network"`
	Subnetworks               []interface{}        `json:"subnetworks"`
	GitBranchID               string               `json:"gitbranchid"`
	GitBranchName             string               `json:"gitbranchname"`
	Instances                 []string             `json:"instances"`
	AcceptedCommunityStrings  []string             `json:"acceptedcommunitystrings"`
	RejectedCommunityStrings  []string             `json:"rejectedcommunitystrings"`
	AcceptedPrefixes          []string             `json:"acceptedprefixes"`
	AdvertisePrefixes         []string             `json:"advertiseprefixes"`
	RejectedPrefixes          []string             `json:"rejectedprefixes"`
	BgpPolicies               []CePolicyDefinition `json:"bgppolicies"`
	BgpNeighborSets           []CeNeighborSet      `json:"bgpneighborsets"`
	BgpCommunitySets          []CeCommunitySet     `json:"bgpcommunitysets"`
	BgpPrefixSets             []CePrefixSet        `json:"bgpprefixsets"`
	BgpTagSets                []CeTagSet           `json:"bgptagsets"`
	BgpStatements             []CeStatement        `json:"bgpstatements"`
	BgpDynamicNeighborSets    []CeDynamicNeighbor  `json:"bgpdynamicneighborsets"`
	BgpPeerGroups             []CePeerGroup        `json:"bgppeergroups"`
	CommunityStrings          []string             `json:"communitystrings"`
	Type                      string               `json:"type"`
	CustomerName              string               `json:"customername"`
	CustomerID                string               `json:"customerid"`
	PipelineID                string               `json:"pipelineid"`
	JobID                     string               `json:"jobid"`
	BackupBucket              string               `json:"backupbucket"`
	BackupProvider            string               `json:"backupprovider"`
	CredentialProvider        interface{}          `json:"credentialprovider"`
	Bucket                    string               `json:"bucket"`
	AccessKey                 string               `json:"accesskey"`
	SecretKey                 string               `json:"secretkey"`
	FileName                  string               `json:"filename"`
	Action                    string               `json:"action"`
	BuildName                 string               `json:"buildname"`
	RoleName                  string               `json:"rolename"`
	Roles                     []string             `json:"roles"`
	Project                   string               `json:"gcpproject"`
	Count                     string               `json:"count"`
	DiskImage                 []string             `json:"diskimage"`
	DiskSize                  []string             `json:"disksize"`
	DiskType                  string               `json:"disktype"`
	Vpc                       interface{}          `json:"vpc"`
	VrouterUsername           string               `json:"vrouterusername"`
	VrouterKeyPath            string               `json:"vrouterkeypath"`
	VrouterPublicKeyMaterial  string               `json:"vrouterpublickeymaterial"`
	VrouterPrivateKeyMaterial string               `json:"vrouterprivatekeymaterial"`
	VrouterAwsKeypairName     string               `json:"vrouterawskeypairname"`
	SecurityGroup             interface{}          `json:"securitygroup"`
	IAMProfile                interface{}          `json:"iamprofile"`
	Subnet                    interface{}          `json:"subnet"`
	KeyName                   string               `json:"keyname"`
	InstanceName              string               `json:"instancename"`
	StaticIPList              []string             `json:"staticiplist"`
	RunVersion                string               `json:"runversion"`
	Licensed                  bool                 `json:"licensed"`
	Vrouters                  []interface{}        `json:"vrouters"`
	Subnets                   []interface{}        `json:"subnets"`
	RoutedPrefixes            []interface{}        `json:"routedprefixes"`
}

type ServiceConfigure struct {
	SiteAssignment            string                `yaml:"SiteAssignment" json:"siteassignment"`
	ObjectLinks               []string              `yaml:"ObjectLinks" json:"objectlinks"`
	Location                  string                `yaml:"Location" json:"location"`
	SimpleName                string                `yaml:"SimpleName" json:"simplename"`
	Tags                      map[string]string     `yaml:"Tags" json:"tags"`
	Description               string                `yaml:"Description" json:"description:"`
	Password                  string                `yaml:"Password" json:"password"`
	Icon                      string                `yaml:"Icon" json:"icon"`
	CertificateMaterial       string                `yaml:"CertificateMaterial" json:"certificatematerial"`
	CertificatePath           string                `yaml:"CertificatePath" json:"certificatepath"`
	Name                      string                `yaml:"Name" json:"name"`
	IPAddress                 string                `yaml:"IPAddress" json:"ipaddress"`
	Username                  string                `yaml:"Username" json:"username"`
	KubeconfigData            *string               `yaml:"KubeconfigData" json:"kubeconfigdata"`
	SSHMode                   string                `yaml:"SSHMode" json:"sshmode"`
	RouterConfigString        *string               `yaml:"RouterConfigString" json:"routerconfigstring"`
	RouterConfig              RouterBootstrap       `yaml:"RouterConfig" json:"routerconfig"`
	CIDR                      string                `json:"cidr"`
	Enabled                   bool                  `json:"enabled"`
	HACount                   string                `json:"hacount"`
	InstanceCount             string                `json:"instancecount"`
	ProjectID                 string                `json:"projectid"`
	Region                    string                `json:"region"`
	Zones                     []string              `json:"zones"`
	Network                   string                `json:"network"`
	Subnetwork                string                `json:"subnetwork"`
	GitBranchID               string                `json:"gitbranchid"`
	GitBranchName             string                `json:"gitbranchname"`
	ProviderCreds             ConnectedEdgeProvider `json:"providercreds"`
	RemoteBackend             ConnectedEdgeProvider `json:"remotebackend"`
	StorageBucket             ConnectedEdgeBucket   `json:"storagebucket"`
	SiteConfig                ConnectedEdgeSite     `json:"siteconfig"`
	Instances                 []string              `json:"instances"`
	AcceptedCommunityStrings  []string              `json:"acceptedcommunitystrings"`
	RejectedCommunityStrings  []string              `json:"rejectedcommunitystrings"`
	AcceptedPrefixes          []string              `json:"acceptedprefixes"`
	AdvertisePrefixes         []string              `json:"advertiseprefixes"`
	RejectedPrefixes          []string              `json:"rejectedprefixes"`
	BgpPolicy                 CePolicyDefinition    `json:"bgppolicy"`
	BgpNeighborSets           []CeNeighborSet       `json:"bgpneighborsets"`
	BgpCommunitySets          []CeCommunitySet      `json:"bgpcommunitysets"`
	BgpPrefixSets             []CePrefixSet         `json:"bgpprefixsets"`
	BgpTagSets                []CeTagSet            `json:"bgptagsets"`
	BgpStatement              CeStatement           `json:"bgpstatement"`
	BgpDynamicNeighborSets    []CeDynamicNeighbor   `json:"bgpdynamicneighborsets"`
	BgpLinkedStatements       []CeStatement         `json:"bgplinkedstatements"`
	BgpLinkedDefinedSets      CeDefinedSets         `json:"bgplinkeddefinedsets"`
	BgpPeerGroups             []CePeerGroup         `json:"bgppeergroups"`
	CommunityStrings          []string              `json:"communitystrings"`
	PolicyGroup               PolicyGroup           `json:"policygroup"`
	GlobalPolicy              bool                  `json:"globalpolicy"`
	GroupAssignments          []string              `json:"groupassignments"`
	GroupExclusions           []string              `json:"groupexclusions"`
	Type                      string                `json:"type"`
	CustomerName              string                `json:"customername"`
	CustomerID                string                `json:"customerid"`
	PipelineID                string                `json:"pipelineid"`
	JobID                     string                `json:"jobid"`
	BackupBucket              string                `json:"backupbucket"`
	BackupProvider            string                `json:"backupprovider"`
	CredentialProvider        string                `json:"credentialprovider"`
	Bucket                    string                `json:"bucket"`
	AccessKey                 string                `json:"accesskey"`
	SecretKey                 string                `json:"secretkey"`
	FileName                  string                `json:"filename"`
	Action                    string                `json:"action"`
	BuildName                 string                `json:"buildname"`
	RoleName                  string                `json:"rolename"`
	Roles                     []string              `json:"roles"`
	Project                   string                `json:"gcpproject"`
	Count                     string                `json:"count"`
	DiskImage                 []string              `json:"diskimage"`
	DiskSize                  []string              `json:"disksize"`
	DiskType                  string                `json:"disktype"`
	VpcID                     string                `json:"vpcid"`
	VrouterUsername           string                `json:"vrouterusername"`
	VrouterKeyPath            string                `json:"vrouterkeypath"`
	VrouterPublicKeyMaterial  string                `json:"vrouterpublickeymaterial"`
	VrouterPrivateKeyMaterial string                `json:"vrouterprivatekeymaterial"`
	VrouterAwsKeypairName     string                `json:"vrouterawskeypairname"`
	SecurityGroup             string                `json:"securitygroup"`
	IAMProfile                string                `json:"iamprofile"`
	SubnetID                  string                `json:"subnetid"`
	KeyName                   string                `json:"keyname"`
	InstanceName              string                `json:"instancename"`
	StaticIPList              []string              `json:"staticiplist"`
	RunVersion                string                `json:"runversion"`
	Licensed                  bool                  `json:"licensed"`
	Vrouters                  []string              `json:"vrouters"`
	Subnets                   []string              `json:"subnets"`
	RoutedPrefixes            []string              `json:"routedprefixes"`
}

type CeDefinedSets struct {
	BgpNeighborSets        []CeNeighborSet     `json:"bgpneighborsets"`
	BgpCommunitySets       []CeCommunitySet    `json:"bgpcommunitysets"`
	BgpPrefixSets          []CePrefixSet       `json:"bgpprefixsets"`
	BgpTagSets             []CeTagSet          `json:"bgptagsets"`
	BgpDynamicNeighborSets []CeDynamicNeighbor `json:"bgpdynamicneighborsets"`
	BgpPeerGroups          []CePeerGroup       `json:"bgppeergroups"`
}
type PolicyGroup struct {
	Name       string                  `json:"name"`
	MemberList []string                `json:"memberlist"`
	PolicyList []string                `json:"policylist"`
	Members    []*ConnectedEdgeService `json:"members"`
	Policy     []*ConnectedEdgeService `json:"policy"`
}
type CePolicyDefinition struct {
	Name        string   `mapstructure:"name" json:"name" yaml:"Name"`
	Description string   `json:"description"`
	Statements  []string `mapstructure:"statements" json:"statements" yaml:"Statements"`
}

type CeStatement struct {
	Name                 string        `mapstructure:"name" json:"name" yaml:"Name"`
	Description          string        `json:"description"`
	Conditions           CeConditions  `mapstructure:"conditions" json:"conditions" yaml:"Conditions"`
	Actions              CeActions     `mapstructure:"actions" json:"actions" yaml:"Actions"`
	BgpLinkedDefinedSets CeDefinedSets `json:"bgplinkeddefinedsets"`
}
type CeConditions struct {
	CallPolicy        string             `mapstructure:"callpolicy" json:"callpolicy" yaml:"CallPolicy"`
	MatchPrefixSet    CeMatchPrefixSet   `mapstructure:"matchprefixset" json:"matchprefixset" yaml:"MatchPrefixSet"`
	MatchNeighborSet  CeMatchNeighborSet `mapstructure:"matchneighborset" json:"matchneighborset" yaml:"MatchNeighborSet"`
	MatchTagSet       CeMatchTagSet      `mapstructure:"matchtagset" json:"matchtagset" yaml:"MatchTagSet"`
	InstallProtocolEq string             `mapstructure:"installprotocoleq" json:"installprotocoleq" yaml:"InstallProtocolEq"`
	IgpConditions     string             `mapstructure:"igpconditions" json:"igpconditions" yaml:"IgpConditions"`
	BgpConditions     CeBgpConditions    `mapstructure:"bgpconditions" json:"bgpconditions" yaml:"BgpConditions"`
}
type CeActions struct {
	RouteDisposition string       `mapstructure:"routedisposition" json:"routedisposition" yaml:"RouteDisposition"`
	IgpActions       CeIgpActions `mapstructure:"igpactions" json:"igpactions" yaml:"IgpActions"`
	BgpActions       CeBgpActions `mapstructure:"bgpactions" json:"bgpactions" yaml:"BgpActions"`
}
type CeMatchPrefixSet struct {
	PrefixSet       string `mapstructure:"prefixset" json:"prefixset" yaml:"PrefixSet"`
	MatchSetOptions string `mapstructure:"matchsetoptions" json:"matchsetoptions" yaml:"MatchSetOptions"`
}
type CeMatchNeighborSet struct {
	NeighborSet     string `mapstructure:"neighborset" json:"neighborset" yaml:"NeighborSet"`
	MatchSetOptions string `mapstructure:"matchsetoptions" json:"matchsetoptions" yaml:"MatchSetOptions"`
}
type CeMatchTagSet struct {
	TagSet          string `mapstructure:"tagset" json:"tagset" yaml:"TagSet"`
	MatchSetOptions string `mapstructure:"matchsetoptions" json:"matchsetoptions" yaml:"MatchSetOptions"`
}
type CeBgpConditions struct {
	MatchCommunitySet      CeMatchCommunitySet      `mapstructure:"matchcommunityset" json:"matchcommunityset" yaml:"MatchCommunitySet"`
	MatchExtCommunitySet   CeMatchExtCommunitySet   `mapstructure:"matchextcommunityset" json:"matchextcommunityset" yaml:"MatchExtCommunitySet"`
	MatchAsPathSet         CeMatchAsPathSet         `mapstructure:"matchaspathset" json:"matchaspathset" yaml:"MatchAsPathSet"`
	MedEq                  uint32                   `mapstructure:"medeq" json:"medeq" yaml:"MedEq"`
	NextHopInList          []string                 `mapstructure:"nexthopinlist" json:"nexthopinlist" yaml:"NextHopInList"`
	LocalPrefEq            uint32                   `mapstructure:"localprefeq" json:"localprefeq" yaml:"LocalPrefEq"`
	MatchLargeCommunitySet CeMatchLargeCommunitySet `mapstructure:"matchlargecommunityset" json:"matchlargecommunityset" yaml:"MatchLargeCommunitySet"`
}

type CeMatchCommunitySet struct {
	CommunitySet    string `mapstructure:"communityset" json:"communityset" yaml:"CommunitySet"`
	MatchSetOptions string `mapstructure:"matchsetoptions" json:"matchsetoptions" yaml:"MatchSetOptions"`
}

type CeMatchLargeCommunitySet struct {
	LargeCommunitySet string `mapstructure:"largecommunityset" json:"largecommunityset" yaml:"LargeCommunitySet"`
	MatchSetOptions   string `mapstructure:"matchsetoptions" json:"matchsetoptions" yaml:"MatchSetOptions"`
}
type CeAsPathLength struct {
	Operator AttributeComparison `mapstructure:"operator" json:"operator" yaml:"Operator"`
	Value    uint32              `mapstructure:"value" json:"value" yaml:"Value"`
}
type CeCommunityCount struct {
	Operator string `mapstructure:"operator" json:"operator" yaml:"Operator"`
	Value    uint32 `mapstructure:"value" json:"value" yaml:"Value"`
}
type CeMatchAsPathSet struct {
	AsPathSet       string `mapstructure:"aspathset" json:"aspathset" yaml:"AsPathSet"`
	MatchSetOptions string `mapstructure:"matchsetoptions" json:"matchsetoptions" yaml:"MatchSetOptions"`
}
type CeMatchExtCommunitySet struct {
	ExtCommunitySet string `mapstructure:"extcommunityset" json:"extcommunityset" yaml:"ExtCommunitySet"`
	MatchSetOptions string `mapstructure:"matchsetoptions" json:"matchsetoptions" yaml:"MatchSetOptions"`
}
type CeIgpActions struct {
	SetTag TagType `mapstructure:"settag" json:"settag" yaml:"SetTag"`
}
type CeBgpActions struct {
	SetAsPathPrepend CeSetAsPathPrepend `mapstructure:"setaspathprepend" json:"setaspathprepend" yaml:"SetAsPathPrepend"`
	SetCommunity     CeSetCommunity     `mapstructure:"setcommunity" json:"setcommunity" yaml:"SetCommunity"`
	SetExtCommunity  CeSetExtCommunity  `mapstructure:"setextcommunity" json:"setextcommunity" yaml:"SetExtCommunity"`
	SetRouteOrigin   string             `mapstructure:"setrouteorigin" json:"setrouteorigin" yaml:"SetRouteOrigin"`
	SetLocalPref     uint32             `mapstructure:"setlocalpref" json:"setlocalpref" yaml:"SetLocalPref"`
	SetNextHop       string             `mapstructure:"setnexthop" json:"setnexthop" yaml:"SetNextHop"`
	SetMed           uint32             `mapstructure:"setmed" json:"setmed" yaml:"SetMed"`
}
type CeSetAsPathPrepend struct {
	RepeatN uint8  `mapstructure:"repeatn" json:"repeatn" yaml:"RepeatN"`
	As      string `mapstructure:"as" json:"as" yaml:"As"`
}
type CeSetCommunity struct {
	SetCommunityMethod CeSetCommunityMethod `mapstructure:"setcommunitymethod" json:"setcommunitymethod" yaml:"SetCommunityMethod"`
	Options            string               `mapstructure:"options" json:"options" yaml:"Options"`
}
type CeSetCommunityMethod struct {
	CommunitiesList []string `mapstructure:"communitieslist" json:"communitieslist" yaml:"CommunitiesList"`
	CommunitySetRef string   `mapstructure:"communitysetref" json:"communitysetref" yaml:"CommunitySetRef"`
}

type CeSetExtCommunity struct {
	SetExtCommunityMethod CeSetExtCommunityMethod `mapstructure:"setextcommunitymethod" json:"setextcommunitymethod" yaml:"SetExtCommunityMethod"`
	Options               string                  `mapstructure:"options" json:"options" yaml:"Options"`
}

type CeSetExtCommunityMethod struct {
	ExtCommunitiesList []string `mapstructure:"extcommunitieslist" json:"extcommunitieslist" yaml:"ExtCommunitiesList"`
	ExtCommunitySetRef string   `mapstructure:"extcommunitysetref" json:"extcommunitysetref" yaml:"ExtCommunitySetRef"`
}

type CePeerGroup struct {
	PeerAs           uint32 `json:"peeras"`
	LocalAs          uint32 `json:"localas"`
	PeerType         string `json:"peertype"`
	AuthPassword     string `json:"authpassword"`
	RemovePrivateAs  bool   `json:"removeprivateas"`
	RouteFlapDamping bool   `json:"routeflapdamping"`
	SendCommunity    string `json:"sendcommunity"`
	Description      string `json:"description"`
	PeerGroupName    string `json:"peergroupname"`
}

type CeDynamicNeighbor struct {
	Name      string `yaml:"Name" json:"name"`
	PeerGroup string `yaml:"PeerGroup" json:"peergroup"`
}

type CeCommunitySet struct {
	Name          string   `yaml:"Name" json:"name"`
	CommunityList []string `yaml:"CommunityList" json:"communitylist"`
}
type CePrefixSet struct {
	Name       string   `yaml:"Name" json:"name"`
	PrefixList []string `yaml:"PrefixList" json:"prefixlist"`
}

type CeTagSet struct {
	Name    string   `yaml:"Name" json:"name"`
	TagList []string `yaml:"TagList" json:"taglist"`
}

type CeNeighborSet struct {
	Name         string   `yaml:"Name" json:"name"`
	NeighborList []string `yaml:"NeighborList" json:"neighborlist"`
}

type RouterK8sBackendPlugins struct {
	RegisteredServices []K8sRegisteredServices `yaml:"RouterK8sBackendPlugins,omitempty" json:"routerk8sbackendplugins,omitempty"`
}

type K8sRegisteredServices struct {
	ServiceName string `yaml:"ServiceName,omitempty" json:"servicename,omitempty"`
	Published   bool   `yaml:"Published,omitempty" json:"published,omitempty"`
	NodePort    string `yaml:"NodePort,omitempty" json:"nodeport,omitempty"`
	Namespace   string `yaml:"Namespace,omitempty" json:"namespace,omitempty"`
	RealPort    string `yaml:"RealPort,omitempty" json:"realport,omitempty"`
	Protocol    string `yaml:"Protocol,omitempty" json:"protocol,omitempty"`
}

// HelloResponse is the JSON representation for a customized message
type HelloResponse struct {
	Message string `json:"message"`
}

type PayloadStruct struct {
	Payload      string   `json:"payload"`
	PayloadArray []string `json:"payloadarray"`
}

type KeygenLicense struct {
	ID            string              `json:"id,omitempty"`
	Type          string              `json:"type,omitempty"`
	Relationships KeygenRelationships `json:"relationships,omitempty"`
	Attributes    LicenseAttributes   `json:"attributes,omitempty"`
}

type LicenseAttributes struct {
	Name     string            `json:"name,omitempty"`
	Metadata map[string]string `json:"metadata,omitempty"`
}

type KeygenRelationships struct {
	Policy KeygenPolicy `json:"policy,omitempty"`
}

type KeygenPolicy struct {
	Data KeygenPolicyData `json:"data,omitempty"`
}

type KeygenUser struct {
	Data KeygenUserData `json:"data,omitempty"`
}

type KeygenPolicyData struct {
	Type string `json:"type,omitempty"`
	ID   string `json:"id,omitempty"`
}

type KeygenUserData struct {
	Type string `json:"type,omitempty"`
	ID   string `json:"id,omitempty"`
}

type ValidationRequest struct {
	Meta ValidationMeta `json:"meta,omitempty"`
}

type ValidationMeta struct {
	Key   string          `json:"key,omitempty"`
	Scope ValidationScope `json:"scope,omitempty"`
}

type ValidationScope struct {
	Product string `json:"product"`
}

type ValidationResponse struct {
	Validation `json:"meta"`
}

type Validation struct {
	Valid  bool   `json:"valid"`
	Detail string `json:"detail"`
}

type DataPayload struct {
	Data interface{} `json:"data"`
}
type KeygenUpdateMachine struct {
	ID         string                  `json:"id,omitempty"`
	Type       string                  `json:"type,omitempty"`
	Attributes KeygenMachineAttributes `json:"attributes,omitempty"`
	// Relationships KeygenMachineRelationships `json:"relationships,omitempty"`
}

type KeygenMachine struct {
	ID            string                     `json:"id,omitempty"`
	Type          string                     `json:"type,omitempty"`
	Attributes    KeygenMachineAttributes    `json:"attributes,omitempty"`
	Relationships KeygenMachineRelationships `json:"relationships,omitempty"`
}

type KeygenMachineRelationships struct {
	// Account KeygenMachineObjects `json:"account,omitempty"`
	// Product KeygenMachineObjects `json:"product,omitempty"`
	License KeygenMachineObjects `json:"license,omitempty"`
	// User    KeygenMachineObjects `json:"user,omitempty"`
}

type KeygenMachineObjects struct {
	//Links KeygenMachineLinks `json:"links,omitempty"`
	Data KeygenKeyData `json:"data,omitempty"`
}

type KeygenMachineLinks struct {
	Related string `json:"related,omitempty"`
}

type KeygenKeyData struct {
	Type string `json:"type,omitempty"`
	ID   string `json:"id,omitempty"`
}

type KeygenMachineAttributes struct {
	Fingerprint      string                  `json:"fingerprint,omitempty"`
	IP               string                  `json:"ip,omitempty"`
	Hostname         string                  `json:"hostname,omitempty"`
	Platform         string                  `json:"platform,omitempty"`
	Name             string                  `json:"name,omitempty"`
	RequireHeartbeat bool                    `json:"requireHeartbeat,omitempty"`
	LastHeartbeat    string                  `json:"lastHeartbeat,omitempty"`
	NextHeartbeat    string                  `json:"nextHeartbeat,omitempty"`
	Metadata         KeygenAttributeMetadata `json:"metadata,omitempty"`
	Created          string                  `json:"created,omitempty"`
	Updated          string                  `json:"updated,omitempty"`
}

type KeygenAttributeMetadata struct {
}

type Itc3ConfigMap struct {
	User    Itc3User    `yaml:"User" json:"user"`
	License Itc3License `yaml:"License" json:"license"`
	Sales   []Sale      `yaml:"Sales" json:"sales"`
}

type Itc3User struct {
	Name        string         `yaml:"Name" json:"name,omitempty"`
	ID          int            `yaml:"ID" json:"id,omitempty"`
	UserToken   AuthToken      `yaml:"UserToken" json:"usertoken,omitempty"`
	URL         string         `yaml:"URL" json:"url,omitempty"`
	Description string         `yaml:"Description" json:"description,omitempty"`
	Link        string         `yaml:"Link" json:"link,omitempty"`
	Slug        string         `yaml:"Slug" json:"slug,omitempty"`
	AvatarUrls  map[int]string `yaml:"AvatarUrls" json:"avatar_urls,omitempty"`
	Meta        Itc3Meta       `yaml:"Meta" json:"meta,omitempty"`
}

type Itc3Meta struct {
	UserID         string `yaml:"UserID" json:"userid,omitempty"`
	ControllerUUID string `yaml:"ControllerUUID" json:"controlleruuid,omitempty"`
	PublicIP       string `yaml:"PublicIP" json:"publicip,omitempty"`
	CustomerID     string `yaml:"CustomerID" json:"customerid,omitempty"`
	Email          string `yaml:"Email" json:"email,omitempty"`
	Password       string `yaml:"Password" json:"password,omitempty"`
	Img24          string `yaml:"Img24" json:"img24,omitempty"`
	Img48          string `yaml:"Img48" json:"img48,omitempty"`
	Img96          string `yaml:"Img96" json:"img96,omitempty"`
	CompanyName    string `yaml:"CompanyName" json:"companyname,omitempty"`
}

type AuthToken struct {
	Token           string `yaml:"Token" json:"token,omitempty"`
	UserEmail       string `yaml:"UserEmail" json:"user_email,omitempty"`
	UserNicename    string `yaml:"UserNicename" json:"user_nicename,omitempty"`
	UserDisplayName string `yaml:"UserDisplayName" json:"user_display_name,omitempty"`
}

type Sale struct {
	ID            int           `yaml:"ID" json:"ID,omitempty"`
	TransactionID interface{}   `yaml:"TransactionID,omitempty" json:"transaction_id,omitempty"`
	Key           string        `yaml:"Key,omitempty" json:"key,omitempty"`
	Subtotal      int           `yaml:"Subtotal,omitempty" json:"subtotal,omitempty"`
	Tax           string        `yaml:"Tax,omitempty" json:"tax,omitempty"`
	Total         string        `yaml:"Total,omitempty" json:"total,omitempty"`
	Gateway       string        `yaml:"Gateway,omitempty" json:"gateway,omitempty"`
	Email         string        `yaml:"Email,omitempty" json:"email,omitempty"`
	Date          string        `yaml:"Date,omitempty" json:"date,omitempty"`
	Products      []Itc3Product `yaml:"Products,omitempty" json:"products,omitempty"`
	Licenses      []Itc3License `yaml:"Licenses,omitempty" json:"licenses,omitempty"`
}

type Itc3Product struct {
	ID        int         `yaml:"ID,omitempty" json:"id,omitempty"`
	Quantity  int         `yaml:"Quantity,omitempty" json:"quantity,omitempty"`
	Name      string      `yaml:"Name,omitempty" json:"name,omitempty"`
	Price     interface{} `yaml:"Price,omitempty" json:"price,omitempty"`
	PriceName string      `yaml:"PriceName,omitempty" json:"pricename,omitempty"`
}
type Itc3License struct {
	ID     int    `yaml:"ID" json:"id,omitempty"`
	Name   string `yaml:"Name" json:"name,omitempty"`
	Status string `yaml:"Status" json:"status,omitempty"`
	Key    string `yaml:"Key" json:"key,omitempty"`
}
