SHELL = /bin/bash
VERSION=$(shell echo $(BRANCH_TAG))
ifeq ($(BRANCH_TAG),)
$(error BRANCH_TAG variable not set.)
endif

TOKEN=$(shell echo $(DOCKER_TOKEN))
ifeq ($(DOCKER_TOKEN),)
$(error DOCKER_TOKEN variable not set.)
endif

.PHONY: help build release local all
.DEFAULT_GOAL := help
# From http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


release-router: ## Release a version of router
	rm -rf rootfs
	cp /home/jeremypogue/go/src/github.com/cbsicloud/c3r/build/rootfs routerrootfs
	git add .
	git commit -a -m "update $(VERSION)
	git push

ci-router: ## Run gitlab-ci for vrouter build
	docker login registry.gitlab.com -u jeremypogue -p $(TOKEN)
	docker build -t registry.gitlab.com/itc3-public/containers/vrouter:$(VERSION) -f Dockerfile.vrouter .
	docker push registry.gitlab.com/itc3-public/containers/vrouter:$(VERSION)

